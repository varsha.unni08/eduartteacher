package com.report.schoolteacher.utils;

import android.content.Context;
import android.content.pm.PackageManager;
import android.webkit.MimeTypeMap;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import com.google.android.material.snackbar.Snackbar;

import java.io.File;

public class Utilities {

    public  static final String imageBaseurl="http://www.centroidsolutions.in/6_school/school_api/photos/";

    public static boolean checkPermission(Context context,String permission)
    {
        boolean a=false;

        if(ContextCompat.checkSelfPermission(context,permission)!= PackageManager.PERMISSION_GRANTED)
        {
            a=false;
        }
        else {

            a=true;
        }

        return a;


    }

    public static String getMimeType(File file)
    {
        MimeTypeMap mime = MimeTypeMap.getSingleton();
        int index = file.getName().lastIndexOf('.')+1;
        String ext = file.getName().substring(index).toLowerCase();
        String type = mime.getMimeTypeFromExtension(ext);

        return type;
    }

    public static void showInfo(Context context,String message)
    {
        Snackbar snackbar = Snackbar.make(((AppCompatActivity)context).findViewById(android.R.id.content), message, Snackbar.LENGTH_LONG);
        snackbar.show();
    }
}
