package com.report.schoolteacher.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.report.schoolteacher.R;
import com.report.schoolteacher.pojo.StudentLeave;

import java.util.List;

public class StudentLeaveAdapter extends RecyclerView.Adapter<StudentLeaveAdapter.StudentLeaveHolder> {


    Context context;
    List<StudentLeave>studentLeaves;

    public StudentLeaveAdapter(Context context, List<StudentLeave> studentLeaves) {
        this.context = context;
        this.studentLeaves = studentLeaves;
    }

    public class StudentLeaveHolder extends RecyclerView.ViewHolder{

        TextView txtdetails;

        public StudentLeaveHolder(@NonNull View itemView) {
            super(itemView);

            txtdetails=itemView.findViewById(R.id.txtdetails);
        }
    }

    @NonNull
    @Override
    public StudentLeaveHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_student_leaveadapter,parent,false);


        return new StudentLeaveHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull StudentLeaveHolder holder, int position) {

        holder.txtdetails.setText("Reason : "+studentLeaves.get(position).getReason()+"\nFrom date : "+studentLeaves.get(position).getFromDate()+"\nTo date : "+studentLeaves.get(position).getToDate());

    }

    @Override
    public int getItemCount() {
        return studentLeaves.size();
    }
}
