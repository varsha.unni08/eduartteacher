package com.report.schoolteacher.webservice;

import android.content.Context;


import com.report.schoolteacher.appconstants.Literals;
import com.report.schoolteacher.preferencehelper.PreferenceHelper;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitHelper {

    private static Retrofit retrofit;
    private static final String BASE_URL = "http://www.centroidsolutions.in/6_school/school_api/teacher_api/";
    public  static final String imageBaseurl="http://www.centroidsolutions.in/6_school/school_api/photos/";

    public static Retrofit getRetrofitInstance(final Context context) {




        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {

                Request original = chain.request();

                Request request = original.newBuilder()
                        .header("Authorization", "Bearer "+new PreferenceHelper(context).getData(Literals.Logintokenkey))

                        .method(original.method(), original.body())
                        .build();
                return chain.proceed(request);
            }
        });




        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(httpClient.build())
                    .build();
        }
        return retrofit;
    }
}
