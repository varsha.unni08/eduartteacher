package com.report.schoolteacher.activities;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatImageView;

import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonArray;
import com.report.schoolteacher.AppLiterals.Constants;
import com.report.schoolteacher.R;
import com.report.schoolteacher.connectivity.NetConnection;
import com.report.schoolteacher.pojo.Classes;
import com.report.schoolteacher.pojo.Subjects;
import com.report.schoolteacher.progress.ProgressFragment;
import com.report.schoolteacher.webservice.RetrofitHelper;
import com.report.schoolteacher.webservice.WebInterfaceimpl;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class AddLessonPlanActivity extends AppCompatActivity {

    ImageView imgback,imgdropdownClass,imgdropdownsubject;

    TextView txtClass,txtSubject,txttoDate;

    AppCompatImageView imgpickToDate;

    ProgressFragment progressFragment;

    String classid="",subjectid;

    Button btnSubmit;

    EditText edtTopic,edtDescription;

    String end_date="";



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_lesson_plan);
        getSupportActionBar().hide();
        imgback=findViewById(R.id.imgback);
        imgdropdownClass=findViewById(R.id.imgdropdownClass);

        imgdropdownsubject=findViewById(R.id.imgdropdownsubject);
        imgpickToDate=findViewById(R.id.imgpickToDate);
        txttoDate=findViewById(R.id.txttoDate);

        edtTopic=findViewById(R.id.edtTopic);
        edtDescription=findViewById(R.id.edtDescription);

        txtClass=findViewById(R.id.txtClass);

        txtSubject=findViewById(R.id.txtSubject);

        btnSubmit=findViewById(R.id.btnSubmit);



        imgpickToDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                showDatePicker();

            }
        });

        txttoDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDatePicker();

            }
        });



        imgback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                onBackPressed();
            }
        });


        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

//        imgdropdownClass.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                getClasses();
//
//            }
//        });
//
//        txtClass.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//                getClasses();
//
//            }
//        });

        imgdropdownsubject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                getSubjects();
            }
        });

        txtSubject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                getSubjects();

            }
        });

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {



                    if(!subjectid.equals(""))
                    {

                        if(!end_date.equals(""))
                        {

                            final Calendar cldr = Calendar.getInstance();
                            final int day = cldr.get(Calendar.DAY_OF_MONTH);
                            final int month = cldr.get(Calendar.MONTH);
                            final int year = cldr.get(Calendar.YEAR);

                            Date sta_date = null;
                            Date end_dat = null;

                            int m1=month+1;

                            String currentdate=year+"-"+m1+"-"+day;


                            try {

                                DateFormat dateFormat = new SimpleDateFormat("yyyy-mm-dd");
                                sta_date = dateFormat.parse(currentdate);
                                end_dat = dateFormat.parse(end_date);

                                if (sta_date.before(end_dat)) {

                                    if(!edtTopic.getText().toString().equals(""))
                                    {

                                        if(!edtDescription.getText().toString().equals(""))
                                        {


                                            addLesson();

                                        }
                                        else {

                                            Toast.makeText(AddLessonPlanActivity.this,"enter a description",Toast.LENGTH_SHORT).show();

                                        }


                                    }
                                    else {

                                        Toast.makeText(AddLessonPlanActivity.this,"enter a topic",Toast.LENGTH_SHORT).show();

                                    }


                                }
                                else {

                                    Toast.makeText(AddLessonPlanActivity.this,"select date properly",Toast.LENGTH_SHORT).show();

                                }

                            }catch (Exception e)
                            {

                            }





                        }
                        else {

                            Toast.makeText(AddLessonPlanActivity.this,"select a  date",Toast.LENGTH_SHORT).show();


                        }


                    }
                    else {

                        Toast.makeText(AddLessonPlanActivity.this,"select subject",Toast.LENGTH_SHORT).show();


                    }






            }
        });
    }

    public void addLesson()
    {

        progressFragment = new ProgressFragment();
        progressFragment.show(getSupportFragmentManager(), "cjkk");

        Retrofit retrofitHelper = RetrofitHelper.getRetrofitInstance(AddLessonPlanActivity.this);

        WebInterfaceimpl webInterfaceimpl = retrofitHelper.create(WebInterfaceimpl.class);

        Call<JsonArray>jsonArrayCall=webInterfaceimpl.addLessonPlan(Constants.classid,subjectid,end_date,edtTopic.getText().toString(),edtDescription.getText().toString());
        jsonArrayCall.enqueue(new Callback<JsonArray>() {
            @Override
            public void onResponse(Call<JsonArray> call, Response<JsonArray> response) {
                progressFragment.dismiss();
                if(response.body()!=null)
                {

                    if(response.body().size()>0)
                    {


                       try{


                           JSONArray jsonArray=new JSONArray(response.body().toString());


                               JSONObject jsonObject=jsonArray.getJSONObject(0);
                               if(jsonObject.getInt("status")==1)
                               {
                                   Toast.makeText(AddLessonPlanActivity.this,jsonObject.getString("message"),Toast.LENGTH_SHORT).show();

                                   edtDescription.setText("");
                                   edtTopic.setText("");
                                   end_date="";
                                   txttoDate.setText("implementation date");
                                   txtSubject.setText("Select a subject");
                                   txtClass.setText("Select a class");

                                   classid="";
                                   subjectid="";

                               }
                               else {

                                   Toast.makeText(AddLessonPlanActivity.this,"failed",Toast.LENGTH_SHORT).show();

                               }





                       }catch (Exception e)
                       {

                       }



                    }
                    else {

                        Toast.makeText(AddLessonPlanActivity.this,"No data found",Toast.LENGTH_SHORT).show();

                    }


                }
                else {

                    Toast.makeText(AddLessonPlanActivity.this,"failed",Toast.LENGTH_SHORT).show();

                }
            }

            @Override
            public void onFailure(Call<JsonArray> call, Throwable t) {
                progressFragment.dismiss();

                if(!NetConnection.isConnected(AddLessonPlanActivity.this))
                {
                    Toast.makeText(AddLessonPlanActivity.this,"Check Internet connection",Toast.LENGTH_SHORT).show();
                }
            }
        });


    }


    public void showDatePicker() {
        final Calendar cldr = Calendar.getInstance();
       final int day = cldr.get(Calendar.DAY_OF_MONTH);
        final int month = cldr.get(Calendar.MONTH);
       final int year = cldr.get(Calendar.YEAR);

        DatePickerDialog datePickerDialog = new DatePickerDialog(AddLessonPlanActivity.this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {



                int m = i1 + 1;




                    end_date = i + "-" + m + "-" + i2;
                    txttoDate.setText(end_date);





                    }
        }, year, month, day);

        datePickerDialog.show();
    }


    public void getSubjects()
    {
        progressFragment = new ProgressFragment();
        progressFragment.show(getSupportFragmentManager(), "cjkk");

        Retrofit retrofitHelper = RetrofitHelper.getRetrofitInstance(AddLessonPlanActivity.this);

        WebInterfaceimpl webInterfaceimpl = retrofitHelper.create(WebInterfaceimpl.class);

        Call<List<Subjects>> listCall=webInterfaceimpl.getSubjects();

        listCall.enqueue(new Callback<List<Subjects>>() {
            @Override
            public void onResponse(Call<List<Subjects>> call, Response<List<Subjects>> response) {

                progressFragment.dismiss();
                if(response.body()!=null)
                {

                    if(response.body().size()>0)
                    {


                        showSubjectList(response.body());



                    }
                    else {

                        Toast.makeText(AddLessonPlanActivity.this,"No data found",Toast.LENGTH_SHORT).show();

                    }


                }
                else {

                    Toast.makeText(AddLessonPlanActivity.this,"No data found",Toast.LENGTH_SHORT).show();

                }


            }

            @Override
            public void onFailure(Call<List<Subjects>> call, Throwable t) {
                progressFragment.dismiss();

                if(!NetConnection.isConnected(AddLessonPlanActivity.this))
                {
                    Toast.makeText(AddLessonPlanActivity.this,"Check Internet connection",Toast.LENGTH_SHORT).show();
                }
            }
        });



    }




    public void showSubjectList(final List<Subjects> subjects)
    {
        List<String>strings=new ArrayList<>();
        AlertDialog.Builder builder = new AlertDialog.Builder(AddLessonPlanActivity.this);
        builder.setTitle("Choose a subject");
        ;

        for (Subjects subjects1:subjects
        ) {

            strings.add(subjects1.getUdcSubjectDetails());

        }



        builder.setItems(strings.toArray(new String[strings.size()]), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                dialog.dismiss();

                txtSubject.setText(subjects.get(which).getUdcSubjectDetails());

                subjectid=subjects.get(which).getSubjectId();

                // showLessonPlanlist();

            }
        });

// create and show the alert dialog
        AlertDialog dialog = builder.create();
        dialog.show();
    }


    public void getClasses()
    {
        progressFragment = new ProgressFragment();
        progressFragment.show(getSupportFragmentManager(), "cjkk");

        Retrofit retrofitHelper = RetrofitHelper.getRetrofitInstance(AddLessonPlanActivity.this);

        WebInterfaceimpl webInterfaceimpl = retrofitHelper.create(WebInterfaceimpl.class);

        Call<List<Classes>>listCall=webInterfaceimpl.getClasses();

        listCall.enqueue(new Callback<List<Classes>>() {
            @Override
            public void onResponse(Call<List<Classes>> call, Response<List<Classes>> response) {

                progressFragment.dismiss();
                if(response.body()!=null)
                {

                    if(response.body().size()>0)
                    {


                        showClassList(response.body());



                    }
                    else {

                        Toast.makeText(AddLessonPlanActivity.this,"No data found",Toast.LENGTH_SHORT).show();

                    }


                }
                else {

                    Toast.makeText(AddLessonPlanActivity.this,"No data found",Toast.LENGTH_SHORT).show();

                }

            }

            @Override
            public void onFailure(Call<List<Classes>> call, Throwable t) {

                progressFragment.dismiss();

                if(!NetConnection.isConnected(AddLessonPlanActivity.this))
                {
                    Toast.makeText(AddLessonPlanActivity.this,"Check Internet connection",Toast.LENGTH_SHORT).show();
                }

            }
        });

    }


    public void showClassList(final List<Classes>classes)
    {

        List<String>strings=new ArrayList<>();
        AlertDialog.Builder builder = new AlertDialog.Builder(AddLessonPlanActivity.this);
        builder.setTitle("Choose a student");
        ;

        for (Classes student:classes
        ) {

            strings.add(student.getUdcClassDetails());

        }



        builder.setItems(strings.toArray(new String[strings.size()]), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                dialog.dismiss();

                txtClass.setText(classes.get(which).getUdcClassDetails());

                classid=classes.get(which).getClassId();

                // showLessonPlanlist();

            }
        });

// create and show the alert dialog
        AlertDialog dialog = builder.create();
        dialog.show();
    }
}
