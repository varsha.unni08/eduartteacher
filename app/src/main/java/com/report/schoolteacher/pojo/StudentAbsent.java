package com.report.schoolteacher.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class StudentAbsent {

    @SerializedName("udc_absence_date")
    @Expose
    private String udcAbsenceDate;
    @SerializedName("udc_comments")
    @Expose
    private String udcComments;
    @SerializedName("udc_father_contact_mobile")
    @Expose
    private String udcFatherContactMobile;

    int index;
    String data="";

    boolean isleave=false;

    String date="";

    public StudentAbsent() {
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public boolean isIsleave() {
        return isleave;
    }

    public void setIsleave(boolean isleave) {
        this.isleave = isleave;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getUdcAbsenceDate() {
        return udcAbsenceDate;
    }

    public void setUdcAbsenceDate(String udcAbsenceDate) {
        this.udcAbsenceDate = udcAbsenceDate;
    }

    public String getUdcComments() {
        return udcComments;
    }

    public void setUdcComments(String udcComments) {
        this.udcComments = udcComments;
    }

    public String getUdcFatherContactMobile() {
        return udcFatherContactMobile;
    }

    public void setUdcFatherContactMobile(String udcFatherContactMobile) {
        this.udcFatherContactMobile = udcFatherContactMobile;
    }
}
