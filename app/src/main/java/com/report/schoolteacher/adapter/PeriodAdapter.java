package com.report.schoolteacher.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.report.schoolteacher.R;
import com.report.schoolteacher.pojo.Periods;
import com.report.schoolteacher.pojo.Timetable;

import java.util.List;

public class PeriodAdapter extends RecyclerView.Adapter<PeriodAdapter.PeriodHolder> {

    Context context;
    List<Timetable>periods;

    public PeriodAdapter(Context context, List<Timetable> periods) {
        this.context = context;
        this.periods = periods;
    }

    public class PeriodHolder extends RecyclerView.ViewHolder{

        TextView txtperiod;
        public PeriodHolder(@NonNull View itemView) {
            super(itemView);

            txtperiod=itemView.findViewById(R.id.txtperiod);
        }
    }

    @NonNull
    @Override
    public PeriodHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_periodadapter,parent,false);

        return new  PeriodHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull PeriodHolder holder, int position) {

        int p=position+1;

        holder.txtperiod.setText("Period no. : period"+p+"\nSubject : "+periods.get(position).getUdcSubjectDetails()+"\nClass : "+periods.get(position).getUdcClassDetails()+"\nDivision : "+periods.get(position).getUdcDivisionName());

    }

    @Override
    public int getItemCount() {
        return periods.size();
    }
}
