package com.report.schoolteacher.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Timetable {


    @SerializedName("udc_assign_id")
    @Expose
    private String udcAssignId;
    @SerializedName("udc_teacher_id")
    @Expose
    private String udcTeacherId;
    @SerializedName("udc_class")
    @Expose
    private String udcClass;
    @SerializedName("udc_division")
    @Expose
    private String udcDivision;
    @SerializedName("udc_subject")
    @Expose
    private String udcSubject;
    @SerializedName("udc_period_id")
    @Expose
    private String udcPeriodId;
    @SerializedName("udc_teacher_status")
    @Expose
    private String udcTeacherStatus;
    @SerializedName("udc_class_deleteid")
    @Expose
    private String udcClassDeleteid;
    @SerializedName("udc_teacher_name")
    @Expose
    private String udcTeacherName;
    @SerializedName("udc_class_details")
    @Expose
    private String udcClassDetails;
    @SerializedName("udc_division_name")
    @Expose
    private String udcDivisionName;
    @SerializedName("udc_subject_details")
    @Expose
    private String udcSubjectDetails;
    @SerializedName("udc_period")
    @Expose
    private String udcPeriod;
    @SerializedName("udc_weekday_name")
    @Expose
    private String udcWeekdayName;

    int selected=0;

    public Timetable() {
    }

    public String getUdcAssignId() {
        return udcAssignId;
    }

    public void setUdcAssignId(String udcAssignId) {
        this.udcAssignId = udcAssignId;
    }

    public String getUdcTeacherId() {
        return udcTeacherId;
    }

    public void setUdcTeacherId(String udcTeacherId) {
        this.udcTeacherId = udcTeacherId;
    }

    public String getUdcClass() {
        return udcClass;
    }

    public void setUdcClass(String udcClass) {
        this.udcClass = udcClass;
    }

    public String getUdcDivision() {
        return udcDivision;
    }

    public void setUdcDivision(String udcDivision) {
        this.udcDivision = udcDivision;
    }

    public String getUdcSubject() {
        return udcSubject;
    }

    public void setUdcSubject(String udcSubject) {
        this.udcSubject = udcSubject;
    }

    public String getUdcPeriodId() {
        return udcPeriodId;
    }

    public void setUdcPeriodId(String udcPeriodId) {
        this.udcPeriodId = udcPeriodId;
    }

    public String getUdcTeacherStatus() {
        return udcTeacherStatus;
    }

    public void setUdcTeacherStatus(String udcTeacherStatus) {
        this.udcTeacherStatus = udcTeacherStatus;
    }

    public String getUdcClassDeleteid() {
        return udcClassDeleteid;
    }

    public void setUdcClassDeleteid(String udcClassDeleteid) {
        this.udcClassDeleteid = udcClassDeleteid;
    }

    public String getUdcTeacherName() {
        return udcTeacherName;
    }

    public void setUdcTeacherName(String udcTeacherName) {
        this.udcTeacherName = udcTeacherName;
    }

    public String getUdcClassDetails() {
        return udcClassDetails;
    }

    public void setUdcClassDetails(String udcClassDetails) {
        this.udcClassDetails = udcClassDetails;
    }

    public String getUdcDivisionName() {
        return udcDivisionName;
    }

    public void setUdcDivisionName(String udcDivisionName) {
        this.udcDivisionName = udcDivisionName;
    }

    public String getUdcSubjectDetails() {
        return udcSubjectDetails;
    }

    public void setUdcSubjectDetails(String udcSubjectDetails) {
        this.udcSubjectDetails = udcSubjectDetails;
    }

    public String getUdcPeriod() {
        return udcPeriod;
    }

    public void setUdcPeriod(String udcPeriod) {
        this.udcPeriod = udcPeriod;
    }

    public String getUdcWeekdayName() {
        return udcWeekdayName;
    }

    public void setUdcWeekdayName(String udcWeekdayName) {
        this.udcWeekdayName = udcWeekdayName;
    }

    public int getSelected() {
        return selected;
    }

    public void setSelected(int selected) {
        this.selected = selected;
    }
}
