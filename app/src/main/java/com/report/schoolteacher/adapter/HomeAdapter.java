package com.report.schoolteacher.adapter;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.core.app.ActivityCompat;
import androidx.core.app.ActivityOptionsCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.report.schoolteacher.AppLiterals.Constants;
import com.report.schoolteacher.R;
import com.report.schoolteacher.activities.AttendanceActivity;
import com.report.schoolteacher.activities.CircularActivity;
import com.report.schoolteacher.activities.ComplaintsActivity;
import com.report.schoolteacher.activities.EventsActivity;
import com.report.schoolteacher.activities.ExamActivity;
import com.report.schoolteacher.activities.LeaveActivity;
import com.report.schoolteacher.activities.LessonPlanActivity;
import com.report.schoolteacher.activities.TimetableActivity;
import com.report.schoolteacher.activities.VideosActivity;

//import com.report.schoolparents.AppLiterals.Constants;
//import com.report.schoolparents.R;
//import com.report.schoolparents.activities.AttendanceActivity;
//import com.report.schoolparents.activities.CommunicationActivity;
//import com.report.schoolparents.activities.EventsActivity;
//import com.report.schoolparents.activities.LessonPlanActivity;
//import com.report.schoolparents.activities.MarkListActivity;
//import com.report.schoolparents.activities.TimeTableActivity;

public class HomeAdapter extends RecyclerView.Adapter<HomeAdapter.HomeHolder> {


    Context context;

    public HomeAdapter(Context context) {
        this.context = context;
    }

    public class HomeHolder extends RecyclerView.ViewHolder{

     AppCompatImageView img;
         TextView txtdata;

        public HomeHolder(@NonNull View itemView) {
            super(itemView);

            img=itemView.findViewById(R.id.img);
            txtdata=itemView.findViewById(R.id.txtdata);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {


                    switch (getAdapterPosition())
                    {

//                        case 0:
//
//                            Intent intent00=new Intent(context, AttendanceActivity.class);
//                            context.startActivity(intent00);
//                            break;
//
//                        case 1:
//
//                            Intent i=new Intent(context, ExamActivity.class);
//                            context.startActivity(i);
//                            break;
//
                        case 0:

                            Intent intent33=new Intent(context, EventsActivity.class);
                            context.startActivity(intent33);
                            break;
//                        case 3:
//
//                            Intent intent2=new Intent(context, TimetableActivity.class);
//                            context.startActivity(intent2);
//                            break;
//                        case 4:
//
//                            Intent intent=new Intent(context, LessonPlanActivity.class);
//                            context.startActivity(intent);
//                            break;
//
//                        case 5:
//
//                            Intent intent5=new Intent(context, LeaveActivity.class);
//                            context.startActivity(intent5);
//                            break;
//
                        case 1:

                            Intent intent1=new Intent(context, VideosActivity.class);
                            context.startActivity(intent1);
                            break;
//
//                        case 7:
//
//                            Intent intent7=new Intent(context, CircularActivity.class);
//                            context.startActivity(intent7);
//                            break;
//
                        case 2:

                            Intent intent8=new Intent(context, ComplaintsActivity.class);
                            context.startActivity(intent8);
                            break;

                        case 3 :


                            Intent intent4=new Intent(context, TimetableActivity.class);
                            context.startActivity(intent4);


                            break;


                    }
//
//                        case 0:
//                           // context.startActivity();
//
//                            Intent intent=new Intent(context, AttendanceActivity.class);
//
//
//                            ActivityOptionsCompat options =
//
//                                    ActivityOptionsCompat.makeSceneTransitionAnimation((Activity) context,
//                                            view,   // Starting view
//                                            context.getString(R.string.transition)    // The String
//                                    );
//
//                            ActivityCompat.startActivity(context, intent, options.toBundle());
//
//
//                            break;
//
//                        case 1:
//                            //context.startActivity(new Intent(context, MarkListActivity.class));
//
//                            Intent intent1=new Intent(context, MarkListActivity.class);
//
//
//                            ActivityOptionsCompat options1 =
//
//                                    ActivityOptionsCompat.makeSceneTransitionAnimation((Activity) context,
//                                            view,   // Starting view
//                                            context.getString(R.string.transition)    // The String
//                                    );
//
//                            ActivityCompat.startActivity(context, intent1, options1.toBundle());
//
//                            break;
//
//                        case 2:
//                            //context.startActivity(new Intent(context, EventsActivity.class));
//
//
//                            Intent intent2=new Intent(context, EventsActivity.class);
//
//
//                            ActivityOptionsCompat options2 =
//
//                                    ActivityOptionsCompat.makeSceneTransitionAnimation((Activity) context,
//                                            view,   // Starting view
//                                            context.getString(R.string.transition)    // The String
//                                    );
//
//                            ActivityCompat.startActivity(context, intent2, options2.toBundle());
//
//                            break;
//                        case 3:
//                            //context.startActivity();
//
//                            Intent intent3=new Intent(context, TimeTableActivity.class);
//
//                            ActivityOptionsCompat options3 =
//
//                                    ActivityOptionsCompat.makeSceneTransitionAnimation((Activity) context,
//                                            view,   // Starting view
//                                            context.getString(R.string.transition)    // The String
//                                    );
//
//                            ActivityCompat.startActivity(context, intent3, options3.toBundle());
//
//                            break;
//                        case 4:
//                           // context.startActivity(new Intent(context, LessonPlanActivity.class));
//
//                            Intent intent4=new Intent(context, LessonPlanActivity.class);
//
//                            ActivityOptionsCompat options4 =
//
//                                    ActivityOptionsCompat.makeSceneTransitionAnimation((Activity) context,
//                                            view,   // Starting view
//                                            context.getString(R.string.transition)    // The String
//                                    );
//
//                            ActivityCompat.startActivity(context, intent4, options4.toBundle());
//
//                            break;
//
//                        case 5:
//                            //context.startActivity(new Intent(context, CommunicationActivity.class));
//
//                            Intent intent5=new Intent(context, CommunicationActivity.class);
//
//                            ActivityOptionsCompat options5 =
//
//                                    ActivityOptionsCompat.makeSceneTransitionAnimation((Activity) context,
//                                            view,   // Starting view
//                                            context.getString(R.string.transition)    // The String
//                                    );
//
//                            ActivityCompat.startActivity(context, intent5, options5.toBundle());
//
//                            break;
//
//
//
//
//                    }
                }
            });
        }
    }

    @NonNull
    @Override
    public HomeHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View v= LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_homeadapter, parent, false);


        return new HomeHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull HomeHolder holder, int position) {

        holder.img.setImageResource(Constants.secondarr_img[position]);

        holder.txtdata.setText(Constants.secondhomearr[position]);

    }

    @Override
    public int getItemCount() {
        return Constants.secondarr_img.length;
    }



}
