package com.report.schoolteacher.activities;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;

import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonArray;
import com.report.schoolteacher.R;
import com.report.schoolteacher.connectivity.NetConnection;
import com.report.schoolteacher.pojo.Classes;
import com.report.schoolteacher.pojo.Divisions;
import com.report.schoolteacher.pojo.Mark;
import com.report.schoolteacher.pojo.StudentByClass;
import com.report.schoolteacher.pojo.Subjects;
import com.report.schoolteacher.preferencehelper.PreferenceHelper;
import com.report.schoolteacher.progress.ProgressFragment;
import com.report.schoolteacher.webservice.RetrofitHelper;
import com.report.schoolteacher.webservice.WebInterfaceimpl;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class EditMarkActivity extends AppCompatActivity {

    Mark mark;

    ImageView imgback;

    Button btnSubmit;




    TextView txtNodata,txtClass,txtDivision;

    ImageView imgdropdownClass,imgDelete,imgdropdownsubject1;



    String classid="",divisionid="";

    ProgressFragment progressFragment;

    List<StudentByClass> studentByClasses;

    TextView txtSubject;

    ImageView imgdivision;

    String subjectid="";


    TextView txtExamSelection;
    ImageView imgselectExam;


    TextView txtTerm;
    ImageView imgSelectTerm;

    TextView txtStartdate;
    ImageView imgpickFromDate;

    String end_date="";

    String examtype="",term="";

    EditText edtTotalMarks,edtComments;

    List<Classes> classes;

    List<Subjects> subjects;

    List<Divisions>divisions;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_mark);
        getSupportActionBar().hide();
        mark=(Mark)getIntent().getSerializableExtra("Mark");
        imgback=findViewById(R.id.imgback);
        btnSubmit=findViewById(R.id.btnSubmit);
        txtNodata=findViewById(R.id.txtNodata);
        txtClass=findViewById(R.id.txtClass);
        txtDivision=findViewById(R.id.txtDivision);
        edtComments=findViewById(R.id.edtComments);


        classes=new ArrayList<>();
        subjects=new ArrayList<>();
        divisions=new ArrayList<>();





        imgdropdownClass=findViewById(R.id.imgdropdownClass);
        imgdivision=findViewById(R.id.imgdivision);
        txtSubject=findViewById(R.id.txtSubject);
        imgdropdownsubject1=findViewById(R.id.imgdropdownsubject);
        txtExamSelection=findViewById(R.id.txtExamSelection);
        imgselectExam=findViewById(R.id.imgselectExam);
        txtTerm=findViewById(R.id.txtTerm);
        imgSelectTerm=findViewById(R.id.imgSelectTerm);
        txtStartdate=findViewById(R.id.txtStartdate);
        imgpickFromDate=findViewById(R.id.imgpickFromDate);
        edtTotalMarks=findViewById(R.id.edtTotalMarks);
        imgDelete=findViewById(R.id.imgDelete);


        if(mark!=null)
        {
            setMarkDetails();
        }




        imgback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                onBackPressed();
            }
        });

        txtClass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                showClassList(classes);
            }
        });

        imgdropdownClass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showClassList(classes);
            }
        });

        txtSubject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                showSubjectList(subjects);

            }
        });

        imgdropdownsubject1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                showSubjectList(subjects);
            }
        });

        imgdivision.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                showDivisionList(divisions);


            }
        });

        txtDivision.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDivisionList(divisions);
            }
        });

        txtStartdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                showDatePicker();
            }
        });

        imgpickFromDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDatePicker();
            }
        });

        imgselectExam.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                showExamList();

            }
        });
        txtExamSelection.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showExamList();
            }
        });


        imgSelectTerm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showTermList();

            }
        });

        txtTerm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showTermList();
            }
        });
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                if(!classid.equals(""))
                {
                    if(!divisionid.equals(""))
                    {

                        if(!subjectid.equals(""))
                        {
                            if(!examtype.equals(""))
                            {
                                if(!term.equals(""))
                                {

                                    if(!end_date.equals(""))
                                    {

                                        final Calendar cldr = Calendar.getInstance();
                                        final int day = cldr.get(Calendar.DAY_OF_MONTH);
                                        final int month = cldr.get(Calendar.MONTH);
                                        final int year = cldr.get(Calendar.YEAR);

                                        Date sta_date = null;
                                        Date end_dat = null;

                                        int m1=month+1;

                                        String currentdate=year+"-"+m1+"-"+day;


                                        try {

                                            DateFormat dateFormat = new SimpleDateFormat("yyyy-mm-dd");
                                            sta_date = dateFormat.parse(currentdate);
                                            end_dat = dateFormat.parse(end_date);

                                            if (end_dat.before(sta_date)) {

                                                if(!edtTotalMarks.getText().toString().equals(""))
                                                {

                                                    if(!edtComments.getText().toString().equals(""))
                                                    {





                                                        editMark();




                                                    }
                                                    else {

                                                        Toast.makeText(EditMarkActivity.this,"enter comments",Toast.LENGTH_SHORT).show();

                                                    }







                                                }
                                                else {

                                                    Toast.makeText(EditMarkActivity.this,"enter total marks",Toast.LENGTH_SHORT).show();

                                                }


                                            }
                                            else {

                                                Toast.makeText(EditMarkActivity.this,"select date properly",Toast.LENGTH_SHORT).show();

                                            }

                                        }catch (Exception e)
                                        {

                                        }



                                    }
                                    else {

                                        Toast.makeText(EditMarkActivity.this,"Select exam date",Toast.LENGTH_SHORT).show();
                                    }


                                }
                                else {

                                    Toast.makeText(EditMarkActivity.this,"Select a term",Toast.LENGTH_SHORT).show();
                                }

                            }
                            else {

                                Toast.makeText(EditMarkActivity.this,"Select a exam type",Toast.LENGTH_SHORT).show();
                            }

                        }
                        else {

                            Toast.makeText(EditMarkActivity.this,"Select a subject",Toast.LENGTH_SHORT).show();
                        }


                    }
                    else {

                        Toast.makeText(EditMarkActivity.this,"Select division",Toast.LENGTH_SHORT).show();
                    }

                }
                else {

                    Toast.makeText(EditMarkActivity.this,"Select class",Toast.LENGTH_SHORT).show();
                }
            }
        });

        imgDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {



                AlertDialog.Builder builder=new AlertDialog.Builder(EditMarkActivity.this);
                builder.setTitle(R.string.app_name);
                builder.setMessage("Do you want to delete the mark ?");
                builder.setPositiveButton("yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();


                        deleteMark();


                    }
                });

                builder.setNegativeButton("no", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        dialogInterface.dismiss();

                    }
                });
                builder.show();











            }
        });



    }


    public void deleteMark()
    {

        progressFragment = new ProgressFragment();
        progressFragment.show(getSupportFragmentManager(), "cjkk");

        Retrofit retrofitHelper = RetrofitHelper.getRetrofitInstance(EditMarkActivity.this);

        WebInterfaceimpl webInterfaceimpl = retrofitHelper.create(WebInterfaceimpl.class);



        Call<JsonArray>jsonArrayCall=webInterfaceimpl.deleteMark(mark.getUdcTestId(),mark.getExam());
        jsonArrayCall.enqueue(new Callback<JsonArray>() {
            @Override
            public void onResponse(Call<JsonArray> call, Response<JsonArray> response) {
                progressFragment.dismiss();
                if(response.body()!=null)
                {

                    try{

                        JSONArray jsonArray=new JSONArray(response.body().toString());
                        if(jsonArray.length()>0)
                        {
                            JSONObject jsonObject=jsonArray.getJSONObject(0);


                            if(jsonObject.getInt("status")==1)
                            {


                                Toast.makeText(EditMarkActivity.this,jsonObject.getString("message"),Toast.LENGTH_SHORT).show();


                                finish();


                            }
                            else {
                                Toast.makeText(EditMarkActivity.this,jsonObject.getString("message"),Toast.LENGTH_SHORT).show();
                            }



                        }



                    }catch (Exception e)
                    {

                    }


                }
                else {



                    Toast.makeText(EditMarkActivity.this,"failed to delete message",Toast.LENGTH_SHORT).show();

                }
            }

            @Override
            public void onFailure(Call<JsonArray> call, Throwable t) {
                progressFragment.dismiss();

                if (!NetConnection.isConnected(EditMarkActivity.this)) {
                    Toast.makeText(EditMarkActivity.this, "Check Internet connection", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    public void editMark()
    {
        progressFragment = new ProgressFragment();
        progressFragment.show(getSupportFragmentManager(), "cjkk");

        Retrofit retrofitHelper = RetrofitHelper.getRetrofitInstance(EditMarkActivity.this);

        WebInterfaceimpl webInterfaceimpl = retrofitHelper.create(WebInterfaceimpl.class);



        Call<JsonArray>jsonArrayCall=webInterfaceimpl.editMarks(classid,divisionid,examtype,term,mark.getUdcTmarksTotal(),mark.getUdcTestDate(),edtTotalMarks.getText().toString(),edtComments.getText().toString(),subjectid,mark.getUdcTestId());
        jsonArrayCall.enqueue(new Callback<JsonArray>() {
            @Override
            public void onResponse(Call<JsonArray> call, Response<JsonArray> response) {
                progressFragment.dismiss();
                if(response.body()!=null)
                {

                    try{

                        JSONArray jsonArray=new JSONArray(response.body().toString());
                        if(jsonArray.length()>0)
                        {
                            JSONObject jsonObject=jsonArray.getJSONObject(0);


                            if(jsonObject.getInt("status")==1)
                            {


                                Toast.makeText(EditMarkActivity.this,jsonObject.getString("message"),Toast.LENGTH_SHORT).show();


                                finish();


                            }
                            else {
                                Toast.makeText(EditMarkActivity.this,jsonObject.getString("message"),Toast.LENGTH_SHORT).show();
                            }



                        }



                    }catch (Exception e)
                    {

                    }


                }
                else {



                    Toast.makeText(EditMarkActivity.this,"failed to delete message",Toast.LENGTH_SHORT).show();

                }
            }

            @Override
            public void onFailure(Call<JsonArray> call, Throwable t) {
                progressFragment.dismiss();

                if (!NetConnection.isConnected(EditMarkActivity.this)) {
                    Toast.makeText(EditMarkActivity.this, "Check Internet connection", Toast.LENGTH_SHORT).show();
                }
            }
        });


    }


    public void setMarkDetails()
    {

        getClasses();

        getSubjects();

         String arr[]={"Quarterly","Half yearly","Annual exam"};

         String arr_exam[]={" Assessment tests","Main Exam"};

         term=mark.getTerm();
         examtype=mark.getExam();

         int t= Integer.parseInt(term);
         t--;


         int e=Integer.parseInt(examtype);
                      e--;
         txtTerm.setText(arr[t]);
         txtExamSelection.setText(arr_exam[e]);
         end_date=mark.getUdcTestDate();

         edtTotalMarks.setText(mark.getUdcTmarksScored());
         txtStartdate.setText(mark.getUdcTestDate());

         edtComments.setText(mark.getUdcTeachersTreview());

         if(progressFragment!=null)
         {
             progressFragment.dismiss();
         }






    }

    public void showDatePicker() {
        final Calendar cldr = Calendar.getInstance();
        final int day = cldr.get(Calendar.DAY_OF_MONTH);
        final int month = cldr.get(Calendar.MONTH);
        final int year = cldr.get(Calendar.YEAR);

        DatePickerDialog datePickerDialog = new DatePickerDialog(EditMarkActivity.this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {



                int m = i1 + 1;




                end_date = i + "-" + m + "-" + i2;
                txtStartdate.setText(end_date);





            }
        }, year, month, day);

        datePickerDialog.show();
    }


    public void getSubjects() {
//        progressFragment = new ProgressFragment();
//        progressFragment.show(getSupportFragmentManager(), "cjkk");

        Retrofit retrofitHelper = RetrofitHelper.getRetrofitInstance(EditMarkActivity.this);

        WebInterfaceimpl webInterfaceimpl = retrofitHelper.create(WebInterfaceimpl.class);

        Call<List<Subjects>> listCall = webInterfaceimpl.getSubjects();

        listCall.enqueue(new Callback<List<Subjects>>() {
            @Override
            public void onResponse(Call<List<Subjects>> call, Response<List<Subjects>> response) {

//                progressFragment.dismiss();
                if (response.body() != null) {

                    if (response.body().size() > 0) {


                        //showSubjectList(response.body());
                        subjects.addAll(response.body());

                        for (Subjects classes:response.body()
                        ) {

                            if(classes.getSubjectId().equals(mark.getSubjectid()))
                            {
                                txtSubject.setText(classes.getUdcSubjectDetails());
                                subjectid=classes.getSubjectId();
                            }

                        }


                    } else {

                        Toast.makeText(EditMarkActivity.this, "No data found", Toast.LENGTH_SHORT).show();

                    }


                } else {

                    Toast.makeText(EditMarkActivity.this, "No data found", Toast.LENGTH_SHORT).show();

                }


            }

            @Override
            public void onFailure(Call<List<Subjects>> call, Throwable t) {
//                progressFragment.dismiss();

                if (!NetConnection.isConnected(EditMarkActivity.this)) {
                    Toast.makeText(EditMarkActivity.this, "Check Internet connection", Toast.LENGTH_SHORT).show();
                }
            }
        });


    }




    public void getClasses() {

//        progressFragment = new ProgressFragment();
//        progressFragment.show(getSupportFragmentManager(), "djaisjfk");

        Retrofit retrofitHelper = RetrofitHelper.getRetrofitInstance(EditMarkActivity.this);

        WebInterfaceimpl webInterfaceimpl = retrofitHelper.create(WebInterfaceimpl.class);

        Call<List<Classes>> listCall = webInterfaceimpl.getClasses();

        listCall.enqueue(new Callback<List<Classes>>() {
            @Override
            public void onResponse(Call<List<Classes>> call, Response<List<Classes>> response) {

//                progressFragment.dismiss();

                if (response.body() != null) {

                    if (response.body().size() > 0) {


                        classes.addAll(response.body());

                        for (Classes classes:response.body()
                             ) {

                            if(classes.getClassId().equals(mark.getClassid()))
                            {
                                txtClass.setText(classes.getUdcClassDetails());
                                classid=classes.getClassId();
                                getDivisions();
                            }

                        }


                    } else {

                        Toast.makeText(EditMarkActivity.this, "No data found", Toast.LENGTH_SHORT).show();

                    }


                } else {

                    Toast.makeText(EditMarkActivity.this, "No data found", Toast.LENGTH_SHORT).show();

                }

            }

            @Override
            public void onFailure(Call<List<Classes>> call, Throwable t) {

//                progressFragment.dismiss();

                if (!NetConnection.isConnected(EditMarkActivity.this)) {
                    Toast.makeText(EditMarkActivity.this, "Check Internet connection", Toast.LENGTH_SHORT).show();
                }

            }
        });

    }



    public void getDivisions()
    {

//        progressFragment=new ProgressFragment();
//        progressFragment.show(getSupportFragmentManager(),"dgl");

        Retrofit retrofitHelper = RetrofitHelper.getRetrofitInstance(EditMarkActivity.this);

        WebInterfaceimpl webInterfaceimpl = retrofitHelper.create(WebInterfaceimpl.class);

        Call<List<Divisions>> listCall=webInterfaceimpl.getDivisions(classid);

        listCall.enqueue(new Callback<List<Divisions>>() {
            @Override
            public void onResponse(Call<List<Divisions>> call, Response<List<Divisions>> response) {
//                progressFragment.dismiss();

                if(response.body()!=null)
                {

                    if(response.body().size()>0)
                    {


                      //  showDivisionList(response.body());
                        for (Divisions divisions:response.body()
                             ) {

                            if(divisions.getUdcDivision().equals(mark.getDivision()))
                            {
                                divisionid=mark.getDivision();
                                txtDivision.setText(divisions.getUdcDivisionName());
                            }

                        }



                    }
                    else {

                        Toast.makeText(EditMarkActivity.this,"No data found",Toast.LENGTH_SHORT).show();

                    }


                }
                else {

                    Toast.makeText(EditMarkActivity.this,"No data found",Toast.LENGTH_SHORT).show();

                }


            }

            @Override
            public void onFailure(Call<List<Divisions>> call, Throwable t) {
//                progressFragment.dismiss();

                if(!NetConnection.isConnected(EditMarkActivity.this))
                {
                    Toast.makeText(EditMarkActivity.this,"Check Internet connection",Toast.LENGTH_SHORT).show();
                }
            }
        });



    }


    public void showClassList(final List<Classes> classes) {

        List<String> strings = new ArrayList<>();
        AlertDialog.Builder builder = new AlertDialog.Builder(EditMarkActivity.this);
        builder.setTitle("Choose a class");
        ;

        for (Classes student : classes
        ) {

            strings.add(student.getUdcClassDetails());

        }


        builder.setItems(strings.toArray(new String[strings.size()]), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                dialog.dismiss();

                txtClass.setText(classes.get(which).getUdcClassDetails());

                classid = classes.get(which).getClassId();

                // showLessonPlanlist();

            }
        });

// create and show the alert dialog
        AlertDialog dialog = builder.create();
        dialog.show();
    }


    public void showSubjectList(final List<Subjects> subjects)
    {
        List<String>strings=new ArrayList<>();
        AlertDialog.Builder builder = new AlertDialog.Builder(EditMarkActivity.this);
        builder.setTitle("Choose a subject");
        ;

        for (Subjects subjects1:subjects
        ) {

            strings.add(subjects1.getUdcSubjectDetails());

        }



        builder.setItems(strings.toArray(new String[strings.size()]), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                dialog.dismiss();

                txtSubject.setText(subjects.get(which).getUdcSubjectDetails());

                subjectid=subjects.get(which).getSubjectId();

                // showLessonPlanlist();

            }
        });

// create and show the alert dialog
        AlertDialog dialog = builder.create();
        dialog.show();
    }


    public void showExamList()
    {


        final String arr[]={" Assessment tests","Main Exam"};

        AlertDialog.Builder builder = new AlertDialog.Builder(EditMarkActivity.this);
        builder.setTitle("Choose an Exam");
        ;



        builder.setItems(arr, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                dialog.dismiss();

                txtExamSelection.setText(arr[which]);

                int t=which+1;

                examtype=String.valueOf(t);

            }
        });

// create and show the alert dialog
        AlertDialog dialog = builder.create();
        dialog.show();
    }


    public void showTermList()
    {


        final String arr[]={"Quarterly","Half yearly","Annual exam"};

        AlertDialog.Builder builder = new AlertDialog.Builder(EditMarkActivity.this);
        builder.setTitle("Choose a Term");
        ;



        builder.setItems(arr, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                dialog.dismiss();

                txtTerm.setText(arr[which]);

                term=String.valueOf(which);

            }
        });

// create and show the alert dialog
        AlertDialog dialog = builder.create();
        dialog.show();
    }



    public void showDivisionList(final List<Divisions> divisions)
    {
        List<String>strings=new ArrayList<>();
        AlertDialog.Builder builder = new AlertDialog.Builder(EditMarkActivity.this);
        builder.setTitle("Choose a division");
        ;

        for (Divisions divi:divisions
        ) {

            strings.add(divi.getUdcDivisionName());

        }



        builder.setItems(strings.toArray(new String[strings.size()]), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                dialog.dismiss();

                txtDivision.setText(divisions.get(which).getUdcDivisionName());

                divisionid=divisions.get(which).getUdcDivision();

                // showLessonPlanlist();

            }
        });

// create and show the alert dialog
        AlertDialog dialog = builder.create();
        dialog.show();
    }

}
