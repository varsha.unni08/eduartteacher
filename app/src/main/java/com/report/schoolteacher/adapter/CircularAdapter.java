package com.report.schoolteacher.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.report.schoolteacher.R;
import com.report.schoolteacher.pojo.Circular;

import java.util.List;

public class CircularAdapter extends RecyclerView.Adapter<CircularAdapter.CircularHolder> {

    List<Circular>circulars;
    Context context;

    public CircularAdapter(List<Circular> circulars, Context context) {
        this.circulars = circulars;
        this.context = context;
    }

    public class CircularHolder extends RecyclerView.ViewHolder{
        TextView txtdetails;

        public CircularHolder(@NonNull View itemView) {
            super(itemView);

            txtdetails=itemView.findViewById(R.id.txtdetails);
        }
    }

    @NonNull
    @Override
    public CircularHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_student_leaveadapter,parent,false);


        return new CircularHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CircularHolder holder, int position) {

        holder.txtdetails.setText(circulars.get(position).getUdcCircularHead()+"\n"+circulars.get(position).getUdcCircularContent().trim()+"\n"+circulars.get(position).getUdcDatePrep());

    }

    @Override
    public int getItemCount() {
        return circulars.size();
    }
}
