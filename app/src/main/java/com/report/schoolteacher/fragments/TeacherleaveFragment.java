package com.report.schoolteacher.fragments;


import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.report.schoolteacher.R;
import com.report.schoolteacher.activities.AddTeacherLeaveActivity;
import com.report.schoolteacher.adapter.TeacherLeaveAdapter;
import com.report.schoolteacher.connectivity.NetConnection;
import com.report.schoolteacher.pojo.TeacherLeave;
import com.report.schoolteacher.pojo.TeacherLeaveStatus;
import com.report.schoolteacher.progress.ProgressFragment;
import com.report.schoolteacher.webservice.RetrofitHelper;
import com.report.schoolteacher.webservice.WebInterfaceimpl;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * A simple {@link Fragment} subclass.
 */
public class TeacherleaveFragment extends Fragment {


    public TeacherleaveFragment() {
        // Required empty public constructor
    }

    View view;

    TextView txtnodatafound;

    RecyclerView recycler_view;
    FloatingActionButton fab;

    ProgressFragment progressFragment;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view= inflater.inflate(R.layout.fragment_teacherleave, container, false);
        txtnodatafound=view.findViewById(R.id.txtnodatafound);
        recycler_view=view.findViewById(R.id.recycler_view);
        fab=view.findViewById(R.id.fab);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                startActivity(new Intent(getActivity(), AddTeacherLeaveActivity.class));
            }
        });

        getTeacherLeave();

        return view;
    }


    public void getTeacherLeave()
    {
        progressFragment = new ProgressFragment();
        progressFragment.show(getChildFragmentManager(), "cjkk");

        Retrofit retrofitHelper = RetrofitHelper.getRetrofitInstance(getActivity());

        WebInterfaceimpl webInterfaceimpl = retrofitHelper.create(WebInterfaceimpl.class);

        Call<TeacherLeaveStatus>listCall=webInterfaceimpl.getTeacherLeave();
        listCall.enqueue(new Callback<TeacherLeaveStatus>() {
            @Override
            public void onResponse(Call<TeacherLeaveStatus> call, Response<TeacherLeaveStatus> response) {
                progressFragment.dismiss();
                if (response.body() != null) {

                    if (response.body().getData()!=null ) {

                        if (response.body().getData().size() > 0) {


                            txtnodatafound.setVisibility(View.GONE);
                            recycler_view.setVisibility(View.VISIBLE);

                            recycler_view.setLayoutManager(new LinearLayoutManager(getActivity()));
                            recycler_view.setAdapter(new TeacherLeaveAdapter(getActivity(), response.body().getData()));


                        } else {
                            txtnodatafound.setVisibility(View.VISIBLE);
                            recycler_view.setVisibility(View.GONE);

                            Toast.makeText(getActivity(), "No data found", Toast.LENGTH_SHORT).show();

                        }
                    }
                    else {
                        txtnodatafound.setVisibility(View.VISIBLE);
                        recycler_view.setVisibility(View.GONE);

                        Toast.makeText(getActivity(), "No data found", Toast.LENGTH_SHORT).show();

                    }

                } else {
                    txtnodatafound.setVisibility(View.VISIBLE);
                    recycler_view.setVisibility(View.GONE);

                    Toast.makeText(getActivity(), "No data found", Toast.LENGTH_SHORT).show();

                }
            }

            @Override
            public void onFailure(Call<TeacherLeaveStatus> call, Throwable t) {
                progressFragment.dismiss();

                if (!NetConnection.isConnected(getActivity())) {
                    Toast.makeText(getActivity(), "Check Internet connection", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

}
