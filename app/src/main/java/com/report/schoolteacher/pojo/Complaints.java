package com.report.schoolteacher.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Complaints {
    @SerializedName("complaint")
    @Expose
    private String complaint;
    @SerializedName("parent")
    @Expose
    private String parent;
    @SerializedName("student")
    @Expose
    private String student;

    public Complaints() {
    }

    public String getComplaint() {
        return complaint;
    }

    public void setComplaint(String complaint) {
        this.complaint = complaint;
    }

    public String getParent() {
        return parent;
    }

    public void setParent(String parent) {
        this.parent = parent;
    }

    public String getStudent() {
        return student;
    }

    public void setStudent(String student) {
        this.student = student;
    }
}
