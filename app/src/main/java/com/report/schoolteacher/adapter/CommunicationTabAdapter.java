package com.report.schoolteacher.adapter;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;


import com.report.schoolteacher.fragments.CircularFragment;
import com.report.schoolteacher.fragments.MessagesFragment;

public class CommunicationTabAdapter extends FragmentPagerAdapter {

    String arr[]={"Circular","Messages"};


    public CommunicationTabAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {

        Fragment fragment=null;

        if(position==0)
        {

            fragment=new CircularFragment();
        }
        else if(position==1) {

            fragment=new MessagesFragment();

        }




        return fragment;
    }

    @Override
    public int getCount() {
        return 2;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return arr[position];
    }
}
