package com.report.schoolteacher.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TeacherLeave {


    @SerializedName("udc_teacherleave_id")
    @Expose
    private String udcTeacherleaveId;
    @SerializedName("udc_teacher_id")
    @Expose
    private String udcTeacherId;
    @SerializedName("udc_from_date")
    @Expose
    private String udcFromDate;
    @SerializedName("udc_to_date")
    @Expose
    private String udcToDate;
    @SerializedName("udc_reason")
    @Expose
    private String udcReason;
    @SerializedName("udc_accademic_year")
    @Expose
    private String udcAccademicYear;
    @SerializedName("udc_delete_id")
    @Expose
    private String udcDeleteId;

    public TeacherLeave() {
    }

    public String getUdcTeacherleaveId() {
        return udcTeacherleaveId;
    }

    public void setUdcTeacherleaveId(String udcTeacherleaveId) {
        this.udcTeacherleaveId = udcTeacherleaveId;
    }

    public String getUdcTeacherId() {
        return udcTeacherId;
    }

    public void setUdcTeacherId(String udcTeacherId) {
        this.udcTeacherId = udcTeacherId;
    }

    public String getUdcFromDate() {
        return udcFromDate;
    }

    public void setUdcFromDate(String udcFromDate) {
        this.udcFromDate = udcFromDate;
    }

    public String getUdcToDate() {
        return udcToDate;
    }

    public void setUdcToDate(String udcToDate) {
        this.udcToDate = udcToDate;
    }

    public String getUdcReason() {
        return udcReason;
    }

    public void setUdcReason(String udcReason) {
        this.udcReason = udcReason;
    }

    public String getUdcAccademicYear() {
        return udcAccademicYear;
    }

    public void setUdcAccademicYear(String udcAccademicYear) {
        this.udcAccademicYear = udcAccademicYear;
    }

    public String getUdcDeleteId() {
        return udcDeleteId;
    }

    public void setUdcDeleteId(String udcDeleteId) {
        this.udcDeleteId = udcDeleteId;
    }
}
