package com.report.schoolteacher.fragments;


import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.report.schoolteacher.R;
import com.report.schoolteacher.activities.CircularActivity;
import com.report.schoolteacher.adapter.CircularAdapter;
import com.report.schoolteacher.connectivity.NetConnection;
import com.report.schoolteacher.pojo.Circular;
import com.report.schoolteacher.pojo.CircularStatus;
import com.report.schoolteacher.progress.ProgressFragment;
import com.report.schoolteacher.webservice.RetrofitHelper;
import com.report.schoolteacher.webservice.WebInterfaceimpl;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * A simple {@link Fragment} subclass.
 */
public class CircularFragment extends Fragment {


    public CircularFragment() {
        // Required empty public constructor
    }

    View view;

    RecyclerView recycler_view;

    TextView txtnodatafound;

    ProgressFragment progressFragment;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view= inflater.inflate(R.layout.fragment_circular, container, false);

        txtnodatafound=view.findViewById(R.id.txtnodatafound);
        recycler_view=view.findViewById(R.id.recycler_view);

        getCirculars();

        return view;
    }


    public void getCirculars()
    {
        progressFragment = new ProgressFragment();
        progressFragment.show(getFragmentManager(), "cjkk");

        Retrofit retrofitHelper = RetrofitHelper.getRetrofitInstance(getActivity());

        WebInterfaceimpl webInterfaceimpl = retrofitHelper.create(WebInterfaceimpl.class);

        Call<CircularStatus> listCall=webInterfaceimpl.getCircular();

        listCall.enqueue(new Callback<CircularStatus>() {
            @Override
            public void onResponse(Call<CircularStatus> call, Response<CircularStatus> response) {
                progressFragment.dismiss();
                if(response.body()!=null)
                {

                    if(response.body().getData().size()>0)
                    {

                        txtnodatafound.setVisibility(View.GONE);
                        recycler_view.setVisibility(View.VISIBLE);

                        recycler_view.setLayoutManager(new LinearLayoutManager(getActivity()));
                        recycler_view.setAdapter(new CircularAdapter(response.body().getData(),getActivity()));




                    }
                    else {

                        txtnodatafound.setVisibility(View.VISIBLE);
                        recycler_view.setVisibility(View.GONE);

                        Toast.makeText(getActivity(),"No data found",Toast.LENGTH_SHORT).show();

                    }


                }
                else {

                    txtnodatafound.setVisibility(View.VISIBLE);
                    recycler_view.setVisibility(View.GONE);

                    Toast.makeText(getActivity(),"No data found",Toast.LENGTH_SHORT).show();

                }
            }

            @Override
            public void onFailure(Call<CircularStatus> call, Throwable t) {
                progressFragment.dismiss();

                if(!NetConnection.isConnected(getActivity()))
                {
                    Toast.makeText(getActivity(),"Check Internet connection",Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

}
