package com.report.schoolteacher.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class StudentByClass implements Serializable {
    @SerializedName("udc_student_id")
    @Expose
    private String udcStudentId;
    @SerializedName("udc_student_name")
    @Expose
    private String udcStudentName;
    @SerializedName("udc_student_photo_path")
    @Expose
    private String udcStudentPhotoPath;

    int markadded=0;

    public StudentByClass() {
    }

    public int getMarkadded() {
        return markadded;
    }

    public void setMarkadded(int markadded) {
        this.markadded = markadded;
    }

    public String getUdcStudentPhotoPath() {
        return udcStudentPhotoPath;
    }

    public void setUdcStudentPhotoPath(String udcStudentPhotoPath) {
        this.udcStudentPhotoPath = udcStudentPhotoPath;
    }

    public String getUdcStudentId() {
        return udcStudentId;
    }

    public void setUdcStudentId(String udcStudentId) {
        this.udcStudentId = udcStudentId;
    }

    public String getUdcStudentName() {
        return udcStudentName;
    }

    public void setUdcStudentName(String udcStudentName) {
        this.udcStudentName = udcStudentName;
    }
}
