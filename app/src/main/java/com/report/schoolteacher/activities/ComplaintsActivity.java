package com.report.schoolteacher.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.report.schoolteacher.R;
import com.report.schoolteacher.adapter.ComplaintAdapter;
import com.report.schoolteacher.connectivity.NetConnection;
import com.report.schoolteacher.pojo.Complaints;
import com.report.schoolteacher.progress.ProgressFragment;
import com.report.schoolteacher.webservice.RetrofitHelper;
import com.report.schoolteacher.webservice.WebInterfaceimpl;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class ComplaintsActivity extends AppCompatActivity {


    ImageView imgback;

    ProgressFragment progressFragment;

    RecyclerView recycler_view;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_complaints);

        getSupportActionBar().hide();

        imgback=findViewById(R.id.imgback);
        recycler_view=findViewById(R.id.recycler_view);

        imgback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                onBackPressed();
            }
        });

        getComplaints();

    }

    public void getComplaints()
    {

        progressFragment = new ProgressFragment();
        progressFragment.show(getSupportFragmentManager(), "cjkk");

        Retrofit retrofitHelper = RetrofitHelper.getRetrofitInstance(ComplaintsActivity.this);

        WebInterfaceimpl webInterfaceimpl = retrofitHelper.create(WebInterfaceimpl.class);

        Call<List<Complaints>>listCall=webInterfaceimpl.getComplaints();

        listCall.enqueue(new Callback<List<Complaints>>() {
            @Override
            public void onResponse(Call<List<Complaints>> call, Response<List<Complaints>> response) {
                progressFragment.dismiss();
                if (response.body() != null) {

                    if (response.body().size() > 0) {

                        recycler_view.setVisibility(View.VISIBLE);

                        recycler_view.setLayoutManager(new LinearLayoutManager(ComplaintsActivity.this));
                        recycler_view.setAdapter(new ComplaintAdapter(ComplaintsActivity.this,response.body()));



                    } else {

                        recycler_view.setVisibility(View.GONE);
                        Toast.makeText(ComplaintsActivity.this, "No data found", Toast.LENGTH_SHORT).show();

                    }


                } else {

                    recycler_view.setVisibility(View.GONE);
                    Toast.makeText(ComplaintsActivity.this, "No data found", Toast.LENGTH_SHORT).show();

                }
            }

            @Override
            public void onFailure(Call<List<Complaints>> call, Throwable t) {
                progressFragment.dismiss();

                if (!NetConnection.isConnected(ComplaintsActivity.this)) {
                    Toast.makeText(ComplaintsActivity.this, "Check Internet connection", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}
