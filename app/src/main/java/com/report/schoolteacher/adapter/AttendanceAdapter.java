package com.report.schoolteacher.adapter;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;


import com.report.schoolteacher.R;
import com.report.schoolteacher.activities.AttendanceActivity;
import com.report.schoolteacher.activities.AttendancedetailsActivity;
import com.report.schoolteacher.pojo.Attendance;
import com.report.schoolteacher.pojo.GuardianData;
import com.report.schoolteacher.pojo.GuardianStatus;
import com.report.schoolteacher.pojo.StudentAbsent;
import com.report.schoolteacher.pojo.StudentLeaveStatus;
import com.report.schoolteacher.progress.ProgressFragment;
import com.report.schoolteacher.webservice.RetrofitHelper;
import com.report.schoolteacher.webservice.WebInterfaceimpl;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class AttendanceAdapter extends RecyclerView.Adapter<AttendanceAdapter.Attendanceholder> {

    Context context;

   List<StudentAbsent>attendances;
   String studentid;

    public AttendanceAdapter(Context context, List<StudentAbsent> attendances,String studentid) {
        this.context = context;
        this.attendances = attendances;
        this.studentid=studentid;
    }

    public class Attendanceholder extends RecyclerView.ViewHolder
    {

        TextView txt1;

        ImageView img;

        public Attendanceholder(@NonNull View itemView) {
            super(itemView);
            txt1=itemView.findViewById(R.id.txt1);
            img=itemView.findViewById(R.id.img);

        }
    }


    @NonNull
    @Override
    public Attendanceholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater=LayoutInflater.from(parent.getContext());
        View view=layoutInflater.inflate(R.layout.layout_attendance,parent,false);

        return new Attendanceholder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Attendanceholder holder, final int position) {

        if(attendances.get(position).getIndex()!=-1)
        {

            if(attendances.get(position).isIsleave()) {

                Typeface face = Typeface.createFromAsset(context.getAssets(),
                        "fonts/montserratsemibold.otf");

                holder.txt1.setTypeface(face);


                holder.img.setBackgroundResource(R.drawable.ic_close);
                holder.img.setVisibility(View.VISIBLE);

                holder.txt1.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {


                        final ProgressFragment    progressFragment = new ProgressFragment();
                        progressFragment.show(((AppCompatActivity)context).getSupportFragmentManager(), "cjkk");

                        Retrofit retrofitHelper = RetrofitHelper.getRetrofitInstance(context);

                        WebInterfaceimpl webInterfaceimpl = retrofitHelper.create(WebInterfaceimpl.class);


                        Call<GuardianStatus> jsonArrayCall=webInterfaceimpl.getGuardian(studentid);

                        jsonArrayCall.enqueue(new Callback<GuardianStatus>() {
                            @Override
                            public void onResponse(Call<GuardianStatus> call, Response<GuardianStatus> response) {
                                progressFragment.dismiss();

                                if(response.body().getStatus()==1)
                                {

                                  //  Toast.makeText(context, "failed", Toast.LENGTH_SHORT).show();

                                    final GuardianData gs=response.body().getData().get(0);

                                    final Dialog dialog=new Dialog(context);


                                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                                    dialog.setContentView(R.layout.layout_leavedetails);
                                    dialog.setCancelable(false);

                                    TextView txtcontact=dialog.findViewById(R.id.txtcontact);
                                    ImageView imgCall=dialog.findViewById(R.id.imgCall);

                                    TextView txt1=dialog.findViewById(R.id.txt1);

                                    Button btnClose=dialog.findViewById(R.id.btnClose);



                                    DisplayMetrics displayMetrics = new DisplayMetrics();
                                    ((AppCompatActivity)context).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
                                    int widthLcl = (int) context.getResources().getDimension(R.dimen.dimen300dp);
                                    int heightLcl = (int) context.getResources().getDimension(R.dimen.dimen400dp);

                                    dialog.getWindow().setLayout(widthLcl,heightLcl);


                                    btnClose.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {

                                            dialog.dismiss();
                                        }
                                    });

                                    txtcontact.setText(gs.getUdcFatherContactMobile());

                                    txt1.setText("Absence date : "+attendances.get(position).getUdcAbsenceDate());

                                    imgCall.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {



                                            Uri u = Uri.parse("tel:"+gs.getUdcFatherContactMobile());


                                            Intent i = new Intent(Intent.ACTION_DIAL, u);

                                            try {

                                                context.startActivity(i);
                                            } catch (SecurityException s) {

                                            }
                                        }
                                    });


                                    dialog.show();





                                }
                                else {

                                    Toast.makeText(context, "failed", Toast.LENGTH_SHORT).show();
                                }



                            }

                            @Override
                            public void onFailure(Call<GuardianStatus> call, Throwable t) {
                                progressFragment.dismiss();
                            }
                        });









                    }
                });


//                holder.txt1.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//
//                        StringBuilder sb=new StringBuilder();
//                        sb.append("Leave from : "+attendances.get(position).getUdcFromDate()+"\n");
//                        sb.append("Leave to : "+attendances.get(position).getUdcToDate()+"\n");
//                        sb.append("Reason : "+attendances.get(position).getUdcReason());
//
//
//
//                        AlertDialog.Builder builder=new AlertDialog.Builder(context);
//                        builder.setTitle(R.string.app_name);
//                        builder.setMessage(sb.toString());
//                        builder.setPositiveButton("ok", new DialogInterface.OnClickListener() {
//                            @Override
//                            public void onClick(DialogInterface dialogInterface, int i) {
//                                dialogInterface.dismiss();
//
//                            }
//                        });
//                        builder.show();
//
//                    }
//                });





            }


            holder.txt1.setText(attendances.get(position).getData());
        }
        else {


            holder.txt1.setText(attendances.get(position).getData());
        }

    }

    @Override
    public int getItemCount() {
        return attendances.size();
    }
}
