package com.report.schoolteacher.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.report.schoolteacher.R;
import com.report.schoolteacher.appconstants.Literals;
import com.report.schoolteacher.connectivity.NetConnection;
import com.report.schoolteacher.pojo.LoginToken;
import com.report.schoolteacher.preferencehelper.PreferenceHelper;
import com.report.schoolteacher.progress.ProgressFragment;
import com.report.schoolteacher.webservice.RetrofitHelperLogin;
import com.report.schoolteacher.webservice.WebInterfaceimpl;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity {

    Button btnlogin;

    EditText edtPhone,edtPassword;

    ProgressFragment progressFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        getSupportActionBar().hide();
        btnlogin=findViewById(R.id.btnlogin);

        edtPhone=findViewById(R.id.edtPhone);
        edtPassword=findViewById(R.id.edtPassword);


        btnlogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(!edtPhone.getText().toString().equals(""))
                {

                    if(!edtPassword.getText().toString().equals(""))
                    {


                        login();

                    }
                    else {

                        Toast.makeText(LoginActivity.this,"Enter password",Toast.LENGTH_SHORT).show();
                    }



                }
                else {

                    Toast.makeText(LoginActivity.this,"Enter username",Toast.LENGTH_SHORT).show();
                }



                //startActivity(new Intent(LoginActivity.this, HomeActivity.class));
            }
        });


    }

    public void login()
    {


        progressFragment=new ProgressFragment();
        progressFragment.show(getSupportFragmentManager(),"cjkk");


        WebInterfaceimpl webInterfaceimpl= RetrofitHelperLogin.getRetrofitInstance().create(WebInterfaceimpl.class);
        Call<LoginToken> jsonObjectCall=webInterfaceimpl.getLoginData(edtPhone.getText().toString(),edtPassword.getText().toString());
        jsonObjectCall.enqueue(new Callback<LoginToken>() {
            @Override
            public void onResponse(Call<LoginToken> call, Response<LoginToken> response) {
                progressFragment.dismiss();

                if(response.body()!=null)
                {

                if(response.body().getStatus()==1)
                {

                    new PreferenceHelper(LoginActivity.this).putData(Literals.Logintokenkey,response.body().getToken());


                    new PreferenceHelper(LoginActivity.this).putBooleanData(Literals.isRegistered,true);

                    startActivity(new Intent(LoginActivity.this,HomeActivity.class));
                    finish();

                }
                else {

                    Toast.makeText(LoginActivity.this,response.body().getMessage(),Toast.LENGTH_SHORT).show();

                }
                }
                else {

                    Toast.makeText(LoginActivity.this,"failed",Toast.LENGTH_SHORT).show();

                }






            }

            @Override
            public void onFailure(Call<LoginToken> call, Throwable t) {
                progressFragment.dismiss();


                if(!NetConnection.isConnected(LoginActivity.this))
                {
                    Toast.makeText(LoginActivity.this,"Check Internet connection",Toast.LENGTH_SHORT).show();
                }


            }
        });
    }
}
