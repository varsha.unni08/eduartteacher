package com.report.schoolteacher.fragments;


import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.Editable;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.report.schoolteacher.AppLiterals.Constants;
import com.report.schoolteacher.R;
import com.report.schoolteacher.activities.EditMarkActivity;
import com.report.schoolteacher.activities.StudentListActivity;
import com.report.schoolteacher.adapter.MarksAdapter;
import com.report.schoolteacher.adapter.StudentByClassAdapter;
import com.report.schoolteacher.connectivity.NetConnection;
import com.report.schoolteacher.pojo.Classes;
import com.report.schoolteacher.pojo.Divisions;
import com.report.schoolteacher.pojo.Mark;
import com.report.schoolteacher.pojo.StudentByClass;
import com.report.schoolteacher.pojo.Subjects;
import com.report.schoolteacher.progress.ProgressFragment;
import com.report.schoolteacher.webservice.RetrofitHelper;
import com.report.schoolteacher.webservice.WebInterfaceimpl;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * A simple {@link Fragment} subclass.
 */
public class MarklistFragment extends Fragment {


    public MarklistFragment() {
        // Required empty public constructor
    }


    View view;

    TextView txtExamSelection, txtTerm, txtClass, txtDivision, txtSubject;

    ImageView imgselectExam, imgSelectTerm, imgdropdownClass, imgdivision, imgdropdownsubject;

    Button btnSubmit;

    String term = "", examtype = "", classid = "", subjectid = "", divisionid = "";

    ProgressFragment progressFragment;

    Dialog dialog = null;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_marklist, container, false);

        txtExamSelection = view.findViewById(R.id.txtExamSelection);
        imgselectExam = view.findViewById(R.id.imgselectExam);


        txtTerm = view.findViewById(R.id.txtTerm);
        imgSelectTerm = view.findViewById(R.id.imgSelectTerm);


        txtClass = view.findViewById(R.id.txtClass);
        imgdropdownClass = view.findViewById(R.id.imgdropdownClass);


        txtDivision = view.findViewById(R.id.txtDivision);
        imgdivision = view.findViewById(R.id.imgdivision);


        imgdropdownsubject = view.findViewById(R.id.imgdropdownsubject);
        txtSubject = view.findViewById(R.id.txtSubject);


        btnSubmit = view.findViewById(R.id.btnSubmit);


        imgselectExam.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                showExamList();

            }
        });

        txtExamSelection.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                showExamList();

            }
        });


        txtTerm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                showTermList();

            }
        });

        imgSelectTerm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                showTermList();

            }
        });


        txtClass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                getClasses();

            }
        });

        imgdropdownClass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                getClasses();

            }
        });


        txtDivision.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                getDivisions();

            }
        });

        imgdivision.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                getDivisions();

            }
        });


        imgdropdownsubject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                getSubjects();

            }
        });

        txtSubject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                getSubjects();

            }
        });


        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                if (!examtype.equals("")) {
                    if (!term.equals("")) {


                                if (!subjectid.equals("")) {


                                    getMarks();


                                } else {

                                    Toast.makeText(getActivity(), "Select subject", Toast.LENGTH_SHORT).show();
                                }





                    } else {

                        Toast.makeText(getActivity(), "Select term", Toast.LENGTH_SHORT).show();
                    }


                } else {

                    Toast.makeText(getActivity(), "Select Exam", Toast.LENGTH_SHORT).show();
                }

            }
        });


        return view;
    }


    public void EditMark(Mark mark)
    {
        if(dialog!=null)
        {
            dialog.dismiss();
        }

        mark.setClassid(classid);
        mark.setDivision(divisionid);
        mark.setExam(examtype);
        mark.setTerm(term);
        mark.setSubjectid(subjectid);

        Intent intent=new Intent(getActivity(), EditMarkActivity.class);
        intent.putExtra("Mark",mark);

        startActivity(intent);




    }


    public void getMarks() {

//        progressFragment = new ProgressFragment();
//        progressFragment.show(getFragmentManager(), "djaisjfk");
//
//        Retrofit retrofitHelper = RetrofitHelper.getRetrofitInstance(getActivity());
//
//        WebInterfaceimpl webInterfaceimpl = retrofitHelper.create(WebInterfaceimpl.class);
//
//        Call<List<Mark>> listCall = webInterfaceimpl.getMarks(Constants.classid, Constants.divisionid, examtype, term, subjectid);
//        listCall.enqueue(new Callback<List<Mark>>() {
//            @Override
//            public void onResponse(Call<List<Mark>> call, Response<List<Mark>> response) {
//                progressFragment.dismiss();
//
//                if (response.body() != null) {
//
//                    if (response.body().size() > 0) {
//
//
//                        showMarkDialog(response.body());
//
//
//                    } else {
//
//                        Toast.makeText(getActivity(), "No data found", Toast.LENGTH_SHORT).show();
//
//                    }
//
//
//                } else {
//
//                    Toast.makeText(getActivity(), "No data found", Toast.LENGTH_SHORT).show();
//
//                }
//
//            }
//
//            @Override
//            public void onFailure(Call<List<Mark>> call, Throwable t) {
//                progressFragment.dismiss();
//
//                if (!NetConnection.isConnected(getActivity())) {
//                    Toast.makeText(getActivity(), "Check Internet connection", Toast.LENGTH_SHORT).show();
//                }
//
//            }
//        });
    }

//    public void showMarkDialog(final List<Mark> marks) {
//        dialog = new Dialog(getActivity());
//        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//        dialog.setContentView(R.layout.layout_marklist);
//
//        DisplayMetrics displayMetrics = new DisplayMetrics();
//        ((AppCompatActivity) getActivity()).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
//        int widthLcl = (int) (displayMetrics.widthPixels);
//        int heightLcl = (int) (displayMetrics.heightPixels);
//
//        dialog.getWindow().setLayout(widthLcl, heightLcl);
//
//
//        EditText edtSearch = dialog.findViewById(R.id.edtSearch);
//
//        LinearLayout layout_head = dialog.findViewById(R.id.layout_head);
//
//        TextView txtTotal = dialog.findViewById(R.id.txtTotal);
//
//        final RecyclerView recycler_view = dialog.findViewById(R.id.recycler_view);
//
//        recycler_view.setLayoutManager(new LinearLayoutManager(getActivity()));
//        recycler_view.setAdapter(new MarksAdapter(getActivity(), marks,MarklistFragment.this));
//
//        txtTotal.setText("Total Marks : " + marks.get(0).getUdcTmarksTotal());
//
//        edtSearch.addTextChangedListener(new TextWatcher() {
//            @Override
//            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
//
//            }
//
//            @Override
//            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
//
//
//                if (!charSequence.toString().equals("")) {
//
//
//                    if (marks.size() > 0) {
//
//                        List<Mark> Mark_search = new ArrayList<>();
//
//                        for (Mark s1 : marks
//                        ) {
//
//                            if (s1.getName().toUpperCase().contains(charSequence.toString().toUpperCase()) || s1.getName().toLowerCase().contains(charSequence.toString().toLowerCase())) {
//                                Mark_search.add(s1);
//                            }
//
//                        }
//
//                        MarksAdapter studentByClassAdapter = new MarksAdapter(getActivity(), Mark_search,MarklistFragment.this);
//
//                        recycler_view.setLayoutManager(new LinearLayoutManager(getActivity()));
//                        recycler_view.setAdapter(studentByClassAdapter);
//
//
//                    } else {
//
//                        Toast.makeText(getActivity(), "No data to search", Toast.LENGTH_SHORT).show();
//                    }
//
//
//                } else {
//
//                    MarksAdapter studentByClassAdapter = new MarksAdapter(getActivity(), marks,MarklistFragment.this);
//
//                    recycler_view.setLayoutManager(new LinearLayoutManager(getActivity()));
//                    recycler_view.setAdapter(studentByClassAdapter);
//
//                }
//
//
//            }
//
//            @Override
//            public void afterTextChanged(Editable editable) {
//
//            }
//        });
//
//
//        dialog.show();
//    }


    public void showSubjectList(final List<Subjects> subjects) {
        List<String> strings = new ArrayList<>();
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Choose a subject");
        ;

        for (Subjects subjects1 : subjects
        ) {

            strings.add(subjects1.getUdcSubjectDetails());

        }


        builder.setItems(strings.toArray(new String[strings.size()]), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                dialog.dismiss();

                txtSubject.setText(subjects.get(which).getUdcSubjectDetails());

                subjectid = subjects.get(which).getSubjectId();

                // showLessonPlanlist();

            }
        });

// create and show the alert dialog
        AlertDialog dialog = builder.create();
        dialog.show();
    }


    public void getClasses() {

        progressFragment = new ProgressFragment();
        progressFragment.show(getFragmentManager(), "djaisjfk");

        Retrofit retrofitHelper = RetrofitHelper.getRetrofitInstance(getActivity());

        WebInterfaceimpl webInterfaceimpl = retrofitHelper.create(WebInterfaceimpl.class);

        Call<List<Classes>> listCall = webInterfaceimpl.getClasses();

        listCall.enqueue(new Callback<List<Classes>>() {
            @Override
            public void onResponse(Call<List<Classes>> call, Response<List<Classes>> response) {

                progressFragment.dismiss();

                if (response.body() != null) {

                    if (response.body().size() > 0) {


                        showClassList(response.body());


                    } else {

                        Toast.makeText(getActivity(), "No data found", Toast.LENGTH_SHORT).show();

                    }


                } else {

                    Toast.makeText(getActivity(), "No data found", Toast.LENGTH_SHORT).show();

                }

            }

            @Override
            public void onFailure(Call<List<Classes>> call, Throwable t) {

                progressFragment.dismiss();

                if (!NetConnection.isConnected(getActivity())) {
                    Toast.makeText(getActivity(), "Check Internet connection", Toast.LENGTH_SHORT).show();
                }

            }
        });

    }


    public void showClassList(final List<Classes> classes) {

        List<String> strings = new ArrayList<>();
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Choose a class");
        ;

        for (Classes student : classes
        ) {

            strings.add(student.getUdcClassDetails());

        }


        builder.setItems(strings.toArray(new String[strings.size()]), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                dialog.dismiss();

                txtClass.setText(classes.get(which).getUdcClassDetails());

                classid = classes.get(which).getClassId();

                // showLessonPlanlist();

            }
        });

// create and show the alert dialog
        AlertDialog dialog = builder.create();
        dialog.show();
    }


    public void getSubjects() {
        progressFragment = new ProgressFragment();
        progressFragment.show(getFragmentManager(), "cjkk");

        Retrofit retrofitHelper = RetrofitHelper.getRetrofitInstance(getActivity());

        WebInterfaceimpl webInterfaceimpl = retrofitHelper.create(WebInterfaceimpl.class);

        Call<List<Subjects>> listCall = webInterfaceimpl.getSubjects();

        listCall.enqueue(new Callback<List<Subjects>>() {
            @Override
            public void onResponse(Call<List<Subjects>> call, Response<List<Subjects>> response) {

                progressFragment.dismiss();
                if (response.body() != null) {

                    if (response.body().size() > 0) {


                        showSubjectList(response.body());


                    } else {

                        Toast.makeText(getActivity(), "No data found", Toast.LENGTH_SHORT).show();

                    }


                } else {

                    Toast.makeText(getActivity(), "No data found", Toast.LENGTH_SHORT).show();

                }


            }

            @Override
            public void onFailure(Call<List<Subjects>> call, Throwable t) {
                progressFragment.dismiss();

                if (!NetConnection.isConnected(getActivity())) {
                    Toast.makeText(getActivity(), "Check Internet connection", Toast.LENGTH_SHORT).show();
                }
            }
        });


    }


    public void getDivisions() {

        progressFragment = new ProgressFragment();
        progressFragment.show(getFragmentManager(), "dgl");

        Retrofit retrofitHelper = RetrofitHelper.getRetrofitInstance(getActivity());

        WebInterfaceimpl webInterfaceimpl = retrofitHelper.create(WebInterfaceimpl.class);

        Call<List<Divisions>> listCall = webInterfaceimpl.getDivisions(classid);

        listCall.enqueue(new Callback<List<Divisions>>() {
            @Override
            public void onResponse(Call<List<Divisions>> call, Response<List<Divisions>> response) {
                progressFragment.dismiss();

                if (response.body() != null) {

                    if (response.body().size() > 0) {


                        showDivisionList(response.body());


                    } else {

                        Toast.makeText(getActivity(), "No data found", Toast.LENGTH_SHORT).show();

                    }


                } else {

                    Toast.makeText(getActivity(), "No data found", Toast.LENGTH_SHORT).show();

                }


            }

            @Override
            public void onFailure(Call<List<Divisions>> call, Throwable t) {
                progressFragment.dismiss();

                if (!NetConnection.isConnected(getActivity())) {
                    Toast.makeText(getActivity(), "Check Internet connection", Toast.LENGTH_SHORT).show();
                }
            }
        });


    }


    public void showExamList() {


        final String arr[] = {" Assessment tests", "Main Exam"};

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Choose an Exam");
        ;


        builder.setItems(arr, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                dialog.dismiss();

                txtExamSelection.setText(arr[which]);

                int t = which + 1;

                examtype = String.valueOf(t);

            }
        });

// create and show the alert dialog
        AlertDialog dialog = builder.create();
        dialog.show();
    }


    public void showTermList() {


        final String arr[] = {"Quarterly", "Half yearly", "Annual exam"};

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Choose a Term");
        ;


        builder.setItems(arr, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                dialog.dismiss();

                txtTerm.setText(arr[which]);

                int w=which+1;

                term = String.valueOf(w);

            }
        });

// create and show the alert dialog
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    public void showDivisionList(final List<Divisions> divisions) {
        List<String> strings = new ArrayList<>();
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Choose a division");
        ;

        for (Divisions divi : divisions
        ) {

            strings.add(divi.getUdcDivisionName());

        }


        builder.setItems(strings.toArray(new String[strings.size()]), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                dialog.dismiss();

                txtDivision.setText(divisions.get(which).getUdcDivisionName());

                divisionid = divisions.get(which).getUdcDivision();

                // showLessonPlanlist();

            }
        });

// create and show the alert dialog
        AlertDialog dialog = builder.create();
        dialog.show();
    }


}
