package com.report.schoolteacher.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Dialog;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.JsonArray;
import com.report.schoolteacher.R;
import com.report.schoolteacher.adapter.StudentByClassAdapter;
import com.report.schoolteacher.connectivity.NetConnection;
import com.report.schoolteacher.fragments.AddMarkFragment;
import com.report.schoolteacher.pojo.MarkCriteria;
import com.report.schoolteacher.pojo.StudentByClass;
import com.report.schoolteacher.pojo.Studentinfo;
import com.report.schoolteacher.progress.ProgressFragment;
import com.report.schoolteacher.webservice.RetrofitHelper;
import com.report.schoolteacher.webservice.WebInterfaceimpl;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class StudentListActivity extends AppCompatActivity {

    ImageView imgback;

    LinearLayout layout_studentdata;

    EditText edtSearch;

    RecyclerView recycler_view;

    MarkCriteria markCriteria;

    List<Studentinfo>studentByClasses;

    Dialog dialog=null;

    StudentByClass studentByClass=null;

    ProgressFragment progressFragment;

     EditText edtAcheivedMarks;

     EditText edtDescription;

    StudentByClassAdapter studentByClassAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_student_list);
        getSupportActionBar().hide();
        imgback=findViewById(R.id.imgback);
        studentByClasses=new ArrayList<>();

        markCriteria=(MarkCriteria) getIntent().getSerializableExtra("markCriteria");

        studentByClasses.addAll(markCriteria.getStudentByClasses());

        edtSearch=findViewById(R.id.edtSearch);
        recycler_view=findViewById(R.id.recycler_view);

        layout_studentdata=findViewById(R.id.layout_studentdata);


        if(studentByClasses.size()>0)
        {

            layout_studentdata.setVisibility(View.VISIBLE);

            studentByClassAdapter=new StudentByClassAdapter(StudentListActivity.this,studentByClasses,markCriteria);

            recycler_view.setLayoutManager(new LinearLayoutManager(StudentListActivity.this));
            recycler_view.setAdapter(studentByClassAdapter);
        }
        else {
            layout_studentdata.setVisibility(View.GONE);

            Toast.makeText(StudentListActivity.this,"No data found",Toast.LENGTH_SHORT).show();

        }


        imgback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                onBackPressed();
            }
        });


        edtSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if(!charSequence.toString().equals(""))
                {



                    if(studentByClasses.size()>0)
                    {

                        List<Studentinfo> student_search=new ArrayList<>();

                        for (Studentinfo s1:studentByClasses
                        ) {

                            if(s1.getUdcStudentName().toUpperCase().contains(charSequence.toString().toUpperCase())||s1.getUdcStudentName().toLowerCase().contains(charSequence.toString().toLowerCase()))
                            {
                                student_search.add(s1);
                            }

                        }

                        studentByClassAdapter=new StudentByClassAdapter(StudentListActivity.this,student_search,markCriteria);

                        recycler_view.setLayoutManager(new LinearLayoutManager(StudentListActivity.this));
                        recycler_view.setAdapter(studentByClassAdapter);





                    }
                    else {

                        Toast.makeText(StudentListActivity.this,"No data to search",Toast.LENGTH_SHORT).show();
                    }




                }
                else {

                    studentByClassAdapter=new StudentByClassAdapter(StudentListActivity.this,studentByClasses,markCriteria);

                    recycler_view.setLayoutManager(new LinearLayoutManager(StudentListActivity.this));
                    recycler_view.setAdapter(studentByClassAdapter);

                }

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }




}
