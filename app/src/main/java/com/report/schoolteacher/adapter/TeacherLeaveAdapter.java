package com.report.schoolteacher.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.report.schoolteacher.R;
import com.report.schoolteacher.pojo.TeacherLeave;

import java.util.List;

public class TeacherLeaveAdapter extends RecyclerView.Adapter<TeacherLeaveAdapter.TeacherLeaveHolder> {


    Context context;
    List<TeacherLeave>teacherLeaves;

    public TeacherLeaveAdapter(Context context, List<TeacherLeave> teacherLeaves) {
        this.context = context;
        this.teacherLeaves = teacherLeaves;
    }

    public class TeacherLeaveHolder extends RecyclerView.ViewHolder{
        TextView txtdetails;
        public TeacherLeaveHolder(@NonNull View itemView) {
            super(itemView);

            txtdetails=itemView.findViewById(R.id.txtdetails);
        }
    }

    @NonNull
    @Override
    public TeacherLeaveHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_student_leaveadapter,parent,false);


        return new TeacherLeaveHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull TeacherLeaveHolder holder, int position) {
        holder.txtdetails.setText("Reason : "+teacherLeaves.get(position).getUdcReason()+"\nFrom date : "+teacherLeaves.get(position).getUdcFromDate()+"\nTo date : "+teacherLeaves.get(position).getUdcToDate());

    }

    @Override
    public int getItemCount() {
        return teacherLeaves.size();
    }
}
