package com.report.schoolteacher.pojo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class MarkCriteria implements Serializable {

    String classid = "";
    String divisionid = "";

    String subjectid = "";
    String examtype = "";
    String term = "";

    String date = "";
    String totalmarke="";

    List<Studentinfo>studentByClasses=new ArrayList<>();

    public MarkCriteria() {
    }

    public String getTotalmarke() {
        return totalmarke;
    }

    public void setTotalmarke(String totalmarke) {
        this.totalmarke = totalmarke;
    }

    public List<Studentinfo> getStudentByClasses() {
        return studentByClasses;
    }

    public void setStudentByClasses(List<Studentinfo> studentByClasses) {
        this.studentByClasses = studentByClasses;
    }

    public String getClassid() {
        return classid;
    }

    public void setClassid(String classid) {
        this.classid = classid;
    }

    public String getDivisionid() {
        return divisionid;
    }

    public void setDivisionid(String divisionid) {
        this.divisionid = divisionid;
    }

    public String getSubjectid() {
        return subjectid;
    }

    public void setSubjectid(String subjectid) {
        this.subjectid = subjectid;
    }

    public String getExamtype() {
        return examtype;
    }

    public void setExamtype(String examtype) {
        this.examtype = examtype;
    }

    public String getTerm() {
        return term;
    }

    public void setTerm(String term) {
        this.term = term;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
