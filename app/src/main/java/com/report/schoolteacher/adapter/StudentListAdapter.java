package com.report.schoolteacher.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.report.schoolteacher.R;
import com.report.schoolteacher.activities.AttendancedetailsActivity;
import com.report.schoolteacher.activities.TeacherProfileActivity;
import com.report.schoolteacher.pojo.StudentData;
import com.report.schoolteacher.pojo.Studentinfo;
import com.report.schoolteacher.pojo.Students;
import com.report.schoolteacher.utils.Utilities;

import java.util.List;

public class StudentListAdapter extends RecyclerView.Adapter<StudentListAdapter.StudentListHolder> {


    Context context;
    List<Studentinfo>students;

    public StudentListAdapter(Context context, List<Studentinfo> students) {
        this.context = context;
        this.students = students;
    }

    public class StudentListHolder extends RecyclerView.ViewHolder{

        ImageView imgStudent;

        TextView txtStudent;

        public StudentListHolder(@NonNull View itemView) {
            super(itemView);
            imgStudent=itemView.findViewById(R.id.imgStudent);

            txtStudent=itemView.findViewById(R.id.txtStudent);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Intent i=new Intent(context, AttendancedetailsActivity.class);
                    i.putExtra("student",students.get(getAdapterPosition()));

                    context.startActivity(i);

                }
            });

        }
    }

    @NonNull
    @Override
    public StudentListHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View v= LayoutInflater.from(context).inflate(R.layout.layout_student_adapter,parent,false);

        return new StudentListHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull StudentListHolder holder, int position) {

        holder.txtStudent.setText(students.get(position).getUdcStudentName());

        Glide.with(context)
                .load(Utilities.imageBaseurl+students.get(position).getUdcStudentPhotoPath())
                .apply(RequestOptions.circleCropTransform())
                .placeholder(R.drawable.u)
                .into(holder.imgStudent);



    }

    @Override
    public int getItemCount() {
        return students.size();
    }
}
