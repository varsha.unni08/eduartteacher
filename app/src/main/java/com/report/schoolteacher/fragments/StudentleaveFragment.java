package com.report.schoolteacher.fragments;


import android.content.DialogInterface;
import android.os.Bundle;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.report.schoolteacher.AppLiterals.Constants;
import com.report.schoolteacher.R;
import com.report.schoolteacher.activities.VideosActivity;
import com.report.schoolteacher.adapter.StudentLeaveAdapter;
import com.report.schoolteacher.connectivity.NetConnection;
import com.report.schoolteacher.pojo.StudentData;
import com.report.schoolteacher.pojo.StudentLeave;
import com.report.schoolteacher.pojo.Studentinfo;
import com.report.schoolteacher.pojo.Students;
import com.report.schoolteacher.progress.ProgressFragment;
import com.report.schoolteacher.webservice.RetrofitHelper;
import com.report.schoolteacher.webservice.WebInterfaceimpl;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * A simple {@link Fragment} subclass.
 */
public class StudentleaveFragment extends Fragment {


    public StudentleaveFragment() {
        // Required empty public constructor
    }

    View view;

    RecyclerView recycler_view;

    ImageView imgdropdownStudent;

    TextView txtNodata,txtStudent;

    ProgressFragment progressFragment;

    String studentid="";


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view= inflater.inflate(R.layout.fragment_studentleave, container, false);

        recycler_view=view.findViewById(R.id.recycler_view);
        txtNodata=view.findViewById(R.id.txtNodata);

        txtStudent = view.findViewById(R.id.txtStudent);
        imgdropdownStudent = view.findViewById(R.id.imgdropdownStudent);


        txtStudent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getStudents();

            }
        });

        imgdropdownStudent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getStudents();

            }
        });

        return view;
    }


    public void getStudents() {
        progressFragment = new ProgressFragment();
        progressFragment.show(getChildFragmentManager(), "cjkk");

        Retrofit retrofitHelper = RetrofitHelper.getRetrofitInstance(getActivity());

        WebInterfaceimpl webInterfaceimpl = retrofitHelper.create(WebInterfaceimpl.class);

        Call<StudentData> listCall = webInterfaceimpl.getAllStudentslist(Constants.classid,Constants.divisionid);

        listCall.enqueue(new Callback<StudentData>() {
            @Override
            public void onResponse(Call<StudentData> call, Response<StudentData> response) {

                progressFragment.dismiss();
                if (response.body() != null) {

                    if (response.body().getData().size() > 0) {


                        showStudentList(response.body().getData());


                    } else {


                        Toast.makeText(getActivity(), "No data found", Toast.LENGTH_SHORT).show();

                    }


                } else {


                    Toast.makeText(getActivity(), "No data found", Toast.LENGTH_SHORT).show();

                }

            }

            @Override
            public void onFailure(Call<StudentData> call, Throwable t) {
                progressFragment.dismiss();

                if (!NetConnection.isConnected(getActivity())) {
                    Toast.makeText(getActivity(), "Check Internet connection", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    public void showStudentList(final List<Studentinfo> students) {
        List<String> strings = new ArrayList<>();
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Choose a student");
        ;

        for (Studentinfo students1 : students
        ) {

            strings.add(students1.getUdcStudentName());

        }


        builder.setItems(strings.toArray(new String[strings.size()]), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                dialog.dismiss();

                txtStudent.setText(students.get(which).getUdcStudentName());

                studentid = students.get(which).getUdcStudentId();

                //getAllVideos();

                getStudentLeave();



            }
        });

// create and show the alert dialog
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    public void getStudentLeave()
    {

        progressFragment = new ProgressFragment();
        progressFragment.show(getChildFragmentManager(), "cjkk");

        Retrofit retrofitHelper = RetrofitHelper.getRetrofitInstance(getActivity());

        WebInterfaceimpl webInterfaceimpl = retrofitHelper.create(WebInterfaceimpl.class);

        Call<List<StudentLeave>>listCall=webInterfaceimpl.getStudentLeave(studentid);
        listCall.enqueue(new Callback<List<StudentLeave>>() {
            @Override
            public void onResponse(Call<List<StudentLeave>> call, Response<List<StudentLeave>> response) {
                progressFragment.dismiss();
                if (response.body() != null) {

                    if (response.body().size() > 0) {

                        recycler_view.setVisibility(View.VISIBLE);
                        txtNodata.setVisibility(View.GONE);


recycler_view.setLayoutManager(new LinearLayoutManager(getActivity()));
recycler_view.setAdapter(new StudentLeaveAdapter(getActivity(),response.body()));


                    } else {

                        recycler_view.setVisibility(View.GONE);
                        txtNodata.setVisibility(View.VISIBLE);
                        Toast.makeText(getActivity(), "No data found", Toast.LENGTH_SHORT).show();

                    }


                } else {

                    recycler_view.setVisibility(View.GONE);
                    txtNodata.setVisibility(View.VISIBLE);
                    Toast.makeText(getActivity(), "No data found", Toast.LENGTH_SHORT).show();

                }
            }

            @Override
            public void onFailure(Call<List<StudentLeave>> call, Throwable t) {
                progressFragment.dismiss();

                if (!NetConnection.isConnected(getActivity())) {
                    Toast.makeText(getActivity(), "Check Internet connection", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

}
