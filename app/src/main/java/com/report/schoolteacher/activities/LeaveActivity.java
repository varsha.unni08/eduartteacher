package com.report.schoolteacher.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.google.android.material.tabs.TabLayout;
import com.report.schoolteacher.R;
import com.report.schoolteacher.adapter.LeaveTabpagerAdapter;

public class LeaveActivity extends AppCompatActivity {

    ImageView imgback;

    TabLayout tablayout;

    ViewPager viewpager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_leave);
        getSupportActionBar().hide();

        imgback=findViewById(R.id.imgback);
        tablayout=findViewById(R.id.tablayout);
        viewpager=findViewById(R.id.viewpager);

        imgback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                onBackPressed();
            }
        });


        tablayout.addTab(tablayout.newTab());
        tablayout.addTab(tablayout.newTab());

        tablayout.setupWithViewPager(viewpager);

        viewpager.setAdapter(new LeaveTabpagerAdapter(getSupportFragmentManager()));

        tablayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {

                viewpager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }
}
