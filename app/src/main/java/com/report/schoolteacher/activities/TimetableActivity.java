package com.report.schoolteacher.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.report.schoolteacher.AppLiterals.Constants;
import com.report.schoolteacher.R;
import com.report.schoolteacher.adapter.TimetableAdapter;
import com.report.schoolteacher.connectivity.NetConnection;
import com.report.schoolteacher.pojo.Period;
import com.report.schoolteacher.pojo.TimeTableStatus;
import com.report.schoolteacher.pojo.Timetable;
import com.report.schoolteacher.progress.ProgressFragment;
import com.report.schoolteacher.webservice.RetrofitHelper;
import com.report.schoolteacher.webservice.WebInterfaceimpl;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class TimetableActivity extends AppCompatActivity {

    RecyclerView recycler_view;

    ImageView imgback;

    ProgressFragment progressFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_timetable);
        getSupportActionBar().hide();
        recycler_view=findViewById(R.id.recycler_view);
        imgback=findViewById(R.id.imgback);

        imgback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                onBackPressed();
            }
        });

        getTimeTable();
    }


    public void getTimeTable()
    {
        progressFragment = new ProgressFragment();
        progressFragment.show(getSupportFragmentManager(), "cjkk");

        Retrofit retrofitHelper = RetrofitHelper.getRetrofitInstance(TimetableActivity.this);

        WebInterfaceimpl webInterfaceimpl = retrofitHelper.create(WebInterfaceimpl.class);

        Call<TimeTableStatus>listCall=webInterfaceimpl.getAllTimetable(Constants.classid,Constants.divisionid);

        listCall.enqueue(new Callback<TimeTableStatus>() {
            @Override
            public void onResponse(Call<TimeTableStatus> call, Response<TimeTableStatus> response) {

                progressFragment.dismiss();
                if(response.body()!=null)
                {

                    if(response.body().getData().size()>0)
                    {


                        recycler_view.setVisibility(View.VISIBLE);




                        Set<String> days=new HashSet<>();
                        List<Period>weekdays=new ArrayList<>();

                        String aa[]=getResources().getStringArray(R.array.days);

//                        for(String b:aa) {
//
//                            for (TimeTable tt : response.body().getData()) {
//
//                                if(b.trim().equalsIgnoreCase(tt.getUdcWeekdayName())) {
//                                    days.add(tt.getUdcWeekdayName());
//                                    break;
//                                }
//                            }
//                        }

                        for(String a : aa)
                        {
                            Period p=new Period();
                            p.setDay(a);
                            p.setSelected(0);
                            weekdays.add(p);
                        }







                        recycler_view.setLayoutManager(new LinearLayoutManager(TimetableActivity.this));
                        recycler_view.setAdapter(new TimetableAdapter(TimetableActivity.this,response.body().getData(),weekdays));


                    }
                    else {

                        recycler_view.setVisibility(View.GONE);

                        Toast.makeText(TimetableActivity.this,"No data found",Toast.LENGTH_SHORT).show();

                    }


                }
                else {

                    recycler_view.setVisibility(View.GONE);

                    Toast.makeText(TimetableActivity.this,"No data found",Toast.LENGTH_SHORT).show();

                }

            }

            @Override
            public void onFailure(Call<TimeTableStatus> call, Throwable t) {
                progressFragment.dismiss();

                if(!NetConnection.isConnected(TimetableActivity.this))
                {
                    Toast.makeText(TimetableActivity.this,"Check Internet connection",Toast.LENGTH_SHORT).show();
                }

            }
        });
    }
}
