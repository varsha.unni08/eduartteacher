package com.report.schoolteacher.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class OnlineClassData {
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("classtopic")
    @Expose
    private String classtopic;
    @SerializedName("udc_teacherid")
    @Expose
    private String udcTeacherid;
    @SerializedName("classdivision")
    @Expose
    private String classdivision;
    @SerializedName("class")
    @Expose
    private String _class;
    @SerializedName("joinlink")
    @Expose
    private String joinlink;
    @SerializedName("time")
    @Expose
    private String time;
    @SerializedName("udc_weekday_name")
    @Expose
    private String udc_weekday_name;


    public OnlineClassData() {
    }

    public String getUdc_weekday_name() {
        return udc_weekday_name;
    }

    public void setUdc_weekday_name(String udc_weekday_name) {
        this.udc_weekday_name = udc_weekday_name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getClasstopic() {
        return classtopic;
    }

    public void setClasstopic(String classtopic) {
        this.classtopic = classtopic;
    }

    public String getUdcTeacherid() {
        return udcTeacherid;
    }

    public void setUdcTeacherid(String udcTeacherid) {
        this.udcTeacherid = udcTeacherid;
    }

    public String getClassdivision() {
        return classdivision;
    }

    public void setClassdivision(String classdivision) {
        this.classdivision = classdivision;
    }

    public String get_class() {
        return _class;
    }

    public void set_class(String _class) {
        this._class = _class;
    }

    public String getJoinlink() {
        return joinlink;
    }

    public void setJoinlink(String joinlink) {
        this.joinlink = joinlink;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}
