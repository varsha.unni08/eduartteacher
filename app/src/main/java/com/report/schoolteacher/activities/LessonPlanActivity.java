package com.report.schoolteacher.activities;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.report.schoolteacher.AppLiterals.Constants;
import com.report.schoolteacher.R;
import com.report.schoolteacher.adapter.LessonPlanAdapter;
import com.report.schoolteacher.connectivity.NetConnection;
import com.report.schoolteacher.pojo.Classes;
import com.report.schoolteacher.pojo.LessonPlan;
import com.report.schoolteacher.pojo.Subjects;
import com.report.schoolteacher.progress.ProgressFragment;
import com.report.schoolteacher.webservice.RetrofitHelper;
import com.report.schoolteacher.webservice.WebInterfaceimpl;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class LessonPlanActivity extends AppCompatActivity {

    ImageView imgback,imgdropdownClass,imgdropdownsubject;

    RecyclerView recycler_view;

    TextView txtClass,txtSubject;

    Button btnSubmit;

    ProgressFragment progressFragment;

    String classid="",subjectid;

    com.google.android.material.floatingactionbutton.FloatingActionButton floatingActionButton;





    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lesson_plan);
        getSupportActionBar().hide();
        imgback=findViewById(R.id.imgback);
        imgdropdownClass=findViewById(R.id.imgdropdownClass);
//        imgdropdowndivision=findViewById(R.id.imgdropdowndivision);
        imgdropdownsubject=findViewById(R.id.imgdropdownsubject);
        floatingActionButton=findViewById(R.id.fab);

        recycler_view=findViewById(R.id.recycler_view);
        txtClass=findViewById(R.id.txtClass);
//        txtDivision=findViewById(R.id.txtDivision);
        txtSubject=findViewById(R.id.txtSubject);

        btnSubmit=findViewById(R.id.btnSubmit);


        imgback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                onBackPressed();
            }
        });


        txtClass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                getClasses();

            }
        });

        imgdropdownClass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getClasses();

            }
        });


        txtSubject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                getSubjects();

            }
        });

        imgdropdownsubject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getSubjects();

            }
        });


        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                    if(!subjectid.equals(""))
                    {


                        getLessonPlans();

                    }

                    else {

                        Toast.makeText(LessonPlanActivity.this,"select a subject",Toast.LENGTH_SHORT).show();

                    }





            }


        });


        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                startActivity(new Intent(LessonPlanActivity.this,AddLessonPlanActivity.class));


            }
        });

       // getLessonPlans();
    }

    private void getLessonPlans() {

        progressFragment = new ProgressFragment();
        progressFragment.show(getSupportFragmentManager(), "cjkk");

        Retrofit retrofitHelper = RetrofitHelper.getRetrofitInstance(LessonPlanActivity.this);

        WebInterfaceimpl webInterfaceimpl = retrofitHelper.create(WebInterfaceimpl.class);

        Call<List<LessonPlan>>listCall=webInterfaceimpl.getLessonPlans(Constants.classid,subjectid);
        listCall.enqueue(new Callback<List<LessonPlan>>() {
            @Override
            public void onResponse(Call<List<LessonPlan>> call, Response<List<LessonPlan>> response) {
                progressFragment.dismiss();
                if(response.body()!=null)
                {

                    if(response.body().size()>0)
                    {


                        recycler_view.setVisibility(View.VISIBLE);
                        recycler_view.setLayoutManager(new LinearLayoutManager(LessonPlanActivity.this));
                        recycler_view.setAdapter(new LessonPlanAdapter(LessonPlanActivity.this,response.body()));





                    }
                    else {

                        recycler_view.setVisibility(View.GONE);

                        Toast.makeText(LessonPlanActivity.this,"No data found",Toast.LENGTH_SHORT).show();

                    }


                }
                else {

                    recycler_view.setVisibility(View.GONE);

                    Toast.makeText(LessonPlanActivity.this,"failed",Toast.LENGTH_SHORT).show();

                }
            }

            @Override
            public void onFailure(Call<List<LessonPlan>> call, Throwable t) {

                progressFragment.dismiss();

                if(!NetConnection.isConnected(LessonPlanActivity.this))
                {
                    Toast.makeText(LessonPlanActivity.this,"Check Internet connection",Toast.LENGTH_SHORT).show();
                }

            }
        });

    }

    public void getSubjects()
    {
        progressFragment = new ProgressFragment();
        progressFragment.show(getSupportFragmentManager(), "cjkk");

        Retrofit retrofitHelper = RetrofitHelper.getRetrofitInstance(LessonPlanActivity.this);

        WebInterfaceimpl webInterfaceimpl = retrofitHelper.create(WebInterfaceimpl.class);

        Call<List<Subjects>>listCall=webInterfaceimpl.getSubjects();

        listCall.enqueue(new Callback<List<Subjects>>() {
            @Override
            public void onResponse(Call<List<Subjects>> call, Response<List<Subjects>> response) {

                progressFragment.dismiss();
                if(response.body()!=null)
                {

                    if(response.body().size()>0)
                    {


                       showSubjectList(response.body());



                    }
                    else {

                        Toast.makeText(LessonPlanActivity.this,"No data found",Toast.LENGTH_SHORT).show();

                    }


                }
                else {

                    Toast.makeText(LessonPlanActivity.this,"No data found",Toast.LENGTH_SHORT).show();

                }


            }

            @Override
            public void onFailure(Call<List<Subjects>> call, Throwable t) {
                progressFragment.dismiss();

                if(!NetConnection.isConnected(LessonPlanActivity.this))
                {
                    Toast.makeText(LessonPlanActivity.this,"Check Internet connection",Toast.LENGTH_SHORT).show();
                }
            }
        });



    }




    public void showSubjectList(final List<Subjects> subjects)
    {
        List<String>strings=new ArrayList<>();
        AlertDialog.Builder builder = new AlertDialog.Builder(LessonPlanActivity.this);
        builder.setTitle("Choose a subject");
        ;

        for (Subjects subjects1:subjects
        ) {

            strings.add(subjects1.getUdcSubjectDetails());

        }



        builder.setItems(strings.toArray(new String[strings.size()]), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                dialog.dismiss();

                txtSubject.setText(subjects.get(which).getUdcSubjectDetails());

                subjectid=subjects.get(which).getSubjectId();

                // showLessonPlanlist();

            }
        });

// create and show the alert dialog
        AlertDialog dialog = builder.create();
        dialog.show();
    }


    public void getClasses()
    {
        progressFragment = new ProgressFragment();
        progressFragment.show(getSupportFragmentManager(), "cjkk");

        Retrofit retrofitHelper = RetrofitHelper.getRetrofitInstance(LessonPlanActivity.this);

        WebInterfaceimpl webInterfaceimpl = retrofitHelper.create(WebInterfaceimpl.class);

        Call<List<Classes>>listCall=webInterfaceimpl.getClasses();

        listCall.enqueue(new Callback<List<Classes>>() {
            @Override
            public void onResponse(Call<List<Classes>> call, Response<List<Classes>> response) {

                progressFragment.dismiss();
                if(response.body()!=null)
                {

                    if(response.body().size()>0)
                    {


                        showClassList(response.body());



                    }
                    else {

                        Toast.makeText(LessonPlanActivity.this,"No data found",Toast.LENGTH_SHORT).show();

                    }


                }
                else {

                    Toast.makeText(LessonPlanActivity.this,"No data found",Toast.LENGTH_SHORT).show();

                }

            }

            @Override
            public void onFailure(Call<List<Classes>> call, Throwable t) {

                progressFragment.dismiss();

                if(!NetConnection.isConnected(LessonPlanActivity.this))
                {
                    Toast.makeText(LessonPlanActivity.this,"Check Internet connection",Toast.LENGTH_SHORT).show();
                }

            }
        });

    }


    public void showClassList(final List<Classes>classes)
    {

        List<String>strings=new ArrayList<>();
        AlertDialog.Builder builder = new AlertDialog.Builder(LessonPlanActivity.this);
        builder.setTitle("Choose a student");
        ;

        for (Classes student:classes
        ) {

            strings.add(student.getUdcClassDetails());

        }



        builder.setItems(strings.toArray(new String[strings.size()]), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                dialog.dismiss();

                txtClass.setText(classes.get(which).getUdcClassDetails());

                classid=classes.get(which).getClassId();

                // showLessonPlanlist();

            }
        });

// create and show the alert dialog
        AlertDialog dialog = builder.create();
        dialog.show();
    }
}
