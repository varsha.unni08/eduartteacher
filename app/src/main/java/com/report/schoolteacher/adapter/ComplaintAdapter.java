package com.report.schoolteacher.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.report.schoolteacher.R;
import com.report.schoolteacher.pojo.Complaints;

import java.util.List;

public class ComplaintAdapter extends RecyclerView.Adapter<ComplaintAdapter.ComplaintHolder> {


    Context context;

    List<Complaints>complaints;

    public ComplaintAdapter(Context context, List<Complaints> complaints) {
        this.context = context;
        this.complaints = complaints;
    }

    public class ComplaintHolder extends RecyclerView.ViewHolder
    {

        TextView txtdetails;

        public ComplaintHolder(@NonNull View itemView) {
            super(itemView);

            txtdetails=itemView.findViewById(R.id.txtdetails);
        }
    }


    @NonNull
    @Override
    public ComplaintHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {


        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_student_leaveadapter,parent,false);


        return new ComplaintHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ComplaintHolder holder, int position) {

        holder.txtdetails.setText("Student : "+complaints.get(position).getStudent()+"\nParent : "+complaints.get(position).getParent()+"\nMessage : "+complaints.get(position).getComplaint());

    }

    @Override
    public int getItemCount() {
        return complaints.size();
    }
}
