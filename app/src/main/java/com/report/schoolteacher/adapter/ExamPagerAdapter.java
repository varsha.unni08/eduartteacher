package com.report.schoolteacher.adapter;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.report.schoolteacher.fragments.AddMarkFragment;
import com.report.schoolteacher.fragments.MarklistFragment;

public class ExamPagerAdapter extends FragmentPagerAdapter {

    String arr[]={"Add Mark","Mark List"};



    public ExamPagerAdapter(@NonNull FragmentManager fm, int behavior) {
        super(fm, behavior);
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {

        Fragment fragment=null;

        if(position==0)
        {
            fragment=new AddMarkFragment();

        }
        else {

            fragment=new MarklistFragment();
        }


        return fragment;
    }

    @Override
    public int getCount() {
        return 2;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return arr[position];
    }
}
