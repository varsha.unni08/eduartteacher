package com.report.schoolteacher.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.report.schoolteacher.AppLiterals.Constants;
import com.report.schoolteacher.R;
import com.report.schoolteacher.adapter.StudentListAdapter;
import com.report.schoolteacher.connectivity.NetConnection;
import com.report.schoolteacher.pojo.Attendance;
import com.report.schoolteacher.pojo.StudentData;
import com.report.schoolteacher.pojo.Studentinfo;
import com.report.schoolteacher.pojo.Students;
import com.report.schoolteacher.progress.ProgressFragment;
import com.report.schoolteacher.webservice.RetrofitHelper;
import com.report.schoolteacher.webservice.WebInterfaceimpl;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class AttendanceActivity extends AppCompatActivity {

    EditText edtSearch;

    RecyclerView recycler_view;

    ProgressFragment progressFragment;

    TextView txtNodata;

    ImageView imgback,imgaddAttendance;

    List<Studentinfo>students;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_attendance);
        getSupportActionBar().hide();
        students=new ArrayList<>();
        edtSearch=findViewById(R.id.edtSearch);
        recycler_view=findViewById(R.id.recycler_view);
        txtNodata=findViewById(R.id.txtNodata);
        imgback=findViewById(R.id.imgback);
        imgaddAttendance=findViewById(R.id.imgaddAttendance);


        imgback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                onBackPressed();
            }
        });

        imgaddAttendance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                startActivity(new Intent(AttendanceActivity.this,AddAttendanceActivity.class));
            }
        });

        getStudents();

        edtSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                if(!charSequence.toString().equals(""))
                {

                    if(students.size()>0)
                    {

                        List<Studentinfo>student_search=new ArrayList<>();

                        for (Studentinfo s1:students
                             ) {

                            if(s1.getUdcStudentName().toLowerCase().contains(charSequence.toString())||s1.getUdcStudentName().toUpperCase().contains(charSequence.toString()))
                            {
                                student_search.add(s1);
                            }

                        }

                        recycler_view.setLayoutManager(new LinearLayoutManager(AttendanceActivity.this));
                        recycler_view.setAdapter(new StudentListAdapter(AttendanceActivity.this,student_search));




                    }
                    else {

                        Toast.makeText(AttendanceActivity.this,"No data to search",Toast.LENGTH_SHORT).show();
                    }




                }
                else {

                    recycler_view.setLayoutManager(new LinearLayoutManager(AttendanceActivity.this));
                    recycler_view.setAdapter(new StudentListAdapter(AttendanceActivity.this,students));

                }

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

    }

    public void getStudents() {
        progressFragment = new ProgressFragment();
        progressFragment.show(getSupportFragmentManager(), "cjkk");

        Retrofit retrofitHelper = RetrofitHelper.getRetrofitInstance(AttendanceActivity.this);

        WebInterfaceimpl webInterfaceimpl = retrofitHelper.create(WebInterfaceimpl.class);

        Call<StudentData> listCall = webInterfaceimpl.getAllStudentslist(Constants.classid,Constants.divisionid);

        listCall.enqueue(new Callback<StudentData>() {
            @Override
            public void onResponse(Call<StudentData> call, Response<StudentData> response) {

                progressFragment.dismiss();
                if (response.body() != null) {

                    if (response.body().getData().size() > 0) {

                        recycler_view.setVisibility(View.VISIBLE);
                        txtNodata.setVisibility(View.GONE);

                        students.addAll(response.body().getData());


                        recycler_view.setLayoutManager(new LinearLayoutManager(AttendanceActivity.this));
                        recycler_view.setAdapter(new StudentListAdapter(AttendanceActivity.this,response.body().getData()));



                    } else {


                        recycler_view.setVisibility(View.GONE);
                        txtNodata.setVisibility(View.VISIBLE);

                        Toast.makeText(AttendanceActivity.this, "No data found", Toast.LENGTH_SHORT).show();

                    }


                } else {

                    recycler_view.setVisibility(View.GONE);
                    txtNodata.setVisibility(View.VISIBLE);
                    Toast.makeText(AttendanceActivity.this, "No data found", Toast.LENGTH_SHORT).show();

                }

            }

            @Override
            public void onFailure(Call<StudentData> call, Throwable t) {
                progressFragment.dismiss();

                if (!NetConnection.isConnected(AttendanceActivity.this)) {
                    Toast.makeText(AttendanceActivity.this, "Check Internet connection", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}
