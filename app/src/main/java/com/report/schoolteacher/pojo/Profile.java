package com.report.schoolteacher.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Profile {

    @SerializedName("udc_teacher_code")
    @Expose
    private String udcTeacherCode;
    @SerializedName("udc_teacher_name")
    @Expose
    private String udcTeacherName;
    @SerializedName("udc_teacherLastName")
    @Expose
    private String udcTeacherLastName;
    @SerializedName("udc_teacher_dob")
    @Expose
    private String udcTeacherDob;
    @SerializedName("udc_teacher_mob")
    @Expose
    private String udcTeacherMob;
    @SerializedName("udc_teacher_email_id")
    @Expose
    private String udcTeacherEmailId;
    @SerializedName("udc_teacher_qualification")
    @Expose
    private String udcTeacherQualification;
    @SerializedName("udc_teacher_joint_date")
    @Expose
    private String udcTeacherJointDate;
    @SerializedName("udc_teacher_service_start_date")
    @Expose
    private String udcTeacherServiceStartDate;
    @SerializedName("ds_image")
    @Expose
    private String dsImage;

    public Profile() {
    }

    public String getUdcTeacherCode() {
        return udcTeacherCode;
    }

    public void setUdcTeacherCode(String udcTeacherCode) {
        this.udcTeacherCode = udcTeacherCode;
    }

    public String getUdcTeacherName() {
        return udcTeacherName;
    }

    public void setUdcTeacherName(String udcTeacherName) {
        this.udcTeacherName = udcTeacherName;
    }

    public String getUdcTeacherLastName() {
        return udcTeacherLastName;
    }

    public void setUdcTeacherLastName(String udcTeacherLastName) {
        this.udcTeacherLastName = udcTeacherLastName;
    }

    public String getUdcTeacherDob() {
        return udcTeacherDob;
    }

    public void setUdcTeacherDob(String udcTeacherDob) {
        this.udcTeacherDob = udcTeacherDob;
    }

    public String getUdcTeacherMob() {
        return udcTeacherMob;
    }

    public void setUdcTeacherMob(String udcTeacherMob) {
        this.udcTeacherMob = udcTeacherMob;
    }

    public String getUdcTeacherEmailId() {
        return udcTeacherEmailId;
    }

    public void setUdcTeacherEmailId(String udcTeacherEmailId) {
        this.udcTeacherEmailId = udcTeacherEmailId;
    }

    public String getUdcTeacherQualification() {
        return udcTeacherQualification;
    }

    public void setUdcTeacherQualification(String udcTeacherQualification) {
        this.udcTeacherQualification = udcTeacherQualification;
    }

    public String getUdcTeacherJointDate() {
        return udcTeacherJointDate;
    }

    public void setUdcTeacherJointDate(String udcTeacherJointDate) {
        this.udcTeacherJointDate = udcTeacherJointDate;
    }

    public String getUdcTeacherServiceStartDate() {
        return udcTeacherServiceStartDate;
    }

    public void setUdcTeacherServiceStartDate(String udcTeacherServiceStartDate) {
        this.udcTeacherServiceStartDate = udcTeacherServiceStartDate;
    }

    public String getDsImage() {
        return dsImage;
    }

    public void setDsImage(String dsImage) {
        this.dsImage = dsImage;
    }
}
