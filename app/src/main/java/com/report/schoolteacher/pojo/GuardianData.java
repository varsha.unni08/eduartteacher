package com.report.schoolteacher.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GuardianData {

    @SerializedName("udc_parent")
    @Expose
    private String udcParent;
    @SerializedName("udc_father_name")
    @Expose
    private String udcFatherName;
    @SerializedName("udc_father_contact_address")
    @Expose
    private String udcFatherContactAddress;
    @SerializedName("udc_father_contact_mobile")
    @Expose
    private String udcFatherContactMobile;
    @SerializedName("udc_father_occupation")
    @Expose
    private String udcFatherOccupation;
    @SerializedName("udc_father_qualification")
    @Expose
    private String udcFatherQualification;
    @SerializedName("udc_father_email_id")
    @Expose
    private String udcFatherEmailId;
    @SerializedName("udc_mother_name")
    @Expose
    private String udcMotherName;
    @SerializedName("udc_mother_contact_address")
    @Expose
    private String udcMotherContactAddress;
    @SerializedName("udc_mother_contact_mobile")
    @Expose
    private String udcMotherContactMobile;
    @SerializedName("udc_mother_occupation")
    @Expose
    private String udcMotherOccupation;
    @SerializedName("udc_mother_qualification")
    @Expose
    private String udcMotherQualification;
    @SerializedName("udc_mother_email_id")
    @Expose
    private String udcMotherEmailId;
    @SerializedName("udc_gardian_name")
    @Expose
    private String udcGardianName;
    @SerializedName("udc_gardian_contact_address")
    @Expose
    private String udcGardianContactAddress;
    @SerializedName("udc_gardian_contact_mobile")
    @Expose
    private String udcGardianContactMobile;
    @SerializedName("udc_gardian_occupation")
    @Expose
    private String udcGardianOccupation;
    @SerializedName("udc_gardian_qualification")
    @Expose
    private String udcGardianQualification;
    @SerializedName("udc_gardian_email_id")
    @Expose
    private String udcGardianEmailId;
    @SerializedName("udc_parent_user_id")
    @Expose
    private String udcParentUserId;
    @SerializedName("udc_parent_password")
    @Expose
    private String udcParentPassword;
    @SerializedName("udc_parent_deleteid")
    @Expose
    private String udcParentDeleteid;


    public String getUdcParent() {
        return udcParent;
    }

    public void setUdcParent(String udcParent) {
        this.udcParent = udcParent;
    }

    public String getUdcFatherName() {
        return udcFatherName;
    }

    public void setUdcFatherName(String udcFatherName) {
        this.udcFatherName = udcFatherName;
    }

    public String getUdcFatherContactAddress() {
        return udcFatherContactAddress;
    }

    public void setUdcFatherContactAddress(String udcFatherContactAddress) {
        this.udcFatherContactAddress = udcFatherContactAddress;
    }

    public String getUdcFatherContactMobile() {
        return udcFatherContactMobile;
    }

    public void setUdcFatherContactMobile(String udcFatherContactMobile) {
        this.udcFatherContactMobile = udcFatherContactMobile;
    }

    public String getUdcFatherOccupation() {
        return udcFatherOccupation;
    }

    public void setUdcFatherOccupation(String udcFatherOccupation) {
        this.udcFatherOccupation = udcFatherOccupation;
    }

    public String getUdcFatherQualification() {
        return udcFatherQualification;
    }

    public void setUdcFatherQualification(String udcFatherQualification) {
        this.udcFatherQualification = udcFatherQualification;
    }

    public String getUdcFatherEmailId() {
        return udcFatherEmailId;
    }

    public void setUdcFatherEmailId(String udcFatherEmailId) {
        this.udcFatherEmailId = udcFatherEmailId;
    }

    public String getUdcMotherName() {
        return udcMotherName;
    }

    public void setUdcMotherName(String udcMotherName) {
        this.udcMotherName = udcMotherName;
    }

    public String getUdcMotherContactAddress() {
        return udcMotherContactAddress;
    }

    public void setUdcMotherContactAddress(String udcMotherContactAddress) {
        this.udcMotherContactAddress = udcMotherContactAddress;
    }

    public String getUdcMotherContactMobile() {
        return udcMotherContactMobile;
    }

    public void setUdcMotherContactMobile(String udcMotherContactMobile) {
        this.udcMotherContactMobile = udcMotherContactMobile;
    }

    public String getUdcMotherOccupation() {
        return udcMotherOccupation;
    }

    public void setUdcMotherOccupation(String udcMotherOccupation) {
        this.udcMotherOccupation = udcMotherOccupation;
    }

    public String getUdcMotherQualification() {
        return udcMotherQualification;
    }

    public void setUdcMotherQualification(String udcMotherQualification) {
        this.udcMotherQualification = udcMotherQualification;
    }

    public String getUdcMotherEmailId() {
        return udcMotherEmailId;
    }

    public void setUdcMotherEmailId(String udcMotherEmailId) {
        this.udcMotherEmailId = udcMotherEmailId;
    }

    public String getUdcGardianName() {
        return udcGardianName;
    }

    public void setUdcGardianName(String udcGardianName) {
        this.udcGardianName = udcGardianName;
    }

    public String getUdcGardianContactAddress() {
        return udcGardianContactAddress;
    }

    public void setUdcGardianContactAddress(String udcGardianContactAddress) {
        this.udcGardianContactAddress = udcGardianContactAddress;
    }

    public String getUdcGardianContactMobile() {
        return udcGardianContactMobile;
    }

    public void setUdcGardianContactMobile(String udcGardianContactMobile) {
        this.udcGardianContactMobile = udcGardianContactMobile;
    }

    public String getUdcGardianOccupation() {
        return udcGardianOccupation;
    }

    public void setUdcGardianOccupation(String udcGardianOccupation) {
        this.udcGardianOccupation = udcGardianOccupation;
    }

    public String getUdcGardianQualification() {
        return udcGardianQualification;
    }

    public void setUdcGardianQualification(String udcGardianQualification) {
        this.udcGardianQualification = udcGardianQualification;
    }

    public String getUdcGardianEmailId() {
        return udcGardianEmailId;
    }

    public void setUdcGardianEmailId(String udcGardianEmailId) {
        this.udcGardianEmailId = udcGardianEmailId;
    }

    public String getUdcParentUserId() {
        return udcParentUserId;
    }

    public void setUdcParentUserId(String udcParentUserId) {
        this.udcParentUserId = udcParentUserId;
    }

    public String getUdcParentPassword() {
        return udcParentPassword;
    }

    public void setUdcParentPassword(String udcParentPassword) {
        this.udcParentPassword = udcParentPassword;
    }

    public String getUdcParentDeleteid() {
        return udcParentDeleteid;
    }

    public void setUdcParentDeleteid(String udcParentDeleteid) {
        this.udcParentDeleteid = udcParentDeleteid;
    }
}
