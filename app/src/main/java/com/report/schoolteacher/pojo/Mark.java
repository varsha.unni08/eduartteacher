package com.report.schoolteacher.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Mark implements Serializable {
    @SerializedName("udc_test_id")
    @Expose
    private String udcTestId;
    @SerializedName("rollnum")
    @Expose
    private String rollnum;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("udc_tmarks_scored")
    @Expose
    private String udcTmarksScored;
    @SerializedName("udc_tmarks_total")
    @Expose
    private String udcTmarksTotal;
    @SerializedName("udc_test_date")
    @Expose
    private String udcTestDate;
    @SerializedName("udc_teachers_treview")
    @Expose
    private String udcTeachersTreview;

    String exam="";
    String term="";
    String classid="";
    String division="";
    String subjectid="";

    public String getExam() {
        return exam;
    }

    public void setExam(String exam) {
        this.exam = exam;
    }

    public String getTerm() {
        return term;
    }

    public void setTerm(String term) {
        this.term = term;
    }

    public String getClassid() {
        return classid;
    }

    public void setClassid(String classid) {
        this.classid = classid;
    }

    public String getDivision() {
        return division;
    }

    public void setDivision(String division) {
        this.division = division;
    }

    public String getSubjectid() {
        return subjectid;
    }

    public void setSubjectid(String subjectid) {
        this.subjectid = subjectid;
    }

    public Mark() {
    }

    public String getUdcTestDate() {
        return udcTestDate;
    }

    public void setUdcTestDate(String udcTestDate) {
        this.udcTestDate = udcTestDate;
    }

    public String getUdcTmarksTotal() {
        return udcTmarksTotal;
    }

    public void setUdcTmarksTotal(String udcTmarksTotal) {
        this.udcTmarksTotal = udcTmarksTotal;
    }

    public String getUdcTestId() {
        return udcTestId;
    }

    public void setUdcTestId(String udcTestId) {
        this.udcTestId = udcTestId;
    }

    public String getRollnum() {
        return rollnum;
    }

    public void setRollnum(String rollnum) {
        this.rollnum = rollnum;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUdcTmarksScored() {
        return udcTmarksScored;
    }

    public void setUdcTmarksScored(String udcTmarksScored) {
        this.udcTmarksScored = udcTmarksScored;
    }

    public String getUdcTeachersTreview() {
        return udcTeachersTreview;
    }

    public void setUdcTeachersTreview(String udcTeachersTreview) {
        this.udcTeachersTreview = udcTeachersTreview;
    }
}
