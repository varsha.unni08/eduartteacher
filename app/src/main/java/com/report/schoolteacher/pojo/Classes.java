package com.report.schoolteacher.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Classes {

    @SerializedName("class_id")
    @Expose
    private String classId;
    @SerializedName("udc_class_details")
    @Expose
    private String udcClassDetails;

    public Classes() {
    }

    public String getClassId() {
        return classId;
    }

    public void setClassId(String classId) {
        this.classId = classId;
    }

    public String getUdcClassDetails() {
        return udcClassDetails;
    }

    public void setUdcClassDetails(String udcClassDetails) {
        this.udcClassDetails = udcClassDetails;
    }
}
