package com.report.schoolteacher.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.report.schoolteacher.R;
import com.report.schoolteacher.pojo.OnlineClass;
import com.report.schoolteacher.pojo.OnlineClassData;

import java.util.List;

public class OnlineClassAdapter extends RecyclerView.Adapter<OnlineClassAdapter.OnlineClassHolder> {

    Context context;
    List<OnlineClassData>onlineClasses;

    public OnlineClassAdapter(Context context, List<OnlineClassData> onlineClasses) {
        this.context = context;
        this.onlineClasses = onlineClasses;
    }

    public class OnlineClassHolder extends RecyclerView.ViewHolder {

        TextView txtdetails,txtlink,txtTime;
        public OnlineClassHolder(@NonNull View itemView) {
            super(itemView);

            txtdetails=itemView.findViewById(R.id.txtdetails);
            txtlink=itemView.findViewById(R.id.txtlink);
            txtTime=itemView.findViewById(R.id.txtTime);
        }
    }

    @NonNull
    @Override
    public OnlineClassHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        LayoutInflater layoutInflater=LayoutInflater.from(parent.getContext());
        View view=layoutInflater.inflate(R.layout.layout_onlineclass,parent,false);

        return new OnlineClassHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull OnlineClassHolder holder, int position) {

        holder.txtdetails.setText(onlineClasses.get(position).getClasstopic());

        holder.txtlink.setText(onlineClasses.get(position).getJoinlink());

        holder.txtTime.setText(onlineClasses.get(position).getUdc_weekday_name()+" "+onlineClasses.get(position).getTime());

    }

    @Override
    public int getItemCount() {
        return onlineClasses.size();
    }
}
