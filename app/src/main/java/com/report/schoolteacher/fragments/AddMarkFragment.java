package com.report.schoolteacher.fragments;


import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.Editable;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.JsonArray;
import com.report.schoolteacher.AppLiterals.Constants;
import com.report.schoolteacher.R;
import com.report.schoolteacher.activities.AddLessonPlanActivity;
import com.report.schoolteacher.activities.AttendanceActivity;
import com.report.schoolteacher.activities.StudentListActivity;
import com.report.schoolteacher.adapter.StudentByClassAdapter;
import com.report.schoolteacher.adapter.StudentListAdapter;
import com.report.schoolteacher.connectivity.NetConnection;
import com.report.schoolteacher.pojo.Classes;
import com.report.schoolteacher.pojo.DivisionStatus;
import com.report.schoolteacher.pojo.Divisions;
import com.report.schoolteacher.pojo.MarkCriteria;
import com.report.schoolteacher.pojo.StudentByClass;
import com.report.schoolteacher.pojo.StudentByClassStatus;
import com.report.schoolteacher.pojo.StudentData;
import com.report.schoolteacher.pojo.Studentinfo;
import com.report.schoolteacher.pojo.Students;
import com.report.schoolteacher.pojo.Subjects;
import com.report.schoolteacher.progress.ProgressFragment;
import com.report.schoolteacher.webservice.RetrofitHelper;
import com.report.schoolteacher.webservice.WebInterfaceimpl;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * A simple {@link Fragment} subclass.
 */
public class AddMarkFragment extends Fragment {


    public AddMarkFragment() {
        // Required empty public constructor
    }

    View view;

    Button btnSubmit;




    TextView txtNodata,txtClass,txtDivision;

    ImageView imgdropdownClass,imgdropdownsubject;



    String classid="",divisionid="";

    ProgressFragment progressFragment;

    List<Studentinfo>studentByClasses;

    TextView txtSubject;

    ImageView imgdropdownsubject1;

    String subjectid="";


    TextView txtExamSelection;
    ImageView imgselectExam;


    TextView txtTerm;
    ImageView imgSelectTerm;

    TextView txtStartdate;
    ImageView imgpickFromDate;

    String end_date="";

    String examtype="",term="";

     EditText edtTotalMarks;
     EditText edtAcheivedMarks;

     EditText edtDescription;

     StudentByClass studentByClass=null;

    Dialog  dialog=null;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view= inflater.inflate(R.layout.fragment_add_mark, container, false);

        btnSubmit=view.findViewById(R.id.btnSubmit);

        txtNodata=view.findViewById(R.id.txtNodata);
        txtClass=view.findViewById(R.id.txtClass);
        txtDivision=view.findViewById(R.id.txtDivision);

        studentByClasses=new ArrayList<>();

        imgdropdownClass=view.findViewById(R.id.imgdropdownClass);
        imgdropdownsubject=view.findViewById(R.id.imgdivision);





        txtSubject=view.findViewById(R.id.txtSubject);

        imgdropdownsubject1=view.findViewById(R.id.imgdropdownsubject);

        txtExamSelection=view.findViewById(R.id.txtExamSelection);
        imgselectExam=view.findViewById(R.id.imgselectExam);


        txtTerm=view.findViewById(R.id.txtTerm);
        imgSelectTerm=view.findViewById(R.id.imgSelectTerm);

        txtStartdate=view.findViewById(R.id.txtStartdate);
        imgpickFromDate=view.findViewById(R.id.imgpickFromDate);

        edtTotalMarks=view.findViewById(R.id.edtTotalMarks);

        txtClass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                getClasses();

            }
        });

        imgdropdownClass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                getClasses();
            }
        });


        txtDivision.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                getDivisions();

            }
        });

        imgdropdownsubject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                getDivisions();
            }
        });


        imgpickFromDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                showDatePicker();

            }
        });

        txtStartdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                showDatePicker();

            }
        });

        txtSubject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                getSubjects();
            }
        });

        imgdropdownsubject1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                getSubjects();

            }
        });

        txtExamSelection.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                showExamList();
            }
        });

        imgselectExam.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                showExamList();

            }
        });

        txtTerm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                showTermList();
            }
        });

        imgSelectTerm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                showTermList();

            }
        });

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {




                        if(!subjectid.equals(""))
                        {
                            if(!examtype.equals(""))
                            {
                                if(!term.equals(""))
                                {

                                    if(!end_date.equals(""))
                                    {

                                        final Calendar cldr = Calendar.getInstance();
                                        final int day = cldr.get(Calendar.DAY_OF_MONTH);
                                        final int month = cldr.get(Calendar.MONTH);
                                        final int year = cldr.get(Calendar.YEAR);

                                        Date sta_date = null;
                                        Date end_dat = null;

                                        int m1=month+1;

                                        String currentdate=year+"-"+m1+"-"+day;


                                        try {

                                            DateFormat dateFormat = new SimpleDateFormat("yyyy-mm-dd");
                                            sta_date = dateFormat.parse(currentdate);
                                            end_dat = dateFormat.parse(end_date);

                                            if (end_dat.before(sta_date)) {

                                                if(!edtTotalMarks.getText().toString().equals(""))
                                                {


                                                    getStudentListByClass();



                                                }
                                                else {

                                                    Toast.makeText(getActivity(),"enter total marks",Toast.LENGTH_SHORT).show();

                                                }


                                            }
                                            else {

                                                Toast.makeText(getActivity(),"select date properly",Toast.LENGTH_SHORT).show();

                                            }

                                        }catch (Exception e)
                                        {

                                        }



                                    }
                                    else {

                                        Toast.makeText(getActivity(),"Select exam date",Toast.LENGTH_SHORT).show();
                                    }


                                }
                                else {

                                    Toast.makeText(getActivity(),"Select a term",Toast.LENGTH_SHORT).show();
                                }

                            }
                            else {

                                Toast.makeText(getActivity(),"Select a exam type",Toast.LENGTH_SHORT).show();
                            }

                        }
                        else {

                            Toast.makeText(getActivity(),"Select a subject",Toast.LENGTH_SHORT).show();
                        }



            }
        });






        return view;
    }





    public void getStudentListByClass() {

        progressFragment = new ProgressFragment();
        progressFragment.show(getFragmentManager(), "djaisjfk");

        Retrofit retrofitHelper = RetrofitHelper.getRetrofitInstance(getActivity());

        WebInterfaceimpl webInterfaceimpl = retrofitHelper.create(WebInterfaceimpl.class);

        Call<StudentData>listCall=webInterfaceimpl.getAllStudentslist(Constants.classid,Constants.divisionid);
        listCall.enqueue(new Callback<StudentData>() {
            @Override
            public void onResponse(Call<StudentData> call, Response<StudentData> response) {
                progressFragment.dismiss();

                if(response.body()!=null)
                {

                    if(response.body().getData().size()>0)
                    {

                        studentByClasses.addAll(response.body().getData());

                        //layout_studentdata.setVisibility(View.VISIBLE);

//                        recycler_view.setLayoutManager(new LinearLayoutManager(getActivity()));
//                        recycler_view.setAdapter(new StudentByClassAdapter(getActivity(),response.body(),AddMarkFragment.this));


                        MarkCriteria markCriteria=new MarkCriteria();
                        markCriteria.setClassid(Constants.classid);
                        markCriteria.setDate(end_date);
                        markCriteria.setDivisionid(Constants.divisionid);
                        markCriteria.setExamtype(examtype);
                        markCriteria.setStudentByClasses(studentByClasses);
                        markCriteria.setSubjectid(subjectid);
                        markCriteria.setTerm(term);
                        markCriteria.setTotalmarke(edtTotalMarks.getText().toString());

                        Intent intent=new Intent(getActivity(), StudentListActivity.class);
                        intent.putExtra("markCriteria",markCriteria);
                        startActivity(intent);





                    }
                    else {

                        //layout_studentdata.setVisibility(View.GONE);

                        Toast.makeText(getActivity(),"No data found",Toast.LENGTH_SHORT).show();

                    }


                }
                else {


                  //  layout_studentdata.setVisibility(View.GONE);
                    Toast.makeText(getActivity(),"No data found",Toast.LENGTH_SHORT).show();

                }
            }

            @Override
            public void onFailure(Call<StudentData> call, Throwable t) {
                progressFragment.dismiss();

                if(!NetConnection.isConnected(getActivity()))
                {
                    Toast.makeText(getActivity(),"Check Internet connection",Toast.LENGTH_SHORT).show();
                }


            }
        });


    }












    public void showDatePicker() {
        final Calendar cldr = Calendar.getInstance();
        final int day = cldr.get(Calendar.DAY_OF_MONTH);
        final int month = cldr.get(Calendar.MONTH);
        final int year = cldr.get(Calendar.YEAR);

        DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {



                int m = i1 + 1;




                end_date = i + "-" + m + "-" + i2;
                txtStartdate.setText(end_date);





            }
        }, year, month, day);

        datePickerDialog.show();
    }



    public void getSubjects()
    {
        progressFragment = new ProgressFragment();
        progressFragment.show(getFragmentManager(), "cjkk");

        Retrofit retrofitHelper = RetrofitHelper.getRetrofitInstance(getActivity());

        WebInterfaceimpl webInterfaceimpl = retrofitHelper.create(WebInterfaceimpl.class);

        Call<List<Subjects>> listCall=webInterfaceimpl.getSubjects();

        listCall.enqueue(new Callback<List<Subjects>>() {
            @Override
            public void onResponse(Call<List<Subjects>> call, Response<List<Subjects>> response) {

                progressFragment.dismiss();
                if(response.body()!=null)
                {

                    if(response.body().size()>0)
                    {


                        showSubjectList(response.body());



                    }
                    else {

                        Toast.makeText(getActivity(),"No data found",Toast.LENGTH_SHORT).show();

                    }


                }
                else {

                    Toast.makeText(getActivity(),"No data found",Toast.LENGTH_SHORT).show();

                }


            }

            @Override
            public void onFailure(Call<List<Subjects>> call, Throwable t) {
                progressFragment.dismiss();

                if(!NetConnection.isConnected(getActivity()))
                {
                    Toast.makeText(getActivity(),"Check Internet connection",Toast.LENGTH_SHORT).show();
                }
            }
        });



    }




    public void showSubjectList(final List<Subjects> subjects)
    {
        List<String>strings=new ArrayList<>();
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Choose a subject");
        ;

        for (Subjects subjects1:subjects
        ) {

            strings.add(subjects1.getUdcSubjectDetails());

        }



        builder.setItems(strings.toArray(new String[strings.size()]), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                dialog.dismiss();

                txtSubject.setText(subjects.get(which).getUdcSubjectDetails());

                subjectid=subjects.get(which).getSubjectId();

                // showLessonPlanlist();

            }
        });

// create and show the alert dialog
        AlertDialog dialog = builder.create();
        dialog.show();
    }




    public void getClasses()
    {

        progressFragment=new ProgressFragment();
        progressFragment.show(getFragmentManager(),"djaisjfk");

        Retrofit retrofitHelper = RetrofitHelper.getRetrofitInstance(getActivity());

        WebInterfaceimpl webInterfaceimpl = retrofitHelper.create(WebInterfaceimpl.class);

        Call<List<Classes>> listCall=webInterfaceimpl.getClasses();

        listCall.enqueue(new Callback<List<Classes>>() {
            @Override
            public void onResponse(Call<List<Classes>> call, Response<List<Classes>> response) {

                progressFragment.dismiss();

                if(response.body()!=null)
                {

                    if(response.body().size()>0)
                    {



                        showClassList(response.body());



                    }
                    else {

                        Toast.makeText(getActivity(),"No data found",Toast.LENGTH_SHORT).show();

                    }


                }
                else {

                    Toast.makeText(getActivity(),"No data found",Toast.LENGTH_SHORT).show();

                }

            }

            @Override
            public void onFailure(Call<List<Classes>> call, Throwable t) {

            progressFragment.dismiss();

                if(!NetConnection.isConnected(getActivity()))
                {
                    Toast.makeText(getActivity(),"Check Internet connection",Toast.LENGTH_SHORT).show();
                }

            }
        });

    }



    public void showClassList(final List<Classes>classes)
    {

        List<String>strings=new ArrayList<>();
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Choose a class");
        ;

        for (Classes student:classes
        ) {

            strings.add(student.getUdcClassDetails());

        }



        builder.setItems(strings.toArray(new String[strings.size()]), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                dialog.dismiss();

                txtClass.setText(classes.get(which).getUdcClassDetails());

                classid=classes.get(which).getClassId();

                // showLessonPlanlist();

            }
        });

// create and show the alert dialog
        AlertDialog dialog = builder.create();
        dialog.show();
    }


    public void getDivisions()
    {

        progressFragment=new ProgressFragment();
        progressFragment.show(getFragmentManager(),"dgl");

        Retrofit retrofitHelper = RetrofitHelper.getRetrofitInstance(getActivity());

        WebInterfaceimpl webInterfaceimpl = retrofitHelper.create(WebInterfaceimpl.class);

        Call<DivisionStatus> listCall=webInterfaceimpl.getDivisionData(classid);

        listCall.enqueue(new Callback<DivisionStatus>() {
            @Override
            public void onResponse(Call<DivisionStatus> call, Response<DivisionStatus> response) {
               progressFragment.dismiss();

                if(response.body()!=null)
                {

                    if(response.body().getData().size()>0)
                    {


                        showDivisionList(response.body().getData());



                    }
                    else {

                        Toast.makeText(getActivity(),"No data found",Toast.LENGTH_SHORT).show();

                    }


                }
                else {

                    Toast.makeText(getActivity(),"No data found",Toast.LENGTH_SHORT).show();

                }


            }

            @Override
            public void onFailure(Call<DivisionStatus> call, Throwable t) {
           progressFragment.dismiss();

                if(!NetConnection.isConnected(getActivity()))
                {
                    Toast.makeText(getActivity(),"Check Internet connection",Toast.LENGTH_SHORT).show();
                }
            }
        });



    }


    public void showExamList()
    {


        final String arr[]={" Assessment tests","Main Exam"};

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Choose an Exam");
        ;



        builder.setItems(arr, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                dialog.dismiss();

                txtExamSelection.setText(arr[which]);

                int t=which+1;

                examtype=String.valueOf(t);

            }
        });

// create and show the alert dialog
        AlertDialog dialog = builder.create();
        dialog.show();
    }


    public void showTermList()
    {


        final String arr[]={"Quarterly","Half yearly","Annual exam"};

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Choose a Term");
        ;



        builder.setItems(arr, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                dialog.dismiss();

                int w=which+1;

                txtTerm.setText(arr[which]);

                term=String.valueOf(w);

            }
        });

// create and show the alert dialog
        AlertDialog dialog = builder.create();
        dialog.show();
    }



    public void showDivisionList(final List<Divisions> divisions)
    {
        List<String>strings=new ArrayList<>();
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Choose a division");
        ;

        for (Divisions divi:divisions
        ) {

            strings.add(divi.getUdcDivisionName());

        }



        builder.setItems(strings.toArray(new String[strings.size()]), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                dialog.dismiss();

                txtDivision.setText(divisions.get(which).getUdcDivisionName());

                divisionid=divisions.get(which).getUdcDivision();

                // showLessonPlanlist();

            }
        });

// create and show the alert dialog
        AlertDialog dialog = builder.create();
        dialog.show();
    }

}
