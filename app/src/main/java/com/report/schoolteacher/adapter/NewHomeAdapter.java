package com.report.schoolteacher.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.recyclerview.widget.RecyclerView;

import com.report.schoolteacher.AppLiterals.Constants;
import com.report.schoolteacher.R;
import com.report.schoolteacher.activities.AttendanceActivity;
import com.report.schoolteacher.activities.CircularActivity;
import com.report.schoolteacher.activities.ExamActivity;
import com.report.schoolteacher.activities.LeaveActivity;
import com.report.schoolteacher.activities.LessonPlanActivity;
import com.report.schoolteacher.activities.OnlineClassActivity;
import com.report.schoolteacher.activities.TimetableActivity;
import com.report.schoolteacher.activities.ViewMarklistActivity;
import com.report.schoolteacher.utils.Utilities;

public class NewHomeAdapter extends RecyclerView.Adapter<NewHomeAdapter.HomeHolder> {

    Context context;

    public NewHomeAdapter(Context context) {
        this.context = context;
    }

    public class HomeHolder extends RecyclerView.ViewHolder{

        AppCompatImageView img;
        TextView txtdata;

        public HomeHolder(@NonNull View itemView) {
            super(itemView);

            img=itemView.findViewById(R.id.img);
            txtdata=itemView.findViewById(R.id.txtdata);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {


                    switch (getAdapterPosition())
                    {

                        case 0:

                            if(!Constants.classid.equalsIgnoreCase("0")&&!Constants.divisionid.equalsIgnoreCase("0")) {

                                Intent intent00 = new Intent(context, AttendanceActivity.class);
                                context.startActivity(intent00);
                            }
                            else{

                                Utilities.showInfo(context,"Select class and division");
                            }
                            break;

                        case 1:



                            if(!Constants.classid.equalsIgnoreCase("0")&&!Constants.divisionid.equalsIgnoreCase("0")) {

                                Intent i=new Intent(context, ViewMarklistActivity.class);
                                context.startActivity(i);
                            }
                            else{

                                Utilities.showInfo(context,"Select class and division");
                            }
                            break;
//

                        case 2:



                            if(!Constants.classid.equalsIgnoreCase("0")&&!Constants.divisionid.equalsIgnoreCase("0")) {
                                Intent intent2=new Intent(context, OnlineClassActivity.class);
                                context.startActivity(intent2);
                            }
                            else{

                                Utilities.showInfo(context,"Select class and division");
                            }
                           break;
                        case 3:


                            if(!Constants.classid.equalsIgnoreCase("0")&&!Constants.divisionid.equalsIgnoreCase("0")) {

                                Intent intent=new Intent(context, LessonPlanActivity.class);
                                context.startActivity(intent);
                            }
                            else{

                                Utilities.showInfo(context,"Select class and division");
                            }
                            break;
//
                        case 4:



                            if(!Constants.classid.equalsIgnoreCase("0")&&!Constants.divisionid.equalsIgnoreCase("0")) {
                                Intent intent5=new Intent(context, LeaveActivity.class);
                                context.startActivity(intent5);
                            }
                            else{

                                Utilities.showInfo(context,"Select class and division");
                            }
                            break;

//
                        case 5:




                            if(!Constants.classid.equalsIgnoreCase("0")&&!Constants.divisionid.equalsIgnoreCase("0")) {
                                Intent intent7=new Intent(context, CircularActivity.class);
                                context.startActivity(intent7);
                            }
                            else{

                                Utilities.showInfo(context,"Select class and division");
                            }
                            break;



                    }

                }
            });
        }
    }


    @NonNull
    @Override
    public HomeHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View v= LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_homeadapter, parent, false);

        return new HomeHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull HomeHolder holder, int position) {
        holder.img.setImageResource(Constants.firstarr_img[position]);

        holder.txtdata.setText(Constants.firsthomearr[position]);
    }

    @Override
    public int getItemCount() {
        return Constants.firsthomearr.length;
    }
}
