package com.report.schoolteacher.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.report.schoolteacher.R;
import com.report.schoolteacher.pojo.Period;
import com.report.schoolteacher.pojo.Periods;
import com.report.schoolteacher.pojo.Timetable;

import java.util.ArrayList;
import java.util.List;

public class TimetableAdapter extends RecyclerView.Adapter<TimetableAdapter.TimeTableHolder> {

    Context context;

    List<Timetable>timetables;
    List<Period>periods;

    public TimetableAdapter(Context context, List<Timetable> timetables, List<Period>periods) {
        this.context = context;
        this.timetables = timetables;
        this.periods=periods;
    }

    public class TimeTableHolder extends RecyclerView.ViewHolder{

        TextView txtTimetable;

        AppCompatImageView imgDropdown;

        RecyclerView recycler_view;

        public TimeTableHolder(@NonNull View itemView) {
            super(itemView);
            txtTimetable=itemView.findViewById(R.id.txtdetails);
            imgDropdown=itemView.findViewById(R.id.imgDropdown);

            recycler_view=itemView.findViewById(R.id.recycler_view);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Timetable timetable=timetables.get(getAdapterPosition());

                    if(timetable.getSelected()==0)
                    {

                        for (Timetable timetable1:timetables
                             ) {
                            timetable1.setSelected(0);
                        }
                        timetables.get(getAdapterPosition()).setSelected(1);







                    }
                    else {

                        for (Timetable timetable1:timetables
                        ) {
                            timetable1.setSelected(0);
                        }
                    }

                    notifyItemRangeChanged(0,timetables.size());
                }
            });
        }
    }

    @NonNull
    @Override
    public TimeTableHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_timetable_adapter,parent,false);

        return new TimeTableHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull TimeTableHolder holder, int position) {

        holder.txtTimetable.setText(timetables.get(position).getUdcWeekdayName());

        if(timetables.get(position).getSelected()==0)
        {
            holder.recycler_view.setVisibility(View.GONE);
            holder.imgDropdown.setImageResource(R.drawable.ic_arrow_drop_down);
        }
        else {

//            Timetable timetable=timetables.get(position);
//
//            holder.recycler_view.setVisibility(View.VISIBLE);
            holder.imgDropdown.setImageResource(R.drawable.ic_arrow_drop_up);
//
//            List<Periods>periods=new ArrayList<>();
//            Periods periods1=new Periods();
//            periods1.setClassname(timetable.getClass1());
//            periods1.setPeriod(timetable.getPeriod1());
//            periods.add(periods1);
//
//            Periods periods2=new Periods();
//            periods2.setClassname(timetable.getClass2());
//            periods2.setPeriod(timetable.getPeriod2());
//            periods.add(periods2);
//
//            Periods periods3=new Periods();
//            periods3.setClassname(timetable.getClass3());
//            periods3.setPeriod(timetable.getPeriod3());
//            periods.add(periods3);
//
//
//            Periods periods4=new Periods();
//            periods4.setClassname(timetable.getClass4());
//            periods4.setPeriod(timetable.getPeriod4());
//            periods.add(periods4);
//
//
//            Periods periods5=new Periods();
//            periods5.setClassname(timetable.getClass5());
//            periods5.setPeriod(timetable.getPeriod5());
//            periods.add(periods5);
//
//            Periods periods6=new Periods();
//            periods6.setClassname(timetable.getClass6());
//            periods6.setPeriod(timetable.getPeriod6());
//            periods.add(periods6);
//
//            Periods periods7=new Periods();
//            periods7.setClassname(timetable.getClass7());
//            periods7.setPeriod(timetable.getPeriod7());
//            periods.add(periods7);

            holder.recycler_view.setVisibility(View.VISIBLE);

            List<Timetable>ttl=new ArrayList<>();

            for(Timetable t:timetables)
            {
                if(t.getUdcWeekdayName().equalsIgnoreCase(periods.get(position).getDay()))
                {

                    ttl.add(t);

                }

            }


//
            holder.recycler_view.setLayoutManager(new LinearLayoutManager(context));
            holder.recycler_view.setAdapter(new PeriodAdapter(context,ttl));
        }

    }

    @Override
    public int getItemCount() {
        return periods.size();
    }
}
