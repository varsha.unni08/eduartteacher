package com.report.schoolteacher.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.report.schoolteacher.AppLiterals.Constants;
import com.report.schoolteacher.R;
import com.report.schoolteacher.adapter.ComplaintAdapter;
import com.report.schoolteacher.adapter.OnlineClassAdapter;
import com.report.schoolteacher.connectivity.NetConnection;
import com.report.schoolteacher.pojo.Complaints;
import com.report.schoolteacher.pojo.OnlineClass;
import com.report.schoolteacher.progress.ProgressFragment;
import com.report.schoolteacher.webservice.RetrofitHelper;
import com.report.schoolteacher.webservice.WebInterfaceimpl;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class OnlineClassActivity extends AppCompatActivity {
    
    RecyclerView recyclerview;
    ImageView imgback;

    ProgressFragment progressFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_online_class);
        getSupportActionBar().hide();

        imgback=findViewById(R.id.imgback);
        recyclerview=findViewById(R.id.recyclerview);


        showOnlineClass();
    }
    
    
    public void showOnlineClass()
    {

        progressFragment = new ProgressFragment();
        progressFragment.show(getSupportFragmentManager(), "cjkk");

        Retrofit retrofitHelper = RetrofitHelper.getRetrofitInstance(OnlineClassActivity.this);

        WebInterfaceimpl webInterfaceimpl = retrofitHelper.create(WebInterfaceimpl.class);

        Call<OnlineClass> listCall=webInterfaceimpl.getOnlineClass(Constants.classid,Constants.divisionid);

        listCall.enqueue(new Callback<OnlineClass>() {
            @Override
            public void onResponse(Call<OnlineClass> call, Response<OnlineClass> response) {
                progressFragment.dismiss();
                if (response.body().getStatus()==1 ) {

                    if (response.body().getData()!=null) {

                        if (response.body().getData().size() > 0) {

                            recyclerview.setVisibility(View.VISIBLE);

                            recyclerview.setLayoutManager(new LinearLayoutManager(OnlineClassActivity.this));
                            recyclerview.setAdapter(new OnlineClassAdapter(OnlineClassActivity.this, response.body().getData()));


                        } else {

                            recyclerview.setVisibility(View.GONE);
                            Toast.makeText(OnlineClassActivity.this, "No data found", Toast.LENGTH_SHORT).show();

                        }
                    }
                    else {

                        recyclerview.setVisibility(View.GONE);
                        Toast.makeText(OnlineClassActivity.this, "No data found", Toast.LENGTH_SHORT).show();

                    }

                } else {

                    recyclerview.setVisibility(View.GONE);
                    Toast.makeText(OnlineClassActivity.this, "No data found", Toast.LENGTH_SHORT).show();

                }
            }

            @Override
            public void onFailure(Call<OnlineClass> call, Throwable t) {
                progressFragment.dismiss();

                if (!NetConnection.isConnected(OnlineClassActivity.this)) {
                    Toast.makeText(OnlineClassActivity.this, "Check Internet connection", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}