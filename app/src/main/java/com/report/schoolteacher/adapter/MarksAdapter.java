package com.report.schoolteacher.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.report.schoolteacher.R;
import com.report.schoolteacher.fragments.MarklistFragment;
import com.report.schoolteacher.pojo.Mark;
import com.report.schoolteacher.pojo.MarkData;

import java.util.List;

public class MarksAdapter  extends RecyclerView.Adapter<MarksAdapter.MarkHolder> {

    Context context;
    List<MarkData>marks;

    MarklistFragment marklistFragment;

    public MarksAdapter(Context context, List<MarkData> marks, MarklistFragment marklistFragment) {
        this.context = context;
        this.marks = marks;
        this.marklistFragment=marklistFragment;
    }

    public class MarkHolder extends RecyclerView.ViewHolder{

        TextView txtName,txtMarks,txtReview;

        ImageView imgEdit;

        public MarkHolder(@NonNull View itemView) {
            super(itemView);
            txtName=itemView.findViewById(R.id.txtName);
            txtMarks=itemView.findViewById(R.id.txtMarks);
            txtReview=itemView.findViewById(R.id.txtReview);

            imgEdit=itemView.findViewById(R.id.imgEdit);

            imgEdit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                   // marklistFragment.EditMark(marks.get(getAdapterPosition()));

                }
            });
        }
    }

    @NonNull
    @Override
    public MarkHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_marklistadapter,parent,false);


        return new MarkHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MarkHolder holder, int position) {

        holder.txtName.setText(marks.get(position).getUdcStudentName());
        holder.txtMarks.setText(marks.get(position).getUdcTmarksScored());
        holder.txtReview.setText(marks.get(position).getUdc_teachers_review());

    }

    @Override
    public int getItemCount() {
        return marks.size();
    }
}
