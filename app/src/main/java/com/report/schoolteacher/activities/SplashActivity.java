package com.report.schoolteacher.activities;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;

import com.report.schoolteacher.R;
import com.report.schoolteacher.appconstants.Literals;
import com.report.schoolteacher.preferencehelper.PreferenceHelper;

public class SplashActivity extends AppCompatActivity {


    Thread thread;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        getSupportActionBar().hide();

        thread=new Thread(new Runnable() {
            @Override
            public void run() {

                try{
                    thread.sleep(3000);


                    if(!new PreferenceHelper(SplashActivity.this).getData(Literals.Logintokenkey).equals(""))

                    {
                        startActivity(new Intent(SplashActivity.this, HomeActivity.class));
                        //overridePendingTransition(R.anim.slide_in_left,R.anim.slide_in_right);

                        finish();
                    }
                    else {


                       startActivity(new Intent(SplashActivity.this, LoginActivity.class));
                       // overridePendingTransition(R.anim.slide_in_left,R.anim.slide_in_right);
                        finish();
                    }


                }catch (Exception e)
                {

                }

            }
        });

        thread.start();
    }
}
