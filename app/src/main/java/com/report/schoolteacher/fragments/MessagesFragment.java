package com.report.schoolteacher.fragments;


import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.gson.JsonArray;
import com.report.schoolteacher.AppLiterals.Constants;
import com.report.schoolteacher.R;
import com.report.schoolteacher.activities.AddLessonPlanActivity;
import com.report.schoolteacher.adapter.TeacherMessageAdapter;
import com.report.schoolteacher.connectivity.NetConnection;
import com.report.schoolteacher.pojo.Classes;
import com.report.schoolteacher.pojo.Divisions;
import com.report.schoolteacher.pojo.Subjects;
import com.report.schoolteacher.pojo.TeacherMessageStatus;
import com.report.schoolteacher.pojo.TeacherMessages;
import com.report.schoolteacher.progress.ProgressFragment;
import com.report.schoolteacher.webservice.RetrofitHelper;
import com.report.schoolteacher.webservice.WebInterfaceimpl;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * A simple {@link Fragment} subclass.
 */
public class MessagesFragment extends Fragment {


    public MessagesFragment() {
        // Required empty public constructor
    }

    RecyclerView recycler_view;
    TextView txtnodatafound;
    TextView txtClass;

    View view;

    FloatingActionButton fab;

    TextView txtprogress,txtDivision;

    String classid="",divisionid="";
    Dialog dialog=null;

    ProgressFragment progressFragment;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view= inflater.inflate(R.layout.fragment_messages, container, false);
        recycler_view=view.findViewById(R.id.recycler_view);

        txtnodatafound=view.findViewById(R.id.txtnodatafound);

        fab=view.findViewById(R.id.fab);

        getMessages();

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                 dialog=new Dialog(getActivity());
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.layout_addmessage);

                 txtClass=dialog.findViewById(R.id.txtClass);
                ImageView imgdropdownClass=dialog.findViewById(R.id.imgdropdownClass);

                 txtprogress=dialog.findViewById(R.id.txtprogress);
                txtDivision=dialog.findViewById(R.id.txtDivision);

                ImageView imgdropdownsubject=dialog.findViewById(R.id.imgdropdownsubject);
                final EditText edtTopic=dialog.findViewById(R.id.edtTopic);
                final EditText edtDescription=dialog.findViewById(R.id.edtDescription);

                Button btnAddMessage=dialog.findViewById(R.id.btnAddMessage);




                DisplayMetrics displayMetrics = new DisplayMetrics();
               getActivity().getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
                int widthLcl = (int) (displayMetrics.widthPixels);
                int heightLcl = (int) (displayMetrics.heightPixels);

                dialog.getWindow().setLayout(widthLcl,heightLcl);

                btnAddMessage.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {



                                if(!edtTopic.getText().toString().equals(""))
                                {


                                    if(!edtDescription.getText().toString().equals(""))
                                    {

                                        txtprogress.setVisibility(View.VISIBLE);



postMessages(Constants.classid,Constants.divisionid,edtTopic.getText().toString(),edtDescription.getText().toString());



                                    }
                                    else {

                                        Toast.makeText(getActivity(),"enter description",Toast.LENGTH_SHORT).show();
                                    }

                                }
                                else {

                                    Toast.makeText(getActivity(),"enter topic",Toast.LENGTH_SHORT).show();
                                }






                    }
                });

                txtDivision.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        txtprogress.setVisibility(View.VISIBLE);

                        getDivisions();

                    }
                });

                imgdropdownsubject.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        txtprogress.setVisibility(View.VISIBLE);

                        getDivisions();

                    }
                });

                txtClass.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        txtprogress.setVisibility(View.VISIBLE);
                        getClasses();

                    }
                });

                imgdropdownClass.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        txtprogress.setVisibility(View.VISIBLE);

                        getClasses();

                    }
                });





                dialog.show();




            }
        });

        return view;
    }


    public void getClasses()
    {


        Retrofit retrofitHelper = RetrofitHelper.getRetrofitInstance(getActivity());

        WebInterfaceimpl webInterfaceimpl = retrofitHelper.create(WebInterfaceimpl.class);

        Call<List<Classes>> listCall=webInterfaceimpl.getClasses();

        listCall.enqueue(new Callback<List<Classes>>() {
            @Override
            public void onResponse(Call<List<Classes>> call, Response<List<Classes>> response) {
                txtprogress.setVisibility(View.GONE);

                if(response.body()!=null)
                {

                    if(response.body().size()>0)
                    {



                        showClassList(response.body());



                    }
                    else {

                        Toast.makeText(getActivity(),"No data found",Toast.LENGTH_SHORT).show();

                    }


                }
                else {

                    Toast.makeText(getActivity(),"No data found",Toast.LENGTH_SHORT).show();

                }

            }

            @Override
            public void onFailure(Call<List<Classes>> call, Throwable t) {

                txtprogress.setVisibility(View.GONE);

                if(!NetConnection.isConnected(getActivity()))
                {
                    Toast.makeText(getActivity(),"Check Internet connection",Toast.LENGTH_SHORT).show();
                }

            }
        });

    }



    public void showClassList(final List<Classes>classes)
    {

        List<String>strings=new ArrayList<>();
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Choose a class");
        ;

        for (Classes student:classes
        ) {

            strings.add(student.getUdcClassDetails());

        }



        builder.setItems(strings.toArray(new String[strings.size()]), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                dialog.dismiss();

                txtClass.setText(classes.get(which).getUdcClassDetails());

                classid=classes.get(which).getClassId();

                // showLessonPlanlist();

            }
        });

// create and show the alert dialog
        AlertDialog dialog = builder.create();
        dialog.show();
    }


    public void getDivisions()
    {

        Retrofit retrofitHelper = RetrofitHelper.getRetrofitInstance(getActivity());

        WebInterfaceimpl webInterfaceimpl = retrofitHelper.create(WebInterfaceimpl.class);

        Call<List<Divisions>> listCall=webInterfaceimpl.getDivisions(classid);

        listCall.enqueue(new Callback<List<Divisions>>() {
            @Override
            public void onResponse(Call<List<Divisions>> call, Response<List<Divisions>> response) {
                txtprogress.setVisibility(View.GONE);

                if(response.body()!=null)
                {

                    if(response.body().size()>0)
                    {


                        showDivisionList(response.body());



                    }
                    else {

                        Toast.makeText(getActivity(),"No data found",Toast.LENGTH_SHORT).show();

                    }


                }
                else {

                    Toast.makeText(getActivity(),"No data found",Toast.LENGTH_SHORT).show();

                }


            }

            @Override
            public void onFailure(Call<List<Divisions>> call, Throwable t) {
                txtprogress.setVisibility(View.GONE);

                if(!NetConnection.isConnected(getActivity()))
                {
                    Toast.makeText(getActivity(),"Check Internet connection",Toast.LENGTH_SHORT).show();
                }
            }
        });



    }




    public void showDivisionList(final List<Divisions> divisions)
    {
        List<String>strings=new ArrayList<>();
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Choose a division");
        ;

        for (Divisions divi:divisions
        ) {

            strings.add(divi.getUdcDivisionName());

        }



        builder.setItems(strings.toArray(new String[strings.size()]), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                dialog.dismiss();

                txtDivision.setText(divisions.get(which).getUdcDivisionName());

                divisionid=divisions.get(which).getUdcDivision();

                // showLessonPlanlist();

            }
        });

// create and show the alert dialog
        AlertDialog dialog = builder.create();
        dialog.show();
    }


    public void getMessages() {
        progressFragment=new ProgressFragment();
        progressFragment.show(getFragmentManager(),"dsifdsj");

        Retrofit retrofitHelper = RetrofitHelper.getRetrofitInstance(getActivity());

        WebInterfaceimpl webInterfaceimpl = retrofitHelper.create(WebInterfaceimpl.class);

        Call<TeacherMessageStatus>listCall=webInterfaceimpl.getTeacherMessages();

        listCall.enqueue(new Callback<TeacherMessageStatus>() {
            @Override
            public void onResponse(Call<TeacherMessageStatus> call, Response<TeacherMessageStatus> response) {

                progressFragment.dismiss();
                if(response.body()!=null)
                {

                    if(response.body().getData().size()>0)
                    {


                        recycler_view.setVisibility(View.VISIBLE);
                        txtnodatafound.setVisibility(View.GONE);

                        recycler_view.setLayoutManager(new LinearLayoutManager(getActivity()));
                        recycler_view.setAdapter(new TeacherMessageAdapter(getActivity(),response.body().getData()));




                    }
                    else {

                        recycler_view.setVisibility(View.GONE);
                        txtnodatafound.setVisibility(View.VISIBLE);

                        Toast.makeText(getActivity(),"No data found",Toast.LENGTH_SHORT).show();

                    }


                }
                else {

                    recycler_view.setVisibility(View.GONE);
                    txtnodatafound.setVisibility(View.VISIBLE);

                    Toast.makeText(getActivity(),"No data found",Toast.LENGTH_SHORT).show();

                }




            }

            @Override
            public void onFailure(Call<TeacherMessageStatus> call, Throwable t) {

                progressFragment.dismiss();
                if(!NetConnection.isConnected(getActivity()))
                {
                    Toast.makeText(getActivity(),"Check Internet connection",Toast.LENGTH_SHORT).show();
                }
            }
        });

    }


    public void postMessages(String classid,String divisionid,String topic,String message) {

        Retrofit retrofitHelper = RetrofitHelper.getRetrofitInstance(getActivity());

        WebInterfaceimpl webInterfaceimpl = retrofitHelper.create(WebInterfaceimpl.class);

        Call<JsonArray> listCall = webInterfaceimpl.addMessage(classid,divisionid,topic,message);
        listCall.enqueue(new Callback<JsonArray>() {
            @Override
            public void onResponse(Call<JsonArray> call, Response<JsonArray> response) {
                txtprogress.setVisibility(View.GONE);

                dialog.dismiss();

                try{

                    JSONArray jsonArray=new JSONArray(response.body().toString());
                    if(jsonArray.length()>0)
                    {
                        JSONObject jsonObject=jsonArray.getJSONObject(0);


                      if(jsonObject.getInt("status")==1)
                      {


                          Toast.makeText(getActivity(),jsonObject.getString("message"),Toast.LENGTH_SHORT).show();


                          getMessages();

                      }



                    }



                }catch (Exception e)
                {

                }


            }

            @Override
            public void onFailure(Call<JsonArray> call, Throwable t) {
                txtprogress.setVisibility(View.GONE);
                dialog.dismiss();
                if(!NetConnection.isConnected(getActivity()))
                {
                    Toast.makeText(getActivity(),"Check Internet connection",Toast.LENGTH_SHORT).show();
                }
            }
        });
    }





    }
