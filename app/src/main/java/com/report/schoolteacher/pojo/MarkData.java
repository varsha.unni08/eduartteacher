package com.report.schoolteacher.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MarkData {

    @SerializedName("udc_tmarks_scored")
    @Expose
    private String udcTmarksScored;
    @SerializedName("udc_tmarks_total")
    @Expose
    private String udcTmarksTotal;
    @SerializedName("udc_student_name")
    @Expose
    private String udcStudentName;
    @SerializedName("udc_teachers_review")
    @Expose
    private String udc_teachers_review;

    public MarkData() {
    }

    public String getUdc_teachers_review() {
        return udc_teachers_review;
    }

    public void setUdc_teachers_review(String udc_teachers_review) {
        this.udc_teachers_review = udc_teachers_review;
    }

    public String getUdcTmarksScored() {
        return udcTmarksScored;
    }

    public void setUdcTmarksScored(String udcTmarksScored) {
        this.udcTmarksScored = udcTmarksScored;
    }

    public String getUdcTmarksTotal() {
        return udcTmarksTotal;
    }

    public void setUdcTmarksTotal(String udcTmarksTotal) {
        this.udcTmarksTotal = udcTmarksTotal;
    }

    public String getUdcStudentName() {
        return udcStudentName;
    }

    public void setUdcStudentName(String udcStudentName) {
        this.udcStudentName = udcStudentName;
    }
}
