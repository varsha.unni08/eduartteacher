package com.report.schoolteacher.activities;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.report.schoolteacher.R;
import com.report.schoolteacher.adapter.LessonPlanAdapter;
import com.report.schoolteacher.adapter.VideosAdapter;
import com.report.schoolteacher.connectivity.NetConnection;
import com.report.schoolteacher.pojo.Students;
import com.report.schoolteacher.pojo.Subjects;
import com.report.schoolteacher.pojo.VideoStatus;
import com.report.schoolteacher.pojo.Videos;
import com.report.schoolteacher.progress.ProgressFragment;
import com.report.schoolteacher.webservice.RetrofitHelper;
import com.report.schoolteacher.webservice.WebInterfaceimpl;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class VideosActivity extends AppCompatActivity {

    TextView txtStudent;
    ImageView imgdropdownStudent, imgback;

    ProgressFragment progressFragment;

    String studentid = "";

    RecyclerView recycler_view;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_videos);
        getSupportActionBar().hide();
        txtStudent = findViewById(R.id.txtStudent);
        imgdropdownStudent = findViewById(R.id.imgdropdownStudent);
        imgback = findViewById(R.id.imgback);
        recycler_view=findViewById(R.id.recycler_view);

        imgback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                onBackPressed();
            }
        });
        getAllVideos();

        txtStudent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getStudents();

            }
        });

        imgdropdownStudent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getStudents();

            }
        });

    }

    public void getStudents() {
        progressFragment = new ProgressFragment();
        progressFragment.show(getSupportFragmentManager(), "cjkk");

        Retrofit retrofitHelper = RetrofitHelper.getRetrofitInstance(VideosActivity.this);

        WebInterfaceimpl webInterfaceimpl = retrofitHelper.create(WebInterfaceimpl.class);

        Call<List<Students>> listCall = webInterfaceimpl.getAllStudents();

        listCall.enqueue(new Callback<List<Students>>() {
            @Override
            public void onResponse(Call<List<Students>> call, Response<List<Students>> response) {

                progressFragment.dismiss();
                if (response.body() != null) {

                    if (response.body().size() > 0) {


                        showStudentList(response.body());


                    } else {


                        Toast.makeText(VideosActivity.this, "No data found", Toast.LENGTH_SHORT).show();

                    }


                } else {


                    Toast.makeText(VideosActivity.this, "No data found", Toast.LENGTH_SHORT).show();

                }

            }

            @Override
            public void onFailure(Call<List<Students>> call, Throwable t) {
                progressFragment.dismiss();

                if (!NetConnection.isConnected(VideosActivity.this)) {
                    Toast.makeText(VideosActivity.this, "Check Internet connection", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    public void showStudentList(final List<Students> students) {
        List<String> strings = new ArrayList<>();
        AlertDialog.Builder builder = new AlertDialog.Builder(VideosActivity.this);
        builder.setTitle("Choose a student");
        ;

        for (Students students1 : students
        ) {

            strings.add(students1.getUdcStudentName());

        }


        builder.setItems(strings.toArray(new String[strings.size()]), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                dialog.dismiss();

                txtStudent.setText(students.get(which).getUdcStudentName());

                studentid = students.get(which).getUdcStudentId();





            }
        });

// create and show the alert dialog
        AlertDialog dialog = builder.create();
        dialog.show();
    }


    public void getAllVideos()
    {
        progressFragment = new ProgressFragment();
        progressFragment.show(getSupportFragmentManager(), "cjkk");

        Retrofit retrofitHelper = RetrofitHelper.getRetrofitInstance(VideosActivity.this);

        WebInterfaceimpl webInterfaceimpl = retrofitHelper.create(WebInterfaceimpl.class);

        Call<VideoStatus>listCall=webInterfaceimpl.getAllVideos(studentid);
        listCall.enqueue(new Callback<VideoStatus>() {
            @Override
            public void onResponse(Call<VideoStatus> call, Response<VideoStatus> response) {

                progressFragment.dismiss();
                if (response.body() != null) {

                    if (response.body().getData().size() > 0) {


                 recycler_view.setVisibility(View.VISIBLE);

                 recycler_view.setLayoutManager(new LinearLayoutManager(VideosActivity.this));
                 recycler_view.setAdapter(new VideosAdapter(VideosActivity.this,response.body().getData()));




                    } else {

                        recycler_view.setVisibility(View.GONE);
                        Toast.makeText(VideosActivity.this, "No data found", Toast.LENGTH_SHORT).show();

                    }


                } else {

                    recycler_view.setVisibility(View.GONE);
                    Toast.makeText(VideosActivity.this, "No data found", Toast.LENGTH_SHORT).show();

                }


            }

            @Override
            public void onFailure(Call<VideoStatus> call, Throwable t) {
                progressFragment.dismiss();

                if (!NetConnection.isConnected(VideosActivity.this)) {
                    Toast.makeText(VideosActivity.this, "Check Internet connection", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}
