package com.report.schoolteacher.webservice;


import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.report.schoolteacher.pojo.Attendance;
import com.report.schoolteacher.pojo.Circular;
import com.report.schoolteacher.pojo.CircularStatus;
import com.report.schoolteacher.pojo.Classes;
import com.report.schoolteacher.pojo.Complaints;
import com.report.schoolteacher.pojo.DivisionStatus;
import com.report.schoolteacher.pojo.Divisions;
import com.report.schoolteacher.pojo.Events;
import com.report.schoolteacher.pojo.GuardianStatus;
import com.report.schoolteacher.pojo.LessonPlan;
import com.report.schoolteacher.pojo.LoginToken;
import com.report.schoolteacher.pojo.Mark;
import com.report.schoolteacher.pojo.Marklist;
import com.report.schoolteacher.pojo.OnlineClass;
import com.report.schoolteacher.pojo.Profile;
import com.report.schoolteacher.pojo.StudentAbsent;
import com.report.schoolteacher.pojo.StudentByClass;
import com.report.schoolteacher.pojo.StudentByClassStatus;
import com.report.schoolteacher.pojo.StudentData;
import com.report.schoolteacher.pojo.StudentLeave;
import com.report.schoolteacher.pojo.StudentLeaveData;
import com.report.schoolteacher.pojo.StudentLeaveStatus;
import com.report.schoolteacher.pojo.Studentinfo;
import com.report.schoolteacher.pojo.Students;
import com.report.schoolteacher.pojo.Subjects;
import com.report.schoolteacher.pojo.TeacherLeave;
import com.report.schoolteacher.pojo.TeacherLeaveStatus;
import com.report.schoolteacher.pojo.TeacherMessageStatus;
import com.report.schoolteacher.pojo.TeacherMessages;
import com.report.schoolteacher.pojo.TimeTableStatus;
import com.report.schoolteacher.pojo.Timetable;
import com.report.schoolteacher.pojo.VideoStatus;
import com.report.schoolteacher.pojo.Videos;


import java.util.List;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;

public interface WebInterfaceimpl {

    @FormUrlEncoded
    @POST("Login.php")
    Call<LoginToken> getLoginData(@Field("user") String user, @Field("password") String password);
//
//
    @GET("teacher_profile.php")
    Call<List<Profile>>getTeacherProfile();


    @GET("teacher_get_class.php")
    Call<List<Classes>>getClasses();

    @GET("teacher_get_subject.php")
     Call<List<Subjects>>getSubjects();

    @FormUrlEncoded
    @POST("teacher_add_lessonplan.php")
    Call<JsonArray>addLessonPlan(@Field("class_id")String class_id,@Field("subject_id")String subject_id,@Field("implementation_date")String implementation_date,@Field("topic")String topic,@Field("decription")String decription);

    @GET("teacher_view_lessonpln.php")
    Call<List<LessonPlan>>getLessonPlans(@Query("class_id") String class_id,@Query("subject_id") String subject_id);

    @FormUrlEncoded
    @POST("lessonplan_complete.php")
    Call<JsonArray>updateLessonPlan(@Field("lessonplan_id") String lessonplan_id,@Field("completion_date")String completion_date,@Field("completion_status")String completion_status);


    @GET("teacher_get_student.php")
    Call<List<Students>>getAllStudents();


    @GET("teacher_get_student.php")
    Call<StudentData>getAllStudentslist(@Query("udc_class")String udc_class,@Query("udc_division")String udc_division);



    @GET("view_teacher_entertainment.php")
    Call<VideoStatus>getAllVideos(@Query("student_id")String student_id);

    @GET("view_timetable_teacher.php")
    Call<TimeTableStatus>getAllTimetable(@Query("class_id")String class_id,@Query("sdivision_id")String sdivision_id);

    @GET("teacher_view_stdleave.php")
    Call<List<StudentLeave>>getStudentLeave(@Query("student_id")String student_id);

    @GET("teacher_view_leave.php")
    Call<TeacherLeaveStatus>getTeacherLeave();

    @FormUrlEncoded
    @POST("teacher_leave_application.php")
    Call<JsonArray>addTeacherLeave(@Field("from_date")String from_date,@Field("to_date")String to_date,@Field("reason")String reason);


    @GET("view_teacher_circular.php")
    Call<CircularStatus>getCircular();

    @GET("view_event_teacher.php")
    Call<List<Events>>getEvents(@Query("month")String month, @Query("year")String year);

    @GET("teacher_view_complaint.php")
    Call<List<Complaints>>getComplaints();

    @GET("view_Online_class.php")
    Call<OnlineClass>getOnlineClass(@Query("class_id")String class_id,@Query("sdivision_id")String sdivision_id);

    @GET("teacher_get_class_division.php")
    Call<List<Divisions>>getDivisions(@Query("class_id")String class_id);

    @GET("teacher_get_class_division.php")
    Call<DivisionStatus>getDivisionData(@Query("class_id")String class_id);

    @FormUrlEncoded
    @POST("teacher_add_message.php")
    Call<JsonArray>addMessage(@Field("class_id")String class_id,@Field("division_id")String division_id,@Field("topic")String topic,@Field("decription")String decription);


    @GET("teacher_view_message.php")
    Call<TeacherMessageStatus>getTeacherMessages();

    @FormUrlEncoded
    @POST("teacher_delete_messgae.php")
    Call<JsonArray>deleteMessage(@Field("msg_id")String msg_id);

    @GET("view_leave_teacher.php")
    Call<List<Attendance>>getAttendance(@Query("month")String month,@Query("year")String year,@Query("student_id")String student_id);



    @GET("teacher_get_student_for_classdivision.php")
    Call<StudentByClassStatus>getStudentByClassDivision(@Query("class_id")String class_id, @Query("division_id")String division_id);


    @FormUrlEncoded
    @POST("teacher_add_marks")
    Call<JsonArray>addMarks(@Field("class_id")String class_id,@Field("sdivision_id")String sdivision_id,@Field("sterm")String sterm,
    @Field("exam_type")String exam_type,@Field("tmarks")String tmarks,@Field("testdate")String testdate,@Field("smarks")String smarks,
                            @Field("treview")String treview,@Field("student_id")String student_id,@Field("subject_id")String subject_id);




    @FormUrlEncoded
    @POST("teacher_view_marks.php")
    Call<Marklist>getMarks(@Field("class_id")String class_id, @Field("sdivision_id")String sdivision_id, @Field("exam_type")String exam_type, @Field("sterm")String sterm, @Field("subject_id")String subject_id);


    @FormUrlEncoded
    @POST("teacher_add_absence.php")
    Call<JsonArray>addAbsence(@Field("student_id")String student_id,@Field("date")String date,@Field("comments")String comments);



    @FormUrlEncoded
    @POST("teacher_delete_marks.php")
    Call<JsonArray>deleteMark(@Field("test_id")String testid,@Field("exam_type")String exam_type);


    @FormUrlEncoded
    @POST("teacher_edit_marks.php")
    Call<JsonArray>editMarks(@Field("class_id")String class_id,@Field("sdivision_id")String sdivision_id,
                             @Field("exam_type")String exam_type,@Field("sterm")String sterm,
                             @Field("tmarks")String tmarks,@Field("testdate")String testdate,
                             @Field("smarks")String smarks,@Field("treview")String treview,
                             @Field("subject_id")String subject_id,@Field("test_id")String test_id);

    @GET("view_leave_teacher.php")
    Call<List<StudentAbsent>>getStudentLeavebyTeacher(@Query("student_id")String student_id, @Query("month")String month, @Query("year")String year);


    @GET("teacher_view_attendance.php")
    Call<StudentLeaveStatus>getStudentLeave(@Query("student_id")String student_id, @Query("month")String month, @Query("year")String year);


    @GET("getFatherMobile.php")
    Call<GuardianStatus>getGuardian(@Query("student_id")String student_id);





}
