package com.report.schoolteacher.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.JsonArray;
import com.report.schoolteacher.R;
import com.report.schoolteacher.connectivity.NetConnection;
import com.report.schoolteacher.pojo.TeacherMessages;
import com.report.schoolteacher.progress.ProgressFragment;
import com.report.schoolteacher.webservice.RetrofitHelper;
import com.report.schoolteacher.webservice.WebInterfaceimpl;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class TeacherMessageAdapter extends RecyclerView.Adapter<TeacherMessageAdapter.TeacherMessageHolder> {


    Context context;
    List<TeacherMessages>teacherMessages;

    ProgressFragment progressFragment;

    public TeacherMessageAdapter(Context context, List<TeacherMessages> teacherMessages) {
        this.context = context;
        this.teacherMessages = teacherMessages;
    }

    public class TeacherMessageHolder extends RecyclerView.ViewHolder{

        TextView txtdetails;

        ImageView imgDelete;

        public TeacherMessageHolder(@NonNull View itemView) {
            super(itemView);

            txtdetails=itemView.findViewById(R.id.txtdetails);
            imgDelete=itemView.findViewById(R.id.imgDelete);

            imgDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    deleteTeacherMessage(getAdapterPosition());

                }
            });
        }
    }

    @NonNull
    @Override
    public TeacherMessageHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_teachermsgadapter,parent,false);


        return new TeacherMessageHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull TeacherMessageHolder holder, int position) {

        holder.txtdetails.setText("Message : "+teacherMessages.get(position).getUdcMessageDescription()+"\n"+
                "Topic : "+teacherMessages.get(position).getUdcMessageTopic()+"\n"+
                "Date : "+teacherMessages.get(position).getUdcMessageDate()+"\n"+
                "Class : "+teacherMessages.get(position).getUdcClassDetails()+" "+teacherMessages.get(position).getUdcDivisionName());

    }

    @Override
    public int getItemCount() {
        return teacherMessages.size();
    }

    public void deleteTeacherMessage(final int position)
    {

        progressFragment=new ProgressFragment();
        progressFragment.show(((AppCompatActivity)context).getSupportFragmentManager(),"dsifdsj");

        Retrofit retrofitHelper = RetrofitHelper.getRetrofitInstance(context);

        WebInterfaceimpl webInterfaceimpl = retrofitHelper.create(WebInterfaceimpl.class);

        Call<JsonArray>jsonArrayCall=webInterfaceimpl.deleteMessage(teacherMessages.get(position).getUdcMessageId());
        jsonArrayCall.enqueue(new Callback<JsonArray>() {
            @Override
            public void onResponse(Call<JsonArray> call, Response<JsonArray> response) {
                progressFragment.dismiss();
                if(response.body()!=null)
                {

                    try{

                        JSONArray jsonArray=new JSONArray(response.body().toString());
                        if(jsonArray.length()>0)
                        {
                            JSONObject jsonObject=jsonArray.getJSONObject(0);


                            if(jsonObject.getInt("status")==1)
                            {


                                Toast.makeText(context,jsonObject.getString("message"),Toast.LENGTH_SHORT).show();


                                teacherMessages.remove(position);
                                notifyItemRemoved(position);

                            }



                        }



                    }catch (Exception e)
                    {

                    }


                }
                else {



                    Toast.makeText(context,"failed to delete message",Toast.LENGTH_SHORT).show();

                }
            }

            @Override
            public void onFailure(Call<JsonArray> call, Throwable t) {
                progressFragment.dismiss();
                if(!NetConnection.isConnected(context))
                {
                    Toast.makeText(context,"Check Internet connection",Toast.LENGTH_SHORT).show();
                }
            }
        });





    }
}
