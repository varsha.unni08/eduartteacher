package com.report.schoolteacher.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Videos implements Serializable {


    @SerializedName("cd_entertainment")
    @Expose
    private String cdEntertainment;
    @SerializedName("cd_parent_id")
    @Expose
    private String cdParentId;
    @SerializedName("cd_student_id")
    @Expose
    private String cdStudentId;
    @SerializedName("ds_entertainment_name")
    @Expose
    private String dsEntertainmentName;
    @SerializedName("ds_entertainment_description")
    @Expose
    private String dsEntertainmentDescription;
    @SerializedName("ds_image")
    @Expose
    private String dsImage;
    @SerializedName("ds_video")
    @Expose
    private String dsVideo;
    @SerializedName("dt_deletion")
    @Expose
    private Object dtDeletion;
    @SerializedName("dt_record")
    @Expose
    private String dtRecord;

    public Videos() {
    }

    public String getCdEntertainment() {
        return cdEntertainment;
    }

    public void setCdEntertainment(String cdEntertainment) {
        this.cdEntertainment = cdEntertainment;
    }

    public String getCdParentId() {
        return cdParentId;
    }

    public void setCdParentId(String cdParentId) {
        this.cdParentId = cdParentId;
    }

    public String getCdStudentId() {
        return cdStudentId;
    }

    public void setCdStudentId(String cdStudentId) {
        this.cdStudentId = cdStudentId;
    }

    public String getDsEntertainmentName() {
        return dsEntertainmentName;
    }

    public void setDsEntertainmentName(String dsEntertainmentName) {
        this.dsEntertainmentName = dsEntertainmentName;
    }

    public String getDsEntertainmentDescription() {
        return dsEntertainmentDescription;
    }

    public void setDsEntertainmentDescription(String dsEntertainmentDescription) {
        this.dsEntertainmentDescription = dsEntertainmentDescription;
    }

    public String getDsImage() {
        return dsImage;
    }

    public void setDsImage(String dsImage) {
        this.dsImage = dsImage;
    }

    public String getDsVideo() {
        return dsVideo;
    }

    public void setDsVideo(String dsVideo) {
        this.dsVideo = dsVideo;
    }

    public Object getDtDeletion() {
        return dtDeletion;
    }

    public void setDtDeletion(Object dtDeletion) {
        this.dtDeletion = dtDeletion;
    }

    public String getDtRecord() {
        return dtRecord;
    }

    public void setDtRecord(String dtRecord) {
        this.dtRecord = dtRecord;
    }
}
