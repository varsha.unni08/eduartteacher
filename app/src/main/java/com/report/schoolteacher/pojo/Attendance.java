package com.report.schoolteacher.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Attendance {

    @SerializedName("udc_from_date")
    @Expose
    private String udcFromDate;
    @SerializedName("udc_to_date")
    @Expose
    private String udcToDate;
    @SerializedName("udc_reason")
    @Expose
    private String udcReason;
    @SerializedName("udc_leave_id")
    @Expose
    private String udcLeaveId;

    int index;
    String data="";

    boolean isleave=false;

    String date="";

    public Attendance() {
    }


    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public boolean isIsleave() {
        return isleave;
    }

    public void setIsleave(boolean isleave) {
        this.isleave = isleave;
    }

    public String getUdcFromDate() {
        return udcFromDate;
    }

    public void setUdcFromDate(String udcFromDate) {
        this.udcFromDate = udcFromDate;
    }

    public String getUdcToDate() {
        return udcToDate;
    }

    public void setUdcToDate(String udcToDate) {
        this.udcToDate = udcToDate;
    }

    public String getUdcReason() {
        return udcReason;
    }

    public void setUdcReason(String udcReason) {
        this.udcReason = udcReason;
    }

    public String getUdcLeaveId() {
        return udcLeaveId;
    }

    public void setUdcLeaveId(String udcLeaveId) {
        this.udcLeaveId = udcLeaveId;
    }
}
