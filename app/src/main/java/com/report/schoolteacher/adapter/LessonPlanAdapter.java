package com.report.schoolteacher.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.JsonArray;
import com.report.schoolteacher.R;
import com.report.schoolteacher.activities.AddLessonPlanActivity;
import com.report.schoolteacher.activities.LessonPlanActivity;
import com.report.schoolteacher.connectivity.NetConnection;
import com.report.schoolteacher.pojo.LessonPlan;
import com.report.schoolteacher.progress.ProgressFragment;
import com.report.schoolteacher.webservice.RetrofitHelper;
import com.report.schoolteacher.webservice.WebInterfaceimpl;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class LessonPlanAdapter extends RecyclerView.Adapter<LessonPlanAdapter.LessonPlanholder> {


    Context context;
    List<LessonPlan> lessonPlans;

    ProgressFragment progressFragment;

    public LessonPlanAdapter(Context context, List<LessonPlan> lessonPlans) {
        this.context = context;
        this.lessonPlans = lessonPlans;
    }

    public class LessonPlanholder extends RecyclerView.ViewHolder {

        TextView txtdata;
        AppCompatImageView imgTick;

        Button btnChangeStatus;

        public LessonPlanholder(@NonNull View itemView) {
            super(itemView);
            txtdata = itemView.findViewById(R.id.txtdata);
            imgTick = itemView.findViewById(R.id.imgTick);
            btnChangeStatus = itemView.findViewById(R.id.btnChangeStatus);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (lessonPlans.get(getAdapterPosition()).getUdcLessonplanCompletionStatus().equals("0")) {

                        if (lessonPlans.get(getAdapterPosition()).getSelected() == 0) {

                            for (LessonPlan lessonPlan : lessonPlans) {

                                lessonPlan.setSelected(0);

                            }

                            lessonPlans.get(getAdapterPosition()).setSelected(1);
                            notifyItemRangeChanged(0, lessonPlans.size());

                        }
                        else {
                            lessonPlans.get(getAdapterPosition()).setSelected(0);
                            notifyItemRangeChanged(0, lessonPlans.size());
                        }
                    }

                }
            });

            btnChangeStatus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    LessonPlan lessonPlan=lessonPlans.get(getAdapterPosition());

                    final Calendar cldr = Calendar.getInstance();
                    final int day = cldr.get(Calendar.DAY_OF_MONTH);
                    final int month = cldr.get(Calendar.MONTH);
                    final int year = cldr.get(Calendar.YEAR);



                    int m1=month+1;

                    String currentdate=year+"-"+m1+"-"+day;

                    progressFragment = new ProgressFragment();
                    progressFragment.show(((AppCompatActivity)context).getSupportFragmentManager(), "cjkk");

                    Retrofit retrofitHelper = RetrofitHelper.getRetrofitInstance(context);

                    WebInterfaceimpl webInterfaceimpl = retrofitHelper.create(WebInterfaceimpl.class);

                    Call<JsonArray>jsonArrayCall=webInterfaceimpl.updateLessonPlan(lessonPlan.getUdcLessonplanId(),currentdate,"1");
                    jsonArrayCall.enqueue(new Callback<JsonArray>() {
                        @Override
                        public void onResponse(Call<JsonArray> call, Response<JsonArray> response) {
                            progressFragment.dismiss();
                            if(response.body()!=null)
                            {

                                if(response.body().size()>0)
                                {


                                    try{


                                        JSONArray jsonArray=new JSONArray(response.body().toString());


                                        JSONObject jsonObject=jsonArray.getJSONObject(0);
                                        if(jsonObject.getInt("status")==1)
                                        {

                                            int posi=getAdapterPosition();
                                            Toast.makeText(context,jsonObject.getString("message"),Toast.LENGTH_SHORT).show();
                                           lessonPlans.get(posi).setUdcLessonplanCompletionStatus("1");
                                           lessonPlans.get(posi).setSelected(0);
                                           notifyItemChanged(posi);

                                        }
                                        else {

                                            Toast.makeText(context,"failed",Toast.LENGTH_SHORT).show();

                                        }





                                    }catch (Exception e)
                                    {

                                    }



                                }
                                else {

                                    Toast.makeText(context,"failed",Toast.LENGTH_SHORT).show();

                                }


                            }
                            else {

                                Toast.makeText(context,"failed",Toast.LENGTH_SHORT).show();

                            }
                        }

                        @Override
                        public void onFailure(Call<JsonArray> call, Throwable t) {
                            progressFragment.dismiss();

                            if(!NetConnection.isConnected(context))
                            {
                                Toast.makeText(context,"Check Internet connection",Toast.LENGTH_SHORT).show();
                            }
                        }
                    });


                }
            });
        }
    }

    @NonNull
    @Override
    public LessonPlanholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());

        View view = layoutInflater.inflate(R.layout.layout_lessonplanadapter, parent, false);

        return new LessonPlanholder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull LessonPlanholder holder, int position) {
        holder.txtdata.setText(lessonPlans.get(position).getUdcSubjectDetails() + "\n" + lessonPlans.get(position).getUdcLessonplanTopic() + "\n" + lessonPlans.get(position).getUdcExpectedImpDate()+"\nClass : "+lessonPlans.get(position).getUdcClassDetails());

        if (lessonPlans.get(position).getUdcLessonplanCompletionStatus().equals("1")) {
            holder.imgTick.setImageResource(R.drawable.ic_check_circle_green);

        } else {

            //holder.imgTick.setImageResource(R.drawable.ic_warning);

        }
        if (lessonPlans.get(position).getUdcLessonplanCompletionStatus().equals("0")) {
        if (lessonPlans.get(position).getSelected() == 1) {

                holder.btnChangeStatus.setVisibility(View.VISIBLE);

            } else {

                holder.btnChangeStatus.setVisibility(View.GONE);
            }


        }
        else {
            holder.btnChangeStatus.setVisibility(View.GONE);
        }


    }

    @Override
    public int getItemCount() {
        return lessonPlans.size();
    }
}
