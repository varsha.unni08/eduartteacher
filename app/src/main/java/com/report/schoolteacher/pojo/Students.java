package com.report.schoolteacher.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Students implements Serializable {

    @SerializedName("udc_student_id")
    @Expose
    private String udcStudentId;
    @SerializedName("udc_student_name")
    @Expose
    private String udcStudentName;
    @SerializedName("ds_image")
    @Expose
    private String ds_image;

    int absencemarked=0;

    public Students() {
    }


    public int getAbsencemarked() {
        return absencemarked;
    }

    public void setAbsencemarked(int absencemarked) {
        this.absencemarked = absencemarked;
    }

    public String getDs_image() {
        return ds_image;
    }

    public void setDs_image(String ds_image) {
        this.ds_image = ds_image;
    }

    public String getUdcStudentId() {
        return udcStudentId;
    }

    public void setUdcStudentId(String udcStudentId) {
        this.udcStudentId = udcStudentId;
    }

    public String getUdcStudentName() {
        return udcStudentName;
    }

    public void setUdcStudentName(String udcStudentName) {
        this.udcStudentName = udcStudentName;
    }
}
