package com.report.schoolteacher.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Divisions {

    @SerializedName("udc_class")
    @Expose
    private String udcClass;
    @SerializedName("udc_division")
    @Expose
    private String udcDivision;
    @SerializedName("udc_division_name")
    @Expose
    private String udcDivisionName;

    public Divisions() {
    }

    public String getUdcClass() {
        return udcClass;
    }

    public void setUdcClass(String udcClass) {
        this.udcClass = udcClass;
    }

    public String getUdcDivision() {
        return udcDivision;
    }

    public void setUdcDivision(String udcDivision) {
        this.udcDivision = udcDivision;
    }

    public String getUdcDivisionName() {
        return udcDivisionName;
    }

    public void setUdcDivisionName(String udcDivisionName) {
        this.udcDivisionName = udcDivisionName;
    }
}
