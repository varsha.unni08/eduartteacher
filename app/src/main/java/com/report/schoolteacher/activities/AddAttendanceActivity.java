package com.report.schoolteacher.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.report.schoolteacher.AppLiterals.Constants;
import com.report.schoolteacher.R;
import com.report.schoolteacher.adapter.StudentAbsentAdapter;
import com.report.schoolteacher.adapter.StudentListAdapter;
import com.report.schoolteacher.connectivity.NetConnection;
import com.report.schoolteacher.pojo.StudentData;
import com.report.schoolteacher.pojo.Studentinfo;
import com.report.schoolteacher.pojo.Students;
import com.report.schoolteacher.progress.ProgressFragment;
import com.report.schoolteacher.webservice.RetrofitHelper;
import com.report.schoolteacher.webservice.WebInterfaceimpl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class AddAttendanceActivity extends AppCompatActivity {

    ImageView imgback,imgpickFromDate;

    RecyclerView recycler_view;

    EditText edtSearch;

    List<Studentinfo>students;

    TextView txtNodata,txtStartdate;

    ProgressFragment progressFragment;

   public static String end_date="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_attendance);
        getSupportActionBar().hide();
        imgback=findViewById(R.id.imgback);
        students=new ArrayList<>();
        edtSearch=findViewById(R.id.edtSearch);
        recycler_view=findViewById(R.id.recycler_view);
        txtNodata=findViewById(R.id.txtNodata);
        imgback=findViewById(R.id.imgback);

        imgpickFromDate=findViewById(R.id.imgpickFromDate);
        txtStartdate=findViewById(R.id.txtStartdate);

        final Calendar cldr = Calendar.getInstance();
        final int day = cldr.get(Calendar.DAY_OF_MONTH);
        final int month = cldr.get(Calendar.MONTH);
        final int year = cldr.get(Calendar.YEAR);

        int m=month+1;

        String date=day+"-"+m+"-"+year;

        txtStartdate.setText(date);

        end_date=date;




        imgpickFromDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                showDatePicker();

            }
        });

        txtStartdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                showDatePicker();

            }
        });

        imgback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                onBackPressed();
            }
        });

        getStudents();

        edtSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                if(!charSequence.toString().equals(""))
                {

                    if(students.size()>0)
                    {

                        List<Studentinfo>student_search=new ArrayList<>();

                        for (Studentinfo s1:students
                        ) {

                            if(s1.getUdcStudentName().toLowerCase().contains(charSequence.toString())||s1.getUdcStudentName().toUpperCase().contains(charSequence.toString()))
                            {
                                student_search.add(s1);
                            }

                        }

                        recycler_view.setLayoutManager(new LinearLayoutManager(AddAttendanceActivity.this));
                        recycler_view.setAdapter(new StudentAbsentAdapter(AddAttendanceActivity.this,student_search));




                    }
                    else {

                        Toast.makeText(AddAttendanceActivity.this,"No data to search",Toast.LENGTH_SHORT).show();
                    }




                }
                else {

                    recycler_view.setLayoutManager(new LinearLayoutManager(AddAttendanceActivity.this));
                    recycler_view.setAdapter(new StudentAbsentAdapter(AddAttendanceActivity.this,students));

                }

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });


    }


    public void showDatePicker() {
        final Calendar cldr = Calendar.getInstance();
        final int day = cldr.get(Calendar.DAY_OF_MONTH);
        final int month = cldr.get(Calendar.MONTH);
        final int year = cldr.get(Calendar.YEAR);

        DatePickerDialog datePickerDialog = new DatePickerDialog(AddAttendanceActivity.this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {



                int m = i1 + 1;




                end_date = i2 + "-" + m + "-" + i;
                txtStartdate.setText(end_date);





            }
        }, year, month, day);

        datePickerDialog.show();
    }


    public void getStudents() {
        progressFragment = new ProgressFragment();
        progressFragment.show(getSupportFragmentManager(), "cjkk");

        Retrofit retrofitHelper = RetrofitHelper.getRetrofitInstance(AddAttendanceActivity.this);

        WebInterfaceimpl webInterfaceimpl = retrofitHelper.create(WebInterfaceimpl.class);

        Call<StudentData> listCall = webInterfaceimpl.getAllStudentslist(Constants.classid,Constants.divisionid);



        listCall.enqueue(new Callback<StudentData>() {
            @Override
            public void onResponse(Call<StudentData> call, Response<StudentData> response) {

                progressFragment.dismiss();
                if (response.body() != null) {



                    if (response.body().getData().size() > 0) {

                        recycler_view.setVisibility(View.VISIBLE);
                        txtNodata.setVisibility(View.GONE);

                        students.addAll(response.body().getData());


                        recycler_view.setLayoutManager(new LinearLayoutManager(AddAttendanceActivity.this));
                        recycler_view.setAdapter(new StudentAbsentAdapter(AddAttendanceActivity.this,response.body().getData()));



                    } else {


                        recycler_view.setVisibility(View.GONE);
                        txtNodata.setVisibility(View.VISIBLE);

                        Toast.makeText(AddAttendanceActivity.this, "No data found", Toast.LENGTH_SHORT).show();

                    }


                } else {

                    recycler_view.setVisibility(View.GONE);
                    txtNodata.setVisibility(View.VISIBLE);
                    Toast.makeText(AddAttendanceActivity.this, "No data found", Toast.LENGTH_SHORT).show();

                }

            }

            @Override
            public void onFailure(Call<StudentData> call, Throwable t) {
                progressFragment.dismiss();

                if (!NetConnection.isConnected(AddAttendanceActivity.this)) {
                    Toast.makeText(AddAttendanceActivity.this, "Check Internet connection", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}
