package com.report.schoolteacher.adapter;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.report.schoolteacher.fragments.StudentleaveFragment;
import com.report.schoolteacher.fragments.TeacherleaveFragment;

public class LeaveTabpagerAdapter extends FragmentPagerAdapter {


    String arr[]={"Student's leave","My leave"};

    public LeaveTabpagerAdapter(@NonNull FragmentManager fm) {
        super(fm);
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        Fragment fragment=null;

        if(position==0)
        {
            fragment=new StudentleaveFragment();
        }
        else {

            fragment=new TeacherleaveFragment();
        }



        return fragment;
    }

    @Override
    public int getCount() {
        return 2;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return arr[position];
    }
}
