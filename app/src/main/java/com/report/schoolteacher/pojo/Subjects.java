package com.report.schoolteacher.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Subjects {

    @SerializedName("subject_id")
    @Expose
    private String subjectId;
    @SerializedName("udc_subject_details")
    @Expose
    private String udcSubjectDetails;

    public Subjects() {
    }

    public String getSubjectId() {
        return subjectId;
    }

    public void setSubjectId(String subjectId) {
        this.subjectId = subjectId;
    }

    public String getUdcSubjectDetails() {
        return udcSubjectDetails;
    }

    public void setUdcSubjectDetails(String udcSubjectDetails) {
        this.udcSubjectDetails = udcSubjectDetails;
    }
}
