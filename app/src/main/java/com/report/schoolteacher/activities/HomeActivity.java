package com.report.schoolteacher.activities;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.LayoutAnimationController;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.report.schoolteacher.AppLiterals.Constants;
import com.report.schoolteacher.R;
import com.report.schoolteacher.adapter.HomeAdapter;
import com.report.schoolteacher.adapter.NewHomeAdapter;
import com.report.schoolteacher.connectivity.NetConnection;
import com.report.schoolteacher.pojo.Classes;
import com.report.schoolteacher.pojo.DivisionStatus;
import com.report.schoolteacher.pojo.Divisions;
import com.report.schoolteacher.preferencehelper.PreferenceHelper;
import com.report.schoolteacher.progress.ProgressFragment;
import com.report.schoolteacher.webservice.RetrofitHelper;
import com.report.schoolteacher.webservice.WebInterfaceimpl;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class HomeActivity extends AppCompatActivity {

    RecyclerView recycler_view,recycler_vi;

    ImageView imgprofile,imglogout,imgdropdownClass,imgdropdowndivision;

    TextView txtClass,txtdivision;

    ProgressFragment progressFragment;
    String classid="",divisionid="";



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        getSupportActionBar().hide();
        recycler_view=findViewById(R.id.recycler_view);
        imgprofile=findViewById(R.id.imgprofile);
        imglogout=findViewById(R.id.imglogout);
        recycler_vi=findViewById(R.id.recycler_vi);
        imgdropdowndivision=findViewById(R.id.imgdropdowndivision);
        imgdropdownClass=findViewById(R.id.imgdropdownClass);

        txtClass=findViewById(R.id.txtClass);
        txtdivision=findViewById(R.id.txtdivision);

        imglogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                AlertDialog.Builder builder=new AlertDialog.Builder(HomeActivity.this);
                builder.setTitle(R.string.app_name);
                builder.setMessage("Do you want to logout from "+getString(R.string.app_name)+" ?");
                builder.setPositiveButton("yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                        startActivity(new Intent(HomeActivity.this,LoginActivity.class));

                        new PreferenceHelper(HomeActivity.this).clearData();


                        finish();
                    }
                });

                builder.setNegativeButton("no", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        dialogInterface.dismiss();

                    }
                });
                builder.show();
            }
        });

        imgprofile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                startActivity(new Intent(HomeActivity.this,TeacherProfileActivity.class));
            }
        });


        imgdropdownClass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                getClasses();

            }
        });

        imgdropdowndivision.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                getDivisions();
            }
        });


        txtClass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                getClasses();

            }
        });

        txtdivision.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                getDivisions();
            }
        });



        setupHomeAdapter();
    }

    public void showDivisionList(final List<Divisions> divisions)
    {
        List<String>strings=new ArrayList<>();
        AlertDialog.Builder builder = new AlertDialog.Builder(HomeActivity.this);
        builder.setTitle("Choose a division");
        ;

        for (Divisions divi:divisions
        ) {

            strings.add(divi.getUdcDivisionName());

        }



        builder.setItems(strings.toArray(new String[strings.size()]), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                dialog.dismiss();

                txtdivision.setText(divisions.get(which).getUdcDivisionName());

                divisionid=divisions.get(which).getUdcDivision();


                Constants.divisionid=divisionid;
                Constants.divisionname=divisions.get(which).getUdcDivisionName();

                // showLessonPlanlist();

            }
        });

// create and show the alert dialog
        AlertDialog dialog = builder.create();
        dialog.show();
    }


    public void getClasses()
    {

        progressFragment=new ProgressFragment();
        progressFragment.show(getSupportFragmentManager(),"djaisjfk");

        Retrofit retrofitHelper = RetrofitHelper.getRetrofitInstance(HomeActivity.this);

        WebInterfaceimpl webInterfaceimpl = retrofitHelper.create(WebInterfaceimpl.class);

        Call<List<Classes>> listCall=webInterfaceimpl.getClasses();

        listCall.enqueue(new Callback<List<Classes>>() {
            @Override
            public void onResponse(Call<List<Classes>> call, Response<List<Classes>> response) {

                progressFragment.dismiss();

                if(response.body()!=null)
                {

                    if(response.body().size()>0)
                    {



                        showClassList(response.body());



                    }
                    else {

                        Toast.makeText(HomeActivity.this,"No data found",Toast.LENGTH_SHORT).show();

                    }


                }
                else {

                    Toast.makeText(HomeActivity.this,"No data found",Toast.LENGTH_SHORT).show();

                }

            }

            @Override
            public void onFailure(Call<List<Classes>> call, Throwable t) {

                progressFragment.dismiss();

                if(!NetConnection.isConnected(HomeActivity.this))
                {
                    Toast.makeText(HomeActivity.this,"Check Internet connection",Toast.LENGTH_SHORT).show();
                }

            }
        });

    }



    public void showClassList(final List<Classes>classes)
    {

        List<String>strings=new ArrayList<>();
        AlertDialog.Builder builder = new AlertDialog.Builder(HomeActivity.this);
        builder.setTitle("Choose a class");
        ;

        for (Classes student:classes
        ) {

            strings.add(student.getUdcClassDetails());

        }



        builder.setItems(strings.toArray(new String[strings.size()]), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                dialog.dismiss();

                txtClass.setText(classes.get(which).getUdcClassDetails());

                classid=classes.get(which).getClassId();

                Constants.classid=classid;
                Constants.classname=classes.get(which).getUdcClassDetails();

                // showLessonPlanlist();

            }
        });

// create and show the alert dialog
        AlertDialog dialog = builder.create();
        dialog.show();
    }


    public void getDivisions()
    {

        progressFragment=new ProgressFragment();
        progressFragment.show(getSupportFragmentManager(),"dgl");

        Retrofit retrofitHelper = RetrofitHelper.getRetrofitInstance(HomeActivity.this);

        WebInterfaceimpl webInterfaceimpl = retrofitHelper.create(WebInterfaceimpl.class);

        Call<DivisionStatus> listCall=webInterfaceimpl.getDivisionData(classid);

        listCall.enqueue(new Callback<DivisionStatus>() {
            @Override
            public void onResponse(Call<DivisionStatus> call, Response<DivisionStatus> response) {
                progressFragment.dismiss();

                if(response.body()!=null)
                {

                    if(response.body().getData().size()>0)
                    {


                        showDivisionList(response.body().getData());



                    }
                    else {

                        Toast.makeText(HomeActivity.this,"No data found",Toast.LENGTH_SHORT).show();

                    }


                }
                else {

                    Toast.makeText(HomeActivity.this,"No data found",Toast.LENGTH_SHORT).show();

                }


            }

            @Override
            public void onFailure(Call<DivisionStatus> call, Throwable t) {
                progressFragment.dismiss();

                if(!NetConnection.isConnected(HomeActivity.this))
                {
                    Toast.makeText(HomeActivity.this,"Check Internet connection",Toast.LENGTH_SHORT).show();
                }
            }
        });



    }

    void setupHomeAdapter() {

        AnimationSet set = new AnimationSet(true);




        Animation animation = new AlphaAnimation(0.0f, 1.0f);
        animation.setDuration(500);
        set.addAnimation(animation);

        animation = new TranslateAnimation(
                Animation.RELATIVE_TO_SELF, 0.0f, Animation.RELATIVE_TO_SELF, 0.0f,
                Animation.RELATIVE_TO_SELF, -1.0f, Animation.RELATIVE_TO_SELF, 0.0f
        );
        animation.setDuration(100);
        set.addAnimation(animation);

        LayoutAnimationController controller = new LayoutAnimationController(set, 0.5f);

        recycler_view.setLayoutManager(new GridLayoutManager(HomeActivity.this, 2));
        recycler_view.setLayoutAnimation(controller);
        recycler_view.setAdapter(new HomeAdapter(HomeActivity.this));



        recycler_vi.setLayoutManager(new GridLayoutManager(HomeActivity.this, 3));
        recycler_vi.setLayoutAnimation(controller);
        recycler_vi.setAdapter(new NewHomeAdapter(HomeActivity.this));
    }

}
