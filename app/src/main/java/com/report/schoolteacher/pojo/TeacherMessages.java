package com.report.schoolteacher.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TeacherMessages {

    @SerializedName("udc_message_id")
    @Expose
    private String udcMessageId;
    @SerializedName("udc_accademic_year_id")
    @Expose
    private String udcAccademicYearId;
    @SerializedName("udc_teacher_id")
    @Expose
    private String udcTeacherId;
    @SerializedName("udc_class")
    @Expose
    private String udcClass;
    @SerializedName("udc_division")
    @Expose
    private String udcDivision;
    @SerializedName("udc_message_date")
    @Expose
    private String udcMessageDate;
    @SerializedName("udc_message_topic")
    @Expose
    private String udcMessageTopic;
    @SerializedName("udc_message_description")
    @Expose
    private String udcMessageDescription;
    @SerializedName("delete_status")
    @Expose
    private String deleteStatus;
    @SerializedName("udc_class_details")
    @Expose
    private String udcClassDetails;
    @SerializedName("udc_division_name")
    @Expose
    private String udcDivisionName;

    public TeacherMessages() {
    }

    public String getUdcClassDetails() {
        return udcClassDetails;
    }

    public void setUdcClassDetails(String udcClassDetails) {
        this.udcClassDetails = udcClassDetails;
    }

    public String getUdcDivisionName() {
        return udcDivisionName;
    }

    public void setUdcDivisionName(String udcDivisionName) {
        this.udcDivisionName = udcDivisionName;
    }

    public String getUdcMessageId() {
        return udcMessageId;
    }

    public void setUdcMessageId(String udcMessageId) {
        this.udcMessageId = udcMessageId;
    }

    public String getUdcAccademicYearId() {
        return udcAccademicYearId;
    }

    public void setUdcAccademicYearId(String udcAccademicYearId) {
        this.udcAccademicYearId = udcAccademicYearId;
    }

    public String getUdcTeacherId() {
        return udcTeacherId;
    }

    public void setUdcTeacherId(String udcTeacherId) {
        this.udcTeacherId = udcTeacherId;
    }

    public String getUdcClass() {
        return udcClass;
    }

    public void setUdcClass(String udcClass) {
        this.udcClass = udcClass;
    }

    public String getUdcDivision() {
        return udcDivision;
    }

    public void setUdcDivision(String udcDivision) {
        this.udcDivision = udcDivision;
    }

    public String getUdcMessageDate() {
        return udcMessageDate;
    }

    public void setUdcMessageDate(String udcMessageDate) {
        this.udcMessageDate = udcMessageDate;
    }

    public String getUdcMessageTopic() {
        return udcMessageTopic;
    }

    public void setUdcMessageTopic(String udcMessageTopic) {
        this.udcMessageTopic = udcMessageTopic;
    }

    public String getUdcMessageDescription() {
        return udcMessageDescription;
    }

    public void setUdcMessageDescription(String udcMessageDescription) {
        this.udcMessageDescription = udcMessageDescription;
    }

    public String getDeleteStatus() {
        return deleteStatus;
    }

    public void setDeleteStatus(String deleteStatus) {
        this.deleteStatus = deleteStatus;
    }
}
