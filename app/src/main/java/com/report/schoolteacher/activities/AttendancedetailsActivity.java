package com.report.schoolteacher.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Dialog;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonArray;
import com.report.schoolteacher.R;
import com.report.schoolteacher.adapter.AttendanceAdapter;
import com.report.schoolteacher.adapter.StudentListAdapter;
import com.report.schoolteacher.connectivity.NetConnection;
import com.report.schoolteacher.pojo.Attendance;
import com.report.schoolteacher.pojo.StudentAbsent;
import com.report.schoolteacher.pojo.StudentLeaveData;
import com.report.schoolteacher.pojo.StudentLeaveStatus;
import com.report.schoolteacher.pojo.Studentinfo;
import com.report.schoolteacher.pojo.Students;
import com.report.schoolteacher.progress.ProgressFragment;
import com.report.schoolteacher.webservice.RetrofitHelper;
import com.report.schoolteacher.webservice.WebInterfaceimpl;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class AttendancedetailsActivity extends AppCompatActivity {

    Studentinfo students;

    ImageView imgback, imgdate;
    TextView txtdate;

    boolean isFirstday = false;

    RecyclerView recyclerView;

    LinearLayout layout_events;

    ProgressFragment progressFragment;
    Integer month,year;

    String expMonth="",expYear="";

    TextView txtTotalLeaves;

    int totalleaves=0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_attendancedetails);
        getSupportActionBar().hide();

        students=(Studentinfo) getIntent().getSerializableExtra("student");

        imgback = findViewById(R.id.imgback);
        imgdate = findViewById(R.id.imgdate);
        txtdate = findViewById(R.id.txtdate);
        recyclerView = findViewById(R.id.recyclerView);
        layout_events=findViewById(R.id.layout_events);

        txtTotalLeaves=findViewById(R.id.txtTotalLeaves);

        imgdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                ExpiryDialog();

            }
        });

        txtdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ExpiryDialog();

            }
        });

        imgback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                onBackPressed();
            }
        });
    }


    private void ExpiryDialog() {

        final Dialog dialog = new Dialog(AttendancedetailsActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_date_picker);
        dialog.show();

        Calendar mCalendar = Calendar.getInstance();

        final DatePicker datePicker = (DatePicker) dialog.findViewById(R.id.date_picker);
        Button date_time_set = (Button) dialog.findViewById(R.id.date_time_set);
        datePicker.init(mCalendar.get(Calendar.YEAR), mCalendar.get(Calendar.MONTH), mCalendar.get(Calendar.DAY_OF_MONTH), null);

        LinearLayout ll = (LinearLayout) datePicker.getChildAt(0);
        LinearLayout ll2 = (LinearLayout) ll.getChildAt(0);
        ll2.getChildAt(1).setVisibility(View.INVISIBLE);

        date_time_set.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                isFirstday = false;

                try {
                    month = datePicker.getMonth() + 1;
                    year = datePicker.getYear();

                     expMonth = (month.toString().length() == 1 ? "0" + month.toString() : month.toString());
                     expYear = year.toString();
                    txtdate.setText(expMonth + "/" + expYear);

                    getAttendance();





                } catch (Exception e) {

                }

            }
        });
    }

    public void getAttendance()
    {
        progressFragment = new ProgressFragment();
        progressFragment.show(getSupportFragmentManager(), "cjkk");

        Retrofit retrofitHelper = RetrofitHelper.getRetrofitInstance(AttendancedetailsActivity.this);

        WebInterfaceimpl webInterfaceimpl = retrofitHelper.create(WebInterfaceimpl.class);

        Call<StudentLeaveStatus>jsonArrayCall=webInterfaceimpl.getStudentLeave(students.getUdcStudentId(),expMonth,expYear);
        jsonArrayCall.enqueue(new Callback<StudentLeaveStatus>() {
            @Override
            public void onResponse(Call<StudentLeaveStatus> call, Response<StudentLeaveStatus> response) {

                progressFragment.dismiss();
                List<StudentAbsent>attendanceList=new ArrayList<>();
                if (response.body() != null) {



                    if (response.body().getData().size()>0) {



                        for(StudentLeaveData adt:response.body().getData()) {

                            String fromdate = adt.getUdcAbsenceDate();
                            SimpleDateFormat inFormat = new SimpleDateFormat("yyyy-MM-dd");
                            try {
                                Date fdate = inFormat.parse(fromdate);
                                String startdate1 = inFormat.format(fdate);
                                StudentAbsent ate1 = new StudentAbsent();
                                ate1.setUdcAbsenceDate(startdate1);
                                ate1.setUdcComments("");
                                attendanceList.add(ate1);


                            } catch (ParseException e) {
                                e.printStackTrace();
                                Log.e("error", e.toString());
                            }

                        }
                        txtTotalLeaves.setText("Total No. of leaves : "+attendanceList.size());
                        setCalendarForAttendance(attendanceList);



//                        Toast.makeText(AttendancedetailsActivity.this, response.body().toString(), Toast.LENGTH_SHORT).show();



                    } else {

                        txtTotalLeaves.setText("Total No. of leaves : 0");

                        setCalendar();

//                        recyclerView.setVisibility(View.GONE);
//                        layout_events.setVisibility(View.GONE);
//                        txtTotalLeaves.setVisibility(View.GONE);
//                        Toast.makeText(AttendancedetailsActivity.this, "No data found", Toast.LENGTH_SHORT).show();

                    }




                } else {

                    txtTotalLeaves.setText("Total No. of leaves : 0");

                    setCalendar();

//                    recyclerView.setVisibility(View.GONE);
//                    layout_events.setVisibility(View.GONE);
//                    txtTotalLeaves.setVisibility(View.GONE);
//                    Toast.makeText(AttendancedetailsActivity.this, "No data found", Toast.LENGTH_SHORT).show();

                }

            }

            @Override
            public void onFailure(Call<StudentLeaveStatus> call, Throwable t) {
                progressFragment.dismiss();

                if (!NetConnection.isConnected(AttendancedetailsActivity.this)) {
                    Toast.makeText(AttendancedetailsActivity.this, "Check Internet connection", Toast.LENGTH_SHORT).show();
                }
            }
        });







    }




    public void setCalendar() {
        try {

            isFirstday = false;

            recyclerView.setVisibility(View.VISIBLE);
            layout_events.setVisibility(View.VISIBLE);

            txtTotalLeaves.setVisibility(View.VISIBLE);


            String expMonth = (month.toString().length() == 1 ? "0" + month.toString() : month.toString());
            String expYear = year.toString();
            txtdate.setText(expMonth + "/" + expYear);

            SimpleDateFormat inFormat = new SimpleDateFormat("dd-MM-yyyy");
            Date date = inFormat.parse("01-" + month + "-" + year);
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);
            String[] days = new String[]{"", "SUNDAY", "MONDAY", "TUESDAY", "WEDNESDAY", "THURSDAY", "FRIDAY", "SATURDAY"};
            String day = days[calendar.get(Calendar.DAY_OF_WEEK)];

            String[] daysloop = new String[]{"SUNDAY", "MONDAY", "TUESDAY", "WEDNESDAY", "THURSDAY", "FRIDAY", "SATURDAY"};

            Calendar cal = Calendar.getInstance();
            cal.set(Calendar.YEAR, year);
            cal.set(Calendar.MONTH, month);
            int numDays = calendar.getActualMaximum(Calendar.DATE);


//                    Map<Integer, List<Attendance>> integerListMap=new HashMap<>();
            List<StudentAbsent> attendances = new ArrayList<>();


            if (!isFirstday) {

                for (int k = 0; k < daysloop.length; k++) {

                    if (daysloop[k].equals(day)) {
                        isFirstday = true;

                        for (int i = 0; i < numDays; i++) {

                            int a = i + 1;
                            String d = "";

                            if (a < 10) {
                                d = "0" + a;
                            } else {
                                d = "" + a;
                            }

                            StudentAbsent attendance = new StudentAbsent();
                            attendance.setIndex(0);
                            attendance.setData(String.valueOf(a));
                            attendance.setDate(expYear + "-" + expMonth + "-" + d);
                            attendances.add(attendance);
                        }
                        break;

                    } else {

                        StudentAbsent attendance = new StudentAbsent();
                        attendance.setIndex(-1);
                        attendance.setData("");
                        attendances.add(attendance);
                    }


                }


            }

            recyclerView.setLayoutManager(new GridLayoutManager(AttendancedetailsActivity.this, 7));
            recyclerView.setAdapter(new AttendanceAdapter(AttendancedetailsActivity.this, attendances,students.getUdcStudentId()));

        } catch (Exception e) {

        }


    }




    public void setCalendarForAttendance(List<StudentAbsent>attendanceList) {
        try {

            isFirstday=false;

            recyclerView.setVisibility(View.VISIBLE);
            layout_events.setVisibility(View.VISIBLE);

            txtTotalLeaves.setVisibility(View.VISIBLE);





            String expMonth = (month.toString().length() == 1 ? "0" + month.toString() : month.toString());
            String expYear = year.toString();
            txtdate.setText(expMonth + "/" + expYear);

            SimpleDateFormat inFormat = new SimpleDateFormat("dd-MM-yyyy");
            Date date = inFormat.parse("01-" + month + "-" + year);
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);
            String[] days = new String[]{"", "SUNDAY", "MONDAY", "TUESDAY", "WEDNESDAY", "THURSDAY", "FRIDAY", "SATURDAY"};
            String day = days[calendar.get(Calendar.DAY_OF_WEEK)];

            String[] daysloop = new String[]{"SUNDAY", "MONDAY", "TUESDAY", "WEDNESDAY", "THURSDAY", "FRIDAY", "SATURDAY"};

            Calendar cal = Calendar.getInstance();
            cal.set(Calendar.YEAR, year);
            cal.set(Calendar.MONTH, month);
            int numDays = calendar.getActualMaximum(Calendar.DATE);


//                    Map<Integer, List<Attendance>> integerListMap=new HashMap<>();
            List<StudentAbsent> attendances = new ArrayList<>();


            if (!isFirstday) {

                for (int k = 0; k < daysloop.length; k++) {

                    if (daysloop[k].equals(day)) {
                        isFirstday = true;

                        for (int i = 0; i < numDays; i++) {

                            int a = i + 1;
                            String d = "";

                            if (a < 10) {
                                d = "0" + a;
                            } else {
                                d = "" + a;
                            }

                            StudentAbsent attendance = new StudentAbsent();
                            attendance.setIndex(0);
                            attendance.setData(String.valueOf(a));
                            attendance.setDate(expYear + "-" + expMonth + "-" + d);
                            attendances.add(attendance);
                        }
                        break;

                    } else {

                        StudentAbsent attendance = new StudentAbsent();
                        attendance.setIndex(-1);
                        attendance.setData("");
                        attendances.add(attendance);
                    }


                }


            }

            boolean isFromdate=false;
            for (StudentAbsent attendance : attendanceList) {


                for (StudentAbsent att1 : attendances) {


                    try {



                            if (att1.getDate().trim().equalsIgnoreCase(attendance.getUdcAbsenceDate().trim())) {




                                att1.setUdcAbsenceDate(attendance.getUdcAbsenceDate());

                                att1.setUdcComments(attendance.getUdcComments());
                                att1.setUdcFatherContactMobile(attendance.getUdcFatherContactMobile());
                                att1.setIsleave(true);
                            }





                    } catch (Exception e) {

                    }


                }

            }






            recyclerView.setLayoutManager(new GridLayoutManager(AttendancedetailsActivity.this, 7));
            recyclerView.setAdapter(new AttendanceAdapter(AttendancedetailsActivity.this, attendances,students.getUdcStudentId()));

        } catch (Exception e) {

        }

    }
}
