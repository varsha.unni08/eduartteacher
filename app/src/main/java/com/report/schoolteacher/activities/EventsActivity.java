package com.report.schoolteacher.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Dialog;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.report.schoolteacher.R;
import com.report.schoolteacher.adapter.EventsAdapter;
import com.report.schoolteacher.connectivity.NetConnection;
import com.report.schoolteacher.pojo.Events;
import com.report.schoolteacher.progress.ProgressFragment;
import com.report.schoolteacher.webservice.RetrofitHelper;
import com.report.schoolteacher.webservice.WebInterfaceimpl;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class EventsActivity extends AppCompatActivity {

    ImageView imgback, imgdate;
    TextView txtdate;

    boolean isFirstday = false;

    RecyclerView recyclerView;

    LinearLayout layout_events;

    ProgressFragment progressFragment;
    Integer month,year;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_events);

        getSupportActionBar().hide();

        imgback = findViewById(R.id.imgback);
        imgdate = findViewById(R.id.imgdate);
        txtdate = findViewById(R.id.txtdate);
        recyclerView = findViewById(R.id.recyclerView);
        layout_events=findViewById(R.id.layout_events);

        imgdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                ExpiryDialog();

            }
        });

        txtdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ExpiryDialog();

            }
        });

        imgback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                onBackPressed();
            }
        });
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        ActivityCompat.finishAfterTransition(this);
    }

    private void ExpiryDialog() {

        final Dialog dialog = new Dialog(EventsActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_date_picker);
        dialog.show();

        Calendar mCalendar = Calendar.getInstance();

        final DatePicker datePicker = (DatePicker) dialog.findViewById(R.id.date_picker);
        Button date_time_set = (Button) dialog.findViewById(R.id.date_time_set);
        datePicker.init(mCalendar.get(Calendar.YEAR), mCalendar.get(Calendar.MONTH), mCalendar.get(Calendar.DAY_OF_MONTH), null);

        LinearLayout ll = (LinearLayout) datePicker.getChildAt(0);
        LinearLayout ll2 = (LinearLayout) ll.getChildAt(0);
        ll2.getChildAt(1).setVisibility(View.INVISIBLE);

        date_time_set.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                isFirstday=false;



                try {
                    month = datePicker.getMonth() + 1;
                    year = datePicker.getYear();
                    String expMonth = (month.toString().length() == 1 ? "0" + month.toString() : month.toString());
                    String expYear = year.toString();
                    txtdate.setText(expMonth + "/" + expYear);

                    SimpleDateFormat inFormat = new SimpleDateFormat("dd-MM-yyyy");
                    Date date = inFormat.parse("01-" + month + "-" + year);
                    Calendar calendar = Calendar.getInstance();
                    calendar.setTime(date);
                    String[] days = new String[]{"", "SUNDAY", "MONDAY", "TUESDAY", "WEDNESDAY", "THURSDAY", "FRIDAY", "SATURDAY"};
                    String day = days[calendar.get(Calendar.DAY_OF_WEEK)];

                    String[] daysloop = new String[]{"SUNDAY", "MONDAY", "TUESDAY", "WEDNESDAY", "THURSDAY", "FRIDAY", "SATURDAY"};

                    Calendar cal = Calendar.getInstance();
                    cal.set(Calendar.YEAR, year);
                    cal.set(Calendar.MONTH, month);
                    int numDays = calendar.getActualMaximum(Calendar.DATE);


//                    Map<Integer, List<Attendance>> integerListMap=new HashMap<>();
                    List<Events> events = new ArrayList<>();


                    if (!isFirstday) {

                        for (int k = 0; k < daysloop.length; k++) {

                            if (daysloop[k].equals(day)) {
                                isFirstday = true;

                                for (int i = 0; i < numDays; i++) {

                                    int a = i + 1;
                                    String d="";

                                    if(a<10)
                                    {
                                        d="0"+a;
                                    }
                                    else {
                                        d=""+a;
                                    }

                                    Events events1 = new Events();
                                    events1.setIndex(0);
                                    events1.setData(String.valueOf(a));
                                    events1.setCalendardate(expYear+"-"+expMonth+"-"+d);
                                    events.add(events1);
                                }
                                break;

                            } else {

                                Events events1 = new Events();
                                events1.setIndex(-1);
                                events1.setData("");
                                events1.setCalendardate("");
                                events.add(events1);
                            }


                        }


                    }

                    getEvents(events);





                } catch (Exception e) {

                }

            }
        });
    }


    public void getEvents(final List<Events> events)
    {
        progressFragment=new ProgressFragment();
        progressFragment.show(getSupportFragmentManager(),"cjkk");

        Retrofit retrofitHelper= RetrofitHelper.getRetrofitInstance(EventsActivity.this);

        WebInterfaceimpl webInterfaceimpl=retrofitHelper.create(WebInterfaceimpl.class);

        Call<List<Events>> listCall=webInterfaceimpl.getEvents(String.valueOf(month),String.valueOf(year));
        listCall.enqueue(new Callback<List<Events>>() {
            @Override
            public void onResponse(Call<List<Events>> call, Response<List<Events>> response) {
                progressFragment.dismiss();

                if(response.body()!=null) {

                    if (response.body().size() > 0) {

                        layout_events.setVisibility(View.VISIBLE);
                        recyclerView.setVisibility(View.VISIBLE);

                        for(Events e:events)
                        {


                            for(Events e1:response.body())
                            {

                                if(e.getCalendardate().trim().equalsIgnoreCase(e1.getUdcEventDate().trim()))
                                {
                                    e.setUdcEventDate(e1.getUdcEventDate());
                                    e.setUdcEventDescription(e1.getUdcEventDescription());
                                    e.setUdcEventName(e1.getUdcEventName());

                                }




                            }


                        }


                        recyclerView.setLayoutManager(new GridLayoutManager(EventsActivity.this, 7));
                        recyclerView.setAdapter(new EventsAdapter(EventsActivity.this, events));


                    }
                    else {

                        layout_events.setVisibility(View.VISIBLE);
                        recyclerView.setVisibility(View.VISIBLE);
//                        layout_events.setVisibility(View.GONE);
//                        recyclerView.setVisibility(View.GONE);

                        recyclerView.setLayoutManager(new GridLayoutManager(EventsActivity.this, 7));
                        recyclerView.setAdapter(new EventsAdapter(EventsActivity.this, events));

                        Toast.makeText(EventsActivity.this,"No data found",Toast.LENGTH_SHORT).show();

                    }

                }

                else {

//                    layout_events.setVisibility(View.GONE);
//                    recyclerView.setVisibility(View.GONE);

                    layout_events.setVisibility(View.VISIBLE);
                    recyclerView.setVisibility(View.VISIBLE);

                    recyclerView.setLayoutManager(new GridLayoutManager(EventsActivity.this, 7));
                    recyclerView.setAdapter(new EventsAdapter(EventsActivity.this, events));


                    Toast.makeText(EventsActivity.this,"No data found",Toast.LENGTH_SHORT).show();

                }

            }

            @Override
            public void onFailure(Call<List<Events>> call, Throwable t) {
                progressFragment.dismiss();


                if(!NetConnection.isConnected(EventsActivity.this))
                {
                    Toast.makeText(EventsActivity.this,"Check Internet connection",Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

}
