package com.report.schoolteacher.adapter;

import android.app.Dialog;
import android.content.Context;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.JsonArray;
import com.report.schoolteacher.R;
import com.report.schoolteacher.activities.StudentListActivity;
import com.report.schoolteacher.connectivity.NetConnection;
import com.report.schoolteacher.fragments.AddMarkFragment;
import com.report.schoolteacher.pojo.MarkCriteria;
import com.report.schoolteacher.pojo.StudentByClass;
import com.report.schoolteacher.pojo.Studentinfo;
import com.report.schoolteacher.progress.ProgressFragment;
import com.report.schoolteacher.webservice.RetrofitHelper;
import com.report.schoolteacher.webservice.WebInterfaceimpl;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class StudentByClassAdapter extends RecyclerView.Adapter<StudentByClassAdapter.StudentByClassHolder> {


    Context context;
    List<Studentinfo>studentByClasses;

    ProgressFragment progressFragment;

    Dialog dialog;

    MarkCriteria markCriteria;

    EditText edtAcheivedMarks,edtDescription;



    public StudentByClassAdapter(Context context, List<Studentinfo> studentByClasses, MarkCriteria markCriteria) {
        this.context = context;
        this.studentByClasses = studentByClasses;
        this.markCriteria=markCriteria;

    }

    public class StudentByClassHolder extends RecyclerView.ViewHolder{

        ImageView imgStudent,imgCheck;

        TextView txtStudent;

        public StudentByClassHolder(@NonNull View itemView) {
            super(itemView);

            imgStudent=itemView.findViewById(R.id.imgStudent);
            imgCheck=itemView.findViewById(R.id.imgCheck);

            txtStudent=itemView.findViewById(R.id.txtStudent);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                   showAddMarklistDialog(studentByClasses.get(getAdapterPosition()),getAdapterPosition());
                }
            });
        }
    }

    @NonNull
    @Override
    public StudentByClassHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View v= LayoutInflater.from(context).inflate(R.layout.layout_student_adapter,parent,false);


        return new StudentByClassHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull StudentByClassHolder holder, int position) {

        holder.txtStudent.setText(studentByClasses.get(position).getUdcStudentName());

        Glide.with(context)
                .load(studentByClasses.get(position).getUdcStudentPhotoPath())
                .apply(RequestOptions.circleCropTransform())
                .placeholder(R.drawable.ic_launcher)
                .into(holder.imgStudent);

        if(studentByClasses.get(position).getMarksadded()==1)
        {
            holder.imgCheck.setVisibility(View.VISIBLE);
        }
        else {
            holder.imgCheck.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        return studentByClasses.size();
    }



    public void showAddMarklistDialog(final Studentinfo studentByClass, final int position)
    {


        dialog=new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.layout_addmark);

        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((AppCompatActivity)context).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int widthLcl = (int) (displayMetrics.widthPixels);
        int heightLcl = (int) (displayMetrics.heightPixels);

        dialog.getWindow().setLayout(widthLcl,heightLcl);


        ImageView imgStudent=dialog.findViewById(R.id.imgStudent);
        TextView txtStudent=dialog.findViewById(R.id.txtStudent);


        edtAcheivedMarks=dialog.findViewById(R.id.edtAcheivedMarks);

        edtDescription=dialog.findViewById(R.id.edtDescription);

        Button btnSubmit=dialog.findViewById(R.id.btnSubmit);

        txtStudent.setText(studentByClass.getUdcStudentName());

        Glide.with(context)
                .load(studentByClass.getUdcStudentPhotoPath())
                .apply(RequestOptions.circleCropTransform())
                .placeholder(R.drawable.ic_launcher)
                .into(imgStudent);


        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {



                if(!edtAcheivedMarks.getText().toString().equals(""))
                {

                    if(!edtDescription.getText().toString().equals(""))
                    {


                        addMark(studentByClass,position);

                    }
                    else {

                        Toast.makeText(context,"enter review",Toast.LENGTH_SHORT).show();

                    }


                }
                else {

                    Toast.makeText(context,"enter acheived marks",Toast.LENGTH_SHORT).show();

                }









            }
        });







        dialog.show();


    }

    public void addMark(final Studentinfo studentByClass, final int position)
    {
        progressFragment = new ProgressFragment();
        progressFragment.show(((StudentListActivity)context).getSupportFragmentManager(), "cjkk");

        Retrofit retrofitHelper = RetrofitHelper.getRetrofitInstance(context);

        WebInterfaceimpl webInterfaceimpl = retrofitHelper.create(WebInterfaceimpl.class);

        Call<JsonArray> jsonArrayCall=webInterfaceimpl.addMarks(markCriteria.getClassid(),markCriteria.getDivisionid(),markCriteria.getTerm(),markCriteria.getExamtype(),markCriteria.getTotalmarke(),markCriteria.getDate(),edtAcheivedMarks.getText().toString(),edtDescription.getText().toString(),studentByClass.getUdcStudentId(),markCriteria.getSubjectid());
        jsonArrayCall.enqueue(new Callback<JsonArray>() {
            @Override
            public void onResponse(Call<JsonArray> call, Response<JsonArray> response) {
                progressFragment.dismiss();
                if(response.body()!=null)
                {

                    try{

                        JSONArray jsonArray=new JSONArray(response.body().toString());
                        if(jsonArray.length()>0)
                        {
                            JSONObject jsonObject=jsonArray.getJSONObject(0);


                            if(jsonObject.getInt("status")==1)
                            {

                                studentByClasses.get(position).setMarksadded(1);

                                notifyItemRangeChanged(0,studentByClasses.size());

                                Toast.makeText(context,jsonObject.getString("message"),Toast.LENGTH_SHORT).show();

                                if(dialog!=null) {
                                    dialog.dismiss();
                                }

                            }



                        }



                    }catch (Exception e)
                    {

                    }


                }
                else {



                    Toast.makeText(context,"failed ",Toast.LENGTH_SHORT).show();

                }
            }

            @Override
            public void onFailure(Call<JsonArray> call, Throwable t) {
                progressFragment.dismiss();

                if(!NetConnection.isConnected(context))
                {
                    Toast.makeText(context,"Check Internet connection",Toast.LENGTH_SHORT).show();
                }
            }
        });

    }
}
