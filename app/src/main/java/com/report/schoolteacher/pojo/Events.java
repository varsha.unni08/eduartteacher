package com.report.schoolteacher.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Events {

    @SerializedName("udc_event_date")
    @Expose
    private String udcEventDate="";
    @SerializedName("udc_event_name")
    @Expose
    private String udcEventName="";
    @SerializedName("udc_event_description")
    @Expose
    private String udcEventDescription="";

    int index;
    String data="";
    String calendardate="";

    public Events() {
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getCalendardate() {
        return calendardate;
    }

    public void setCalendardate(String calendardate) {
        this.calendardate = calendardate;
    }

    public String getUdcEventDate() {
        return udcEventDate;
    }

    public void setUdcEventDate(String udcEventDate) {
        this.udcEventDate = udcEventDate;
    }

    public String getUdcEventName() {
        return udcEventName;
    }

    public void setUdcEventName(String udcEventName) {
        this.udcEventName = udcEventName;
    }

    public String getUdcEventDescription() {
        return udcEventDescription;
    }

    public void setUdcEventDescription(String udcEventDescription) {
        this.udcEventDescription = udcEventDescription;
    }
}
