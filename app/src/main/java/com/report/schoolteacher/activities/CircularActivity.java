package com.report.schoolteacher.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.tabs.TabLayout;
import com.report.schoolteacher.R;
import com.report.schoolteacher.adapter.CircularAdapter;
import com.report.schoolteacher.adapter.CommunicationTabAdapter;
import com.report.schoolteacher.connectivity.NetConnection;
import com.report.schoolteacher.pojo.Circular;
import com.report.schoolteacher.progress.ProgressFragment;
import com.report.schoolteacher.webservice.RetrofitHelper;
import com.report.schoolteacher.webservice.WebInterfaceimpl;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class CircularActivity extends AppCompatActivity {

    ImageView imgback;

//    RecyclerView recycler_view;
//    TextView txtnodatafound;
//
//    ProgressFragment progressFragment;

    ViewPager viewpager;

    TabLayout tablayout;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_circular);
        getSupportActionBar().hide();
        imgback=findViewById(R.id.imgback);
        tablayout=findViewById(R.id.tablayout);
        viewpager=findViewById(R.id.viewpager);
//        recycler_view=findViewById(R.id.recycler_view);
//        txtnodatafound=findViewById(R.id.txtnodatafound);


        viewpager.setAdapter(new CommunicationTabAdapter(getSupportFragmentManager()));
        tablayout.setupWithViewPager(viewpager);

        tablayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {

                viewpager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });





        imgback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                onBackPressed();
            }
        });


    }


}
