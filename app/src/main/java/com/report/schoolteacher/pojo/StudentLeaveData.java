package com.report.schoolteacher.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class StudentLeaveData{
    @SerializedName("udc_absence_id")
    @Expose
    private String udcAbsenceId;
    @SerializedName("udc_student_id")
    @Expose
    private String udcStudentId;
    @SerializedName("udc_accademic_year_id")
    @Expose
    private String udcAccademicYearId;
    @SerializedName("udc_teacher_id")
    @Expose
    private String udcTeacherId;
    @SerializedName("udc_absence_date")
    @Expose
    private String udcAbsenceDate;
    @SerializedName("dt_record")
    @Expose
    private String dtRecord;
    @SerializedName("udc_comments")
    @Expose
    private String udcComments;


        public StudentLeaveData() {
        }


    public String getUdcAbsenceId() {
        return udcAbsenceId;
    }

    public void setUdcAbsenceId(String udcAbsenceId) {
        this.udcAbsenceId = udcAbsenceId;
    }

    public String getUdcStudentId() {
        return udcStudentId;
    }

    public void setUdcStudentId(String udcStudentId) {
        this.udcStudentId = udcStudentId;
    }

    public String getUdcAccademicYearId() {
        return udcAccademicYearId;
    }

    public void setUdcAccademicYearId(String udcAccademicYearId) {
        this.udcAccademicYearId = udcAccademicYearId;
    }

    public String getUdcTeacherId() {
        return udcTeacherId;
    }

    public void setUdcTeacherId(String udcTeacherId) {
        this.udcTeacherId = udcTeacherId;
    }

    public String getUdcAbsenceDate() {
        return udcAbsenceDate;
    }

    public void setUdcAbsenceDate(String udcAbsenceDate) {
        this.udcAbsenceDate = udcAbsenceDate;
    }

    public String getDtRecord() {
        return dtRecord;
    }

    public void setDtRecord(String dtRecord) {
        this.dtRecord = dtRecord;
    }

    public String getUdcComments() {
        return udcComments;
    }

    public void setUdcComments(String udcComments) {
        this.udcComments = udcComments;
    }
}
