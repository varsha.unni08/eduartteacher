package com.report.schoolteacher.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatImageView;

import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.MediaController;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.VideoView;

import com.bumptech.glide.Glide;
import com.report.schoolteacher.R;
import com.report.schoolteacher.pojo.Videos;

public class VideoDetailsActivity extends AppCompatActivity {

    ImageView imgback;

    TextView txtDescription,txtName;

    Videos videos;

    AppCompatImageView capturedimg;

    VideoView videoview;

    RelativeLayout relvideo;

    Handler handler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_details);
        getSupportActionBar().hide();
        imgback=findViewById(R.id.imgback);

        videos=(Videos) getIntent().getSerializableExtra("Entertainment");

        handler=new Handler();

        imgback=findViewById(R.id.imgback);
        txtName=findViewById(R.id.txtName);
        txtDescription=findViewById(R.id.txtDescription);
        capturedimg=findViewById(R.id.capturedimg);
        videoview=findViewById(R.id.videoview);

        relvideo=findViewById(R.id.relvideo);

        txtName.setText(videos.getDsEntertainmentName());
        txtDescription.setText(videos.getDsEntertainmentDescription());


        if(videos.getDsImage().contains(".jpg")) {


            DisplayMetrics displayMetrics = new DisplayMetrics();
            getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);

            int margin = (int) getResources().getDimension(R.dimen.dimen_10dp);

            int width = displayMetrics.widthPixels;

            double heght = width / 1.5;
            int h = (int) heght;

            LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) capturedimg.getLayoutParams();

            layoutParams.width = width;
            layoutParams.height = h;

            layoutParams.setMargins(margin, margin, margin, margin);
            capturedimg.setLayoutParams(layoutParams);

            Glide.with(VideoDetailsActivity.this)
                    .load(videos.getDsImage())

                    .placeholder(R.mipmap.ic_launcher)
                    .into(capturedimg);
        }
        else {

            capturedimg.setVisibility(View.GONE);
        }

        if(videos.getDsVideo().contains(".mp4")) {

            MediaController mediaController = new MediaController(VideoDetailsActivity.this);
            videoview.setVideoURI(Uri.parse(videos.getDsVideo()));
            mediaController.setAnchorView(videoview);
            videoview.setMediaController(mediaController);
            videoview.start();
        }
        else {
            videoview.setVisibility(View.GONE);
            relvideo.setVisibility(View.GONE);
        }




        imgback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                onBackPressed();
            }
        });
    }
}
