package com.report.schoolteacher.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.report.schoolteacher.R;
import com.report.schoolteacher.activities.VideoDetailsActivity;
import com.report.schoolteacher.pojo.Videos;

import java.util.List;

public class VideosAdapter extends RecyclerView.Adapter<VideosAdapter.VideosHolder> {

    Context context;
    List<Videos>videos;

    public VideosAdapter(Context context, List<Videos> videos) {
        this.context = context;
        this.videos = videos;
    }

    public class VideosHolder extends RecyclerView.ViewHolder{

        ImageView imgEntertain;

        TextView txtname,txtDescription;



        public VideosHolder(@NonNull View itemView) {
            super(itemView);
            txtDescription=itemView.findViewById(R.id.txtDescription);
            txtname=itemView.findViewById(R.id.txtname);
            imgEntertain=itemView.findViewById(R.id.imgEntertain);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Intent intent=new Intent(context, VideoDetailsActivity.class);
                    intent.putExtra("Entertainment",videos.get(getAdapterPosition()));

                    context.startActivity(intent);
                }
            });
        }
    }

    @NonNull
    @Override
    public VideosHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_entertainadapter,parent,false);


        return new VideosHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull VideosHolder holder, int position) {

        if(videos.get(position).getDsImage()!=null) {
            Glide.with(context)
                    .load(videos.get(position).getDsImage())

                    .placeholder(R.drawable.ic_launcher)
                    .into(holder.imgEntertain);
        }
        holder.
                txtname.setText(videos.get(position).getDsEntertainmentName());
        holder.txtDescription.setText(videos.get(position).getDsEntertainmentDescription());

    }

    @Override
    public int getItemCount() {
        return videos.size();
    }
}
