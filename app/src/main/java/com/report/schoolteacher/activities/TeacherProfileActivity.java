package com.report.schoolteacher.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.report.schoolteacher.R;
import com.report.schoolteacher.connectivity.NetConnection;
import com.report.schoolteacher.pojo.Profile;
import com.report.schoolteacher.progress.ProgressFragment;
import com.report.schoolteacher.webservice.RetrofitHelper;
import com.report.schoolteacher.webservice.WebInterfaceimpl;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class TeacherProfileActivity extends AppCompatActivity {

    ImageView imgback,imgprofile;

    ProgressFragment progressFragment;

    TextView txtName,txtEmail,txtMobile,txtQualification,txtOccupation,txtAddress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_teacher_profile);
        getSupportActionBar().hide();
        imgback=findViewById(R.id.imgback);
        imgprofile=findViewById(R.id.imgprofile);

        txtQualification=findViewById(R.id.txtQualification);
        txtName=findViewById(R.id.txtName);
        txtEmail=findViewById(R.id.txtEmail);
        txtMobile=findViewById(R.id.txtMobile);
        txtOccupation=findViewById(R.id.txtOccupation);
        txtAddress=findViewById(R.id.txtAddress);


        imgback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                onBackPressed();
            }
        });

        getParentProfile();
    }


    public void getParentProfile() {
        progressFragment = new ProgressFragment();
        progressFragment.show(getSupportFragmentManager(), "cjkk");

        Retrofit retrofitHelper = RetrofitHelper.getRetrofitInstance(TeacherProfileActivity.this);

        WebInterfaceimpl webInterfaceimpl = retrofitHelper.create(WebInterfaceimpl.class);

        Call<List<Profile>>listCall=webInterfaceimpl.getTeacherProfile();
        listCall.enqueue(new Callback<List<Profile>>() {
            @Override
            public void onResponse(Call<List<Profile>> call, Response<List<Profile>> response) {
                progressFragment.dismiss();
                if(response.body()!=null)
                {

                    if(response.body().size()>0)
                    {

                        txtName.setText(response.body().get(0).getUdcTeacherName()+" "+response.body().get(0).getUdcTeacherLastName());

                        txtEmail.setText(response.body().get(0).getUdcTeacherEmailId());
                        txtMobile.setText(response.body().get(0).getUdcTeacherMob());
                        txtQualification.setText(response.body().get(0).getUdcTeacherQualification());
                        txtOccupation.setText(response.body().get(0).getUdcTeacherJointDate());
                        txtAddress.setText(response.body().get(0).getUdcTeacherCode());


                        Glide.with(TeacherProfileActivity.this)
                                .load(response.body().get(0).getDsImage())
                                .apply(RequestOptions.circleCropTransform())
                                .placeholder(R.drawable.ic_launcher)
                                .into(imgprofile);

                    }
                    else {

                        Toast.makeText(TeacherProfileActivity.this,"No data found",Toast.LENGTH_SHORT).show();

                    }


                }
                else {

                    Toast.makeText(TeacherProfileActivity.this,"failed",Toast.LENGTH_SHORT).show();

                }

            }

            @Override
            public void onFailure(Call<List<Profile>> call, Throwable t) {
                progressFragment.dismiss();

                if(!NetConnection.isConnected(TeacherProfileActivity.this))
                {
                    Toast.makeText(TeacherProfileActivity.this,"Check Internet connection",Toast.LENGTH_SHORT).show();
                }

            }
        });

    }
}
