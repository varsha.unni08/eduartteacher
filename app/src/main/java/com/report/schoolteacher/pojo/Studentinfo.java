package com.report.schoolteacher.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Studentinfo implements Serializable {

    @SerializedName("udc_student_id")
    @Expose
    private String udcStudentId;
    @SerializedName("udc_student_name")
    @Expose
    private String udcStudentName;
    @SerializedName("udc_student_photo_path")
    @Expose
    private String udcStudentPhotoPath;
    @SerializedName("udc_teacher_id")
    @Expose
    private String udcTeacherId;

         int absencemarked=0;
         int marksadded=0;

    public Studentinfo() {
    }

    public int getMarksadded() {
        return marksadded;
    }

    public void setMarksadded(int marksadded) {
        this.marksadded = marksadded;
    }

    public int getAbsencemarked() {
        return absencemarked;
    }

    public void setAbsencemarked(int absencemarked) {
        this.absencemarked = absencemarked;
    }

    public String getUdcStudentId() {
        return udcStudentId;
    }

    public void setUdcStudentId(String udcStudentId) {
        this.udcStudentId = udcStudentId;
    }

    public String getUdcStudentName() {
        return udcStudentName;
    }

    public void setUdcStudentName(String udcStudentName) {
        this.udcStudentName = udcStudentName;
    }

    public String getUdcStudentPhotoPath() {
        return udcStudentPhotoPath;
    }

    public void setUdcStudentPhotoPath(String udcStudentPhotoPath) {
        this.udcStudentPhotoPath = udcStudentPhotoPath;
    }

    public String getUdcTeacherId() {
        return udcTeacherId;
    }

    public void setUdcTeacherId(String udcTeacherId) {
        this.udcTeacherId = udcTeacherId;
    }
}
