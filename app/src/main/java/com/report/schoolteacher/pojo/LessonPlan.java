package com.report.schoolteacher.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LessonPlan {
    @SerializedName("udc_lessonplan_id")
    @Expose
    private String udcLessonplanId;
    @SerializedName("udc_class_details")
    @Expose
    private String udcClassDetails;
    @SerializedName("udc_subject_details")
    @Expose
    private String udcSubjectDetails;
    @SerializedName("udc_lessonplan_topic")
    @Expose
    private String udcLessonplanTopic;
    @SerializedName("udc_expected_imp_date")
    @Expose
    private String udcExpectedImpDate;
    @SerializedName("udc_lessonplan_completion_status")
    @Expose
    private String udcLessonplanCompletionStatus;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("udc_lessonplan_completion_date")
    @Expose
    private String udcLessonplanCompletionDate;

    int selected=0;

    public LessonPlan() {
    }

    public String getUdcLessonplanId() {
        return udcLessonplanId;
    }

    public void setUdcLessonplanId(String udcLessonplanId) {
        this.udcLessonplanId = udcLessonplanId;
    }

    public String getUdcLessonplanCompletionDate() {
        return udcLessonplanCompletionDate;
    }

    public void setUdcLessonplanCompletionDate(String udcLessonplanCompletionDate) {
        this.udcLessonplanCompletionDate = udcLessonplanCompletionDate;
    }

    public int getSelected() {
        return selected;
    }

    public void setSelected(int selected) {
        this.selected = selected;
    }

    public String getUdcClassDetails() {
        return udcClassDetails;
    }

    public void setUdcClassDetails(String udcClassDetails) {
        this.udcClassDetails = udcClassDetails;
    }

    public String getUdcSubjectDetails() {
        return udcSubjectDetails;
    }

    public void setUdcSubjectDetails(String udcSubjectDetails) {
        this.udcSubjectDetails = udcSubjectDetails;
    }

    public String getUdcLessonplanTopic() {
        return udcLessonplanTopic;
    }

    public void setUdcLessonplanTopic(String udcLessonplanTopic) {
        this.udcLessonplanTopic = udcLessonplanTopic;
    }

    public String getUdcExpectedImpDate() {
        return udcExpectedImpDate;
    }

    public void setUdcExpectedImpDate(String udcExpectedImpDate) {
        this.udcExpectedImpDate = udcExpectedImpDate;
    }

    public String getUdcLessonplanCompletionStatus() {
        return udcLessonplanCompletionStatus;
    }

    public void setUdcLessonplanCompletionStatus(String udcLessonplanCompletionStatus) {
        this.udcLessonplanCompletionStatus = udcLessonplanCompletionStatus;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
