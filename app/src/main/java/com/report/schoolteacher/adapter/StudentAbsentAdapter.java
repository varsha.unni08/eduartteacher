package com.report.schoolteacher.adapter;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.JsonArray;
import com.report.schoolteacher.R;
import com.report.schoolteacher.activities.AddAttendanceActivity;
import com.report.schoolteacher.activities.HomeActivity;
import com.report.schoolteacher.activities.LoginActivity;
import com.report.schoolteacher.connectivity.NetConnection;
import com.report.schoolteacher.pojo.Studentinfo;
import com.report.schoolteacher.pojo.Students;
import com.report.schoolteacher.preferencehelper.PreferenceHelper;
import com.report.schoolteacher.progress.ProgressFragment;
import com.report.schoolteacher.webservice.RetrofitHelper;
import com.report.schoolteacher.webservice.WebInterfaceimpl;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class StudentAbsentAdapter extends RecyclerView.Adapter<StudentAbsentAdapter.StudentAbsentHolder> {


    Context context;
    List<Studentinfo>students;

    ProgressFragment progressFragment;

    public StudentAbsentAdapter(Context context, List<Studentinfo> students) {
        this.context = context;
        this.students = students;
    }

    public class StudentAbsentHolder extends RecyclerView.ViewHolder{

        TextView txtStudent;

        CheckBox checkbox;

        ImageView imgStudent;

        public StudentAbsentHolder(@NonNull View itemView) {
            super(itemView);
            imgStudent=itemView.findViewById(R.id.imgStudent);
            checkbox=itemView.findViewById(R.id.checkbox);
            txtStudent=itemView.findViewById(R.id.txtStudent);

            checkbox.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if(students.get(getAdapterPosition()).getAbsencemarked()==0)
                    {

                        AlertDialog.Builder builder=new AlertDialog.Builder(context);
                        builder.setTitle(R.string.app_name);
                        builder.setMessage("Do you want to mark absence to "+students.get(getAdapterPosition()).getUdcStudentName()+" ?");
                        builder.setPositiveButton("yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();

                                students.get(getAdapterPosition()).setAbsencemarked(0);

                                notifyItemChanged(getAdapterPosition());

                                addAbsence(students.get(getAdapterPosition()),getAdapterPosition());

                            }
                        });

                        builder.setNegativeButton("no", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {

                                dialogInterface.dismiss();

                            }
                        });
                        builder.show();


                    }
                    else {
                        students.get(getAdapterPosition()).setAbsencemarked(1);

                        notifyItemChanged(getAdapterPosition());
                    }


                }
            });
        }
    }

    @NonNull
    @Override
    public StudentAbsentHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View v= LayoutInflater.from(context).inflate(R.layout.layout_add_absence,parent,false);



        return new StudentAbsentHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull StudentAbsentHolder holder, int position) {

        holder.txtStudent.setText(students.get(position).getUdcStudentName());

        Glide.with(context)
                .load(students.get(position).getUdcStudentPhotoPath())
                .apply(RequestOptions.circleCropTransform())
                .placeholder(R.drawable.u)
                .into(holder.imgStudent);
        if(students.get(position).getAbsencemarked()==0)
        {
            holder.checkbox.setChecked(false);
        }
        else {

            holder.checkbox.setChecked(true);
        }


    }

    @Override
    public int getItemCount() {
        return students.size();
    }

    public void addAbsence(final Studentinfo student,final int position)
    {
        progressFragment = new ProgressFragment();
        progressFragment.show(((AppCompatActivity)context).getSupportFragmentManager(), "cjkk");

        Retrofit retrofitHelper = RetrofitHelper.getRetrofitInstance(context);

        WebInterfaceimpl webInterfaceimpl = retrofitHelper.create(WebInterfaceimpl.class);

        String d=AddAttendanceActivity.end_date;
        String date="";

        try{

            DateFormat df=new SimpleDateFormat("dd-MM-yyyy");
            Date dt=df.parse(d);

            DateFormat df1=new SimpleDateFormat("yyyy-MM-dd");

             date=df1.format(dt);



        }catch (Exception e)
        {

        }





        Call<JsonArray>jsonArrayCall=webInterfaceimpl.addAbsence(student.getUdcStudentId(),date,"");
        jsonArrayCall.enqueue(new Callback<JsonArray>() {
            @Override
            public void onResponse(Call<JsonArray> call, Response<JsonArray> response) {
                progressFragment.dismiss();
                if(response.body()!=null)
                {

                    try{

                        JSONArray jsonArray=new JSONArray(response.body().toString());
                        if(jsonArray.length()>0)
                        {
                            JSONObject jsonObject=jsonArray.getJSONObject(0);


                            if(jsonObject.getInt("status")==1)
                            {


                                Toast.makeText(context,jsonObject.getString("message"),Toast.LENGTH_SHORT).show();


                                students.get(position).setAbsencemarked(1);
                                notifyItemRangeChanged(0,students.size());

                            }
                            else {
                                Toast.makeText(context,jsonObject.getString("message"),Toast.LENGTH_SHORT).show();
                            }



                        }



                    }catch (Exception e)
                    {

                    }


                }
                else {



                    Toast.makeText(context,"failed to delete message",Toast.LENGTH_SHORT).show();

                }
            }

            @Override
            public void onFailure(Call<JsonArray> call, Throwable t) {
                progressFragment.dismiss();

                if (!NetConnection.isConnected(context)) {
                    Toast.makeText(context, "Check Internet connection", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}
